using System;
using System.Collections.Generic;

[Serializable]
public class BallInfo
{
    public int C = -1;

    public BubbleType T = BubbleType.Empty;
    // public BubbleType bubbleType => (BubbleType)T;
}

[Serializable]
public class LevelRowInfo
{
    public List<BallInfo> Cells = new List<BallInfo>();
}