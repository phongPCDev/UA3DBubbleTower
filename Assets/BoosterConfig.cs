﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "GameData/BoosterConfig", fileName = "BoosterConfig")]
public class BoosterConfig : ScriptableObject
{
    public List<BoosterConfigItem> Boosters;

    public BoosterConfigItem GetBoosterConfigItem(BoosterType type)
    {
        int count = Boosters.Count;
        for (int i = 0; i < count; i++)
        {
            if (Boosters[i].Type == type)
            {
                return Boosters[i];
            }
        }

        Debug.LogError("Booster does not exist");
        return null;
    }
}

[Serializable]
public class BoosterConfigItem
{
    public string Name;
    // public string Description;
    public int Cost = 300;
    public BoosterType Type;
    public Sprite LockIcon;
    public Sprite UiIcon;
    public Sprite UiIconGlow;
    // public GameObject Prefab;
    public int SellAmount;
    // public GameObject ExplosiveEffect;
    public int LevelUnlock;
    public int CollectValue = 20;
    public Sprite ProgressSprite;

    public void UpdateName()
    {
        if (Type > BoosterType.Random)
        {
            Name = Type.ToString();
        }
    }
}

public enum BoosterType
{
    None,
    Bomb,
    Rainbow,
    Thunder,
    Laser,
    Random,

    ThunderBolt,
    Rocket,
    ColorBrush
}