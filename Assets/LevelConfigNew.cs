using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using UnityEngine;

[Serializable]
public class LevelConfigNew
{
    public int id;
    public int d;
    public bool hard;
    public int moveLeft;
    public int row;
    public int col;
    public int star1;
    public int star2;
    public int star3;
    public List<int> colors;
    public List<int> startBalls = new List<int>();
    public List<int> bubbles_Id = new List<int>();
    public List<BubbleType> bubbles_Type = new List<BubbleType>();
    public string randRate;

    [JsonIgnore] public List<LevelRowInfo> rows = new List<LevelRowInfo>();

    public void Init()
    {
        rows = new List<LevelRowInfo>();
        for (int i = 0; i < row; i++)
        {
            LevelRowInfo rowInfo = new LevelRowInfo();
            for (int j = 0; j < col; j++)
            {
                int index = i * col + j;
                rowInfo.Cells.Add(new BallInfo { C = bubbles_Id[index], T = bubbles_Type[index] });
            }

            rows.Add(rowInfo);
        }
    }

    public void InitBoard(BallInfo info = null)
    {
        if (info == null)
        {
            info = new BallInfo { C = -1, T = BubbleType.Empty };
        }

        rows = new List<LevelRowInfo>();
        for (int i = 0; i < row; i++)
        {
            LevelRowInfo row = new LevelRowInfo();
            for (int j = 0; j < col; j++)
            {
                row.Cells.Add(info);
            }

            rows.Add(row);
        }
    }

    public void InsertRow(int index)
    {
        LevelRowInfo rowInfo = new LevelRowInfo();
        for (int j = 0; j < col; j++)
        {
            rowInfo.Cells.Add(new BallInfo { C = -1, T = BubbleType.Empty });
        }

        rows.Insert(index, rowInfo);
        row = rows.Count;
    }

    public void InsertCol(int index)
    {
        foreach (var rowInfo in rows)
        {
            rowInfo.Cells.Insert(index, new BallInfo { C = -1, T = BubbleType.Empty });
        }

        col = rows[0].Cells.Count;
    }

    public void RemoveRow(int index)
    {
        rows.RemoveAt(index);
        row = rows.Count;
    }

    public void RemoveCol(int index)
    {
        foreach (var rowInfo in rows)
        {
            rowInfo.Cells.RemoveAt(index);
        }

        col = rows[0].Cells.Count;
    }

    public Dictionary<int, float> GetRateRandom()
    {
        Dictionary<int, float> dict = new Dictionary<int, float>();
        if (!string.IsNullOrEmpty(randRate))
        {
            var arr = randRate.Split('-');
            int length = Mathf.Min(arr.Length, colors.Count);
            float randomRate = 0;
            for (int i = 0; i < length; i++)
            {
                float.TryParse(arr[i], out var value);
                randomRate += value;
                dict.Add(colors[i], randomRate);
            }

            var delta = (colors.Count - length);
            if (delta > 0)
            {
                var step = (100 - randomRate) / delta;
                for (int i = length; i < colors.Count; i++)
                {
                    randomRate += step;
                    dict.Add(colors[i], randomRate);
                }
            }
        }

        return dict;
    }
    
    // public LevelConfig GetLevelConfig()
    // {
    //     LevelConfig levelConfig = ScriptableObject.CreateInstance<LevelConfig>();
    //     levelConfig.LevelNumber = id;
    //     levelConfig.d = d;
    //     levelConfig.isLevelHard = hard;
    //     levelConfig.MoveLeft = moveLeft;
    //     levelConfig.NumRow = row;
    //     levelConfig.NumColl = col;
    //     levelConfig.LevelRowInfo = rows;
    //     levelConfig.StartBalls = startBalls.Select(s=>new BallInfo(){C = s, T = BubbleType.Normal}).ToList();
    //     levelConfig.NumStar1 = star1;
    //     levelConfig.NumStar2 = star2;
    //     levelConfig.NumStar3 = star3;
    //     levelConfig.Colors = colors;
    //     levelConfig.startBallsRandomRate = randRate;
    //     
    //     return levelConfig;
    // }
}