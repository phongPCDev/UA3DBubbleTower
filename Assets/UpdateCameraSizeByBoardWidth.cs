using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateCameraSizeByBoardWidth : MonoBehaviour
{
    public static Action OnCameraSizeChanged;
#if UNITY_EDITOR
    [SerializeField] private bool isRealTimeUpdate;
#endif
    private Camera _camera;

    private void Awake()
    {
        _camera = Camera.main;
    }

    private void OnEnable()
    {
        CalculatorCameraSize();
        BubblePuzzleLogic.OnLevelLoaded += CalculatorCameraSize;
    }

    private void OnDisable()
    {
        BubblePuzzleLogic.OnLevelLoaded -= CalculatorCameraSize;
    }

#if !UNITY_EDITOR_WIN && UNITY_STANDALONE_WIN
    private void Update()
    {
        if (Mathf.Abs(Input.mouseScrollDelta.y) > 0.01f)
        {
            _camera.orthographicSize -= Time.deltaTime * 120 * Input.mouseScrollDelta.y;
            OnCameraSizeChanged?.Invoke();
            // if (_camera.orthographicSize < 8.68f) _camera.orthographicSize = 8.68f;
            //else if (_camera.orthographicSize > maxSize) _camera.orthographicSize = maxSize;
            // cameraSize = _camera.orthographicSize;
        }
    }
#endif

// #if UNITY_EDITOR
//     void Update()
//     {
//         if (isRealTimeUpdate)
//         {
//             CalculatorCameraSize();
//         }
//     }
// #endif

    private void CalculatorCameraSize()
    {
        return; 
    }
}