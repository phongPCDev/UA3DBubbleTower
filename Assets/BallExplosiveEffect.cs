﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallExplosiveEffect : MonoBehaviour
{
    public float LiveTime = 1;
    public List<ParticleSystem> Particles;

    private static readonly Color[] MatchColors =
    {
        new Color(0.8941177f, 0f, 0.04705882f), new Color(0.3333333f, 0.8980392f, 0.04705882f),
        new Color(0.0859f, 0.0859f, 0.941f), new Color(0.9960784f, 0.5882353f, 0.1333333f),
        new Color(0.9803922f, 0f, 0.6745098f), new Color(0f, 0.7254902f, 0.8117647f),
    };

    private void OnEnable()
    {
        StartCoroutine(RemoveObj());
    }

    IEnumerator RemoveObj()
    {
        yield return new WaitForSeconds(LiveTime);
        Destroy(gameObject);
    }

    public void SetData(Color color)
    {
        transform.localScale = Vector3.one;
        foreach (var particle in Particles)
        {
            var particleMain = particle.main;
            particleMain.startColor = color;
        }
    }

    public void SetData(int colorIndex)
    {
        transform.localScale = Vector3.one;
        foreach (var particle in Particles)
        {
            var particleMain = particle.main;
            particleMain.startColor = MatchColors[colorIndex];
        }
    }
}