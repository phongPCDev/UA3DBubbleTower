using DG.Tweening;
using UnityEngine;

public class SwapSuggestController : MonoBehaviour
{
    [SerializeField] private SpriteRenderer rotateArrow1;
    [SerializeField] private SpriteRenderer rotateArrow2;
    [SerializeField] private float defaultAlpha;
    public bool isShowing;

    public void ShowSuggest()
    {
        if (isShowing)
        {
            return;
        }

        isShowing = true;
        transform.DOKill();
        rotateArrow1.DOFade(defaultAlpha, 0.2f).SetTarget(transform);
        rotateArrow2.DOFade(defaultAlpha, 0.2f).SetTarget(transform);
    }

    public void HideSuggest()
    {
        if (!isShowing)
            return;

        isShowing = false;
        transform.DOKill();
        rotateArrow1.DOFade(0, 0.2f).SetTarget(transform);
        rotateArrow2.DOFade(0, 0.2f).SetTarget(transform);
    }
}