﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
#if USE_SPINE
using Spine.Unity;
#endif
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Object = UnityEngine.Object;

public static class GameHelper
{
    #region Vecter2 Extentions

    public static float GetAngleInRadian(this Vector2 v1, Vector2 v2)
    {
        return Mathf.Atan2(v2.y - v1.y, v2.x - v1.x);
    }

    public static float GetAngleInDegree(this Vector2 v1, Vector2 v2)
    {
        return Mathf.Atan2(v2.y - v1.y, v2.x - v1.x) * Mathf.Rad2Deg;
    }

    public static Vector2 Rotate(this Vector2 v, float angle)
    {
        v = Quaternion.AngleAxis(angle, Vector2.up) * v;
        return v;
    }

    #endregion

    #region Vecter3 Extentions

    public static float GetAngleInRadian(this Vector3 v1, Vector3 v2)
    {
        return Mathf.Atan2(v2.y - v1.y, v2.x - v1.x);
    }

    public static float GetAngleInDegree(this Vector3 v1, Vector3 v2)
    {
        return Mathf.Atan2(v2.y - v1.y, v2.x - v1.x) * Mathf.Rad2Deg;
    }

    public static float GetAngleInDegree(this Vector3 v1)
    {
        return Mathf.Atan2(v1.y, v1.x) * Mathf.Rad2Deg;
    }


    public static Vector3 Rotate(this Vector3 v, float angle)
    {
        v = Quaternion.AngleAxis(angle, Vector3.back) * v;
        return v;
    }

    #endregion

    #region Transform Extentions

    public static List<Transform> GetAllChilds(this Transform t)
    {
        List<Transform> lstTransforms = new List<Transform>();
        int childCount = t.childCount;
        for (int i = 0; i < childCount; i++)
            lstTransforms.Add(t.GetChild(i));

        return lstTransforms;
    }

    public static void DeleteAllChilds(this Transform t)
    {
        int childCount = t.childCount;
        for (int i = 0; i < childCount; i++)
            Object.DestroyImmediate(t.GetChild(0).gameObject);
    }

    public static void OrderByY(this Transform t)
    {
        var position = t.position;
        position.z = position.y * 0.01f;
        t.position = position;
    }

    public static void SetSizeByWidth(this RectTransform img, float width, float aspect)
    {
        img.sizeDelta = new Vector2(width, width * aspect);
    }

    public static void SetSizeByHeight(this RectTransform img, float height, float aspect)
    {
        img.sizeDelta = new Vector2(height / aspect, height);
    }

    #endregion

    #region Image Extentions

    public static void SetSizeByWidth(this Image img, float width)
    {
        if (img.sprite == null)
            return;
        var sprite = img.sprite;
        float aspect = sprite.bounds.size.y / sprite.bounds.size.x;
        img.GetComponent<RectTransform>().sizeDelta = new Vector2(width, width * aspect);
    }

    public static void SetSizeByHeight(this Image img, float height)
    {
        if (img.sprite == null)
            return;
        var sprite = img.sprite;
        float aspect = sprite.bounds.size.y / sprite.bounds.size.x;
        img.GetComponent<RectTransform>().sizeDelta = new Vector2(height / aspect, height);
    }

    public static void SetAlpha(this Image img, float alpha)
    {
        Color color = img.color;
        color.a = alpha;
        img.color = color;
    }

    public static void SetAlpha(this TextMesh text, float alpha)
    {
        Color color = text.color;
        color.a = alpha;
        text.color = color;
    }

    #endregion

    #region AsssetDatabase Helper

    public static void SetDirty(Object obj)
    {
#if UNITY_EDITOR
        UnityEditor.EditorUtility.SetDirty(obj);
#endif
    }

    public static void SaveAssetDatabase(Object obj, bool isRefresh = false)
    {
#if UNITY_EDITOR
        UnityEditor.EditorUtility.SetDirty(obj);
        UnityEditor.AssetDatabase.SaveAssets();
        if (isRefresh)
            UnityEditor.AssetDatabase.Refresh();
#endif
    }

    public static List<T> GetAllAssetAtPath<T>(string filter, string path)
    {
#if UNITY_EDITOR
        string[] findAssets = UnityEditor.AssetDatabase.FindAssets(filter, new[] {path});
        List<T> os = new List<T>();
        foreach (var findAsset in findAssets)
        {
            os.Add((T) Convert.ChangeType(
                UnityEditor.AssetDatabase.LoadAssetAtPath(UnityEditor.AssetDatabase.GUIDToAssetPath(findAsset),
                    typeof(T)), typeof(T)));
        }

        return os;
#endif
        return null;
    }

    public static List<Object> GetAllAssetsAtPath(string path)
    {
#if UNITY_EDITOR
        string[] paths = {path};
        var assets = UnityEditor.AssetDatabase.FindAssets(null, paths);
        var assetsObj = assets.Select(s =>
            UnityEditor.AssetDatabase.LoadMainAssetAtPath(UnityEditor.AssetDatabase.GUIDToAssetPath(s))).ToList();
        return assetsObj;
#endif
        return null;
    }

    public static List<Sprite> GetAllSpriteAssetsAtPath(string path)
    {
#if UNITY_EDITOR
        string[] paths = {path};
        var assets = UnityEditor.AssetDatabase.FindAssets("t:sprite", paths);
        var assetsObj = assets.Select(s =>
            UnityEditor.AssetDatabase.LoadAssetAtPath<Sprite>(UnityEditor.AssetDatabase.GUIDToAssetPath(s))).ToList();
        return assetsObj;
#endif
        return null;
    }

    public static List<Material> GetAllMaterialAssetsAtPath(string path)
    {
#if UNITY_EDITOR
        string[] paths = { path };
        var assets = UnityEditor.AssetDatabase.FindAssets("t:material", paths);
        var assetsObj = assets.Select(s =>
            UnityEditor.AssetDatabase.LoadAssetAtPath<Material>(UnityEditor.AssetDatabase.GUIDToAssetPath(s))).ToList();
        return assetsObj;
#endif
        return null;
    }
    #endregion

    #region List Extentions

    public static void Shuffle<T>(this IList<T> ts)
    {
        var count = ts.Count;
        var last = count - 1;
        for (var i = 0; i < last; ++i)
        {
            var r = UnityEngine.Random.Range(i, count);
            var tmp = ts[i];
            ts[i] = ts[r];
            ts[r] = tmp;
        }
    }

    #endregion

    #region Random Extention

    public static float GetRandom1(float min, float max)
    {
        return UnityEngine.Random.Range(min, max) * (UnityEngine.Random.Range(0, 2) == 1 ? 1 : -1);
    }

    #endregion

    #region Scene Extention

    public static void DeleteScene(string sceneName)
    {
#if UNITY_EDITOR
        var scene = GetAllScenes().Find(s => s == sceneName);
        if (scene != null)
        {
            var lstObjs = SceneManager.GetSceneByName(scene).GetRootGameObjects();
            foreach (var gameObject in lstObjs)
                GameObject.Destroy(gameObject);
        }
#endif
    }

    public static List<string> GetAllScenes()
    {
        List<string> scenes = new List<string>();
        for (int i = 0; i < SceneManager.sceneCount; i++)
            scenes.Add(SceneManager.GetSceneAt(i).name);
        return scenes;
    }

    #endregion

#if USE_SPINE
    public static string GetSkinNameAtIndex(this SkeletonDataAsset spine, int index)
    {
        return spine.GetSkeletonData(true).Skins.Items[index].Name;
    }

    public static void SetSkin(this SkeletonGraphic spine, string skinName)
    {
        spine.Skeleton.SetSkin(skinName);
        spine.Skeleton.SetSlotsToSetupPose();
        spine.AnimationState.Apply(spine.Skeleton);
    }

    public static void SetSkin(this SkeletonAnimation spine, string skinName)
    {
        spine.Skeleton.SetSkin(skinName);
        spine.Skeleton.SetSlotsToSetupPose();
        spine.AnimationState.Apply(spine.Skeleton);
    }
#endif
    
    public static Vector2 GetPivot(this Sprite s)
    {
        return new Vector2(s.pivot.x / s.rect.width, s.pivot.y / s.rect.height);
    }

    public static void SetPivot(GameObject o)
    {
        var sprite = o.GetComponent<Image>().sprite;
        var rect = o.GetComponent<RectTransform>();
        Vector2 curPivot = rect.pivot;
        rect.pivot = sprite.GetPivot();
        Vector2 delta = curPivot - rect.pivot;
        rect.anchoredPosition -= new Vector2(rect.sizeDelta.x * delta.x, rect.sizeDelta.y * delta.y);
    }

    public static void SetAlpha(this SpriteRenderer sprite, float alpha)
    {
        var color = sprite.color;
        color.a = alpha;
        sprite.color = color;
    }

    #region Random Percent Helper
    public static T GetRandomValueBasedOnPercent<T>(params (float, T)[] list)
    {
        float percentValue = UnityEngine.Random.Range(0f, 100f);
        float curPercent = 0f;

        for (int i = 0; i < list.Length; i++)
        {
            curPercent += list[i].Item1;
            if (percentValue < curPercent)
            {
                return list[i].Item2;
            }
        }

        return list[list.Length - 1].Item2;
    }

    public static int GetRandomIndexBasedOnPercent(float[] perCentList)
    {
        float percentValue = UnityEngine.Random.Range(0f, 100f);
        float curPercent = 0f;

        for (int i = 0; i < perCentList.Length; i++)
        {
            int index = i;
            curPercent += perCentList[index];
            if (percentValue < curPercent)
            {
                return index;
            }
        }

        return perCentList.Length - 1;
    }
    #endregion
}