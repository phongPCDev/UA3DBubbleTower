using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallController : MonoBehaviour
{
    [SerializeField] private List<Transform> leftWalls;
    [SerializeField] private List<Transform> rightWalls;

    private void OnEnable()
    {
        BubblePuzzleLogic.OnLevelLoaded += UpdateWallsPosition;
        UpdateCameraSizeByBoardWidth.OnCameraSizeChanged += UpdateWallsPosition;
    }

    private void OnDisable()
    {
        BubblePuzzleLogic.OnLevelLoaded -= UpdateWallsPosition;
        UpdateCameraSizeByBoardWidth.OnCameraSizeChanged -= UpdateWallsPosition;
    }

    private void UpdateWallsPosition()
    {
        int numCol = BubblePuzzleLogic.I.GetNumCols();
        var half = numCol / 2f;
        foreach (var wall in leftWalls)
        {
            var render = wall.GetComponent<SpriteRenderer>();
            ChangeSize(render);
            var width = render.bounds.size.x;
            var pos = wall.position;
            pos.x = -half - width / 2;
            wall.position = pos;
        }
        foreach (var wall in rightWalls)
        {
            var render = wall.GetComponent<SpriteRenderer>();
            ChangeSize(render);
            var width = render.bounds.size.x;
            var pos = wall.position;
            pos.x = half + width / 2;
            wall.position = pos;
        }
    }

    private void ChangeSize(SpriteRenderer renderer)
    {
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
        if (renderer.drawMode == SpriteDrawMode.Sliced)
        {
            renderer.size = new(500, 500);
        }
#endif
    }
}