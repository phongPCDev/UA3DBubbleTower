using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageLocalizeController : MonoBehaviour
{
    public Sprite globalSprite;
    public Sprite chinaSprite;
    public Image target;

    private void Start()
    {
        target.sprite = GameController.I.isGlobal ? globalSprite : chinaSprite;
    }

    private void Reset()
    {
        target = GetComponent<Image>();
    }
}