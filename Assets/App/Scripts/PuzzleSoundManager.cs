using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class PuzzleSoundManager : MonoBehaviour, IPuzzleSound
{
    private void Awake()
    {
        BubblePuzzleLogic.PuzzleSound = this;
    }

    public void PlayWinSound()
    {
        SoundController.I.play_clear.PlaySound();
    }

    public void PlayClearSound()
    {
        SoundController.I.bubble_pop1.Volume = 1;
        SoundController.I.bubble_pop1?.PlaySoundOne(0.1f);
    }

    public void PlayLoseSound()
    {
    }

    public void PlayShootSound()
    {
        SoundController.I.bubble_shoot.PlaySound();
    }

    public void PlayHitSound()
    {
        SoundController.I.bubble_bounce.PlaySound();
    }

    public void PlayShootOnWinSound()
    {
        int count = Random.Range(0, 6);
        switch (count)
        {
            case 0:
                SoundController.I.clear_bubbles1?.PlaySound();
                break;
            case 1:
                SoundController.I.clear_bubbles2?.PlaySound();
                break;
            case 2:
                SoundController.I.clear_bubbles3?.PlaySound();
                break;
            case 3:
                SoundController.I.clear_bubbles4?.PlaySound();
                break;
            case 4:
                SoundController.I.clear_bubbles5?.PlaySound();
                break;
            default:
                SoundController.I.clear_bubbles6?.PlaySound();
                break;
        }
    }

    public void PlayReadySound()
    {
        SoundController.I.play_ready.PlaySound();
    }

    public void PlayDropCollectedSound()
    {
        SoundController.I.bubble_fall?.PlaySoundOne();
    }

    public void PlayStarAppearSound()
    {
    }

    public void PlayStarCollectedSound()
    {
    }

    public void PlaySwapSound()
    {
        SoundController.I.bubble_switch?.PlaySoundOne();
    }

    public void PlayFireSound()
    {
        SoundController.I.item_firecracker.PlaySound();
    }

    public void PlayRainbowSound()
    {
        SoundController.I.item_rainbow_paint.PlaySound();
    }

    public void PlayLightingSound()
    {
        SoundController.I.bubble_lightning.PlaySound();
    }

    public void PlayBeamSound()
    {
        SoundController.I.bubble_beam.PlaySound();
    }

    public void PlayColorBrushSound()
    {
        SoundController.I.bubble_colorbomb.PlaySound();
    }

    public void PlayRocketActiveSound()
    {
        SoundController.I.bubble_lightning.PlaySound();
    }

    public void PlayRocketSound()
    {
        SoundController.I.bubble_spark.PlaySound();
    }

    public void PlayThunderBoltSound()
    {
        SoundController.I.bubble_spark.PlaySound();
    }
}