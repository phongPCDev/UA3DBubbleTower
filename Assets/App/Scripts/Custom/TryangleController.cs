using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TryangleController : BaseController
{
    public Transform hand;
    public GameObject tutorial;

    public override void Setup()
    {
        tutorial.gameObject.SetActive(false);
        GameManager.I.level = level;

        BubblePuzzleLogic.OnReady += () =>
        {
            StartCoroutine(DelayShowAim());
        };
        base.Setup();
    }

    IEnumerator DelayShowAim()
    {
        yield return new WaitForEndOfFrame();
        tutorial.gameObject.SetActive(true);
        BubblePuzzleLogic.I.Aim(hand.position);
        BubblePuzzleLogic.I.aimController.Hide();
        BubblePuzzleLogic.I.aimController.GetComponent<AimDotManager>()._aimDots[0].gameObject.SetActive(false);
    }
    
    private void Update()
    {
        if (tutorial.activeInHierarchy)
        {
            BubblePuzzleLogic.I.Aim(hand.position);
            if (Input.GetMouseButtonDown(0))
            {
                tutorial.SetActive(false);
            }
        }
    }
}