using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseController : MonoBehaviour
{
    public eControl controlType;
    public int level = 76;

    public virtual void Setup()
    {
        GameManager.I.LoadLevel();
    }
}