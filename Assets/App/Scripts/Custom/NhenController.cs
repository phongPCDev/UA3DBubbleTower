using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class NhenController : BaseController
{
    public List<GameObject> objectToDisableList;
    public List<int> itemTutorialList;
    public SpriteRenderer _black;
    public Transform textTakeOffTheWeb;

    public override void Setup()
  {
        GameManager.I.level = 77;
        _black.color = Color.clear;
        textTakeOffTheWeb.localScale = Vector3.zero;
        BubblePuzzleLogic.OnReady += () =>
        {
            var bubbles = BubblePuzzleLogic.I.GetBubbles();
            _black.DOFade(.5f, .3f);
            foreach (int i in itemTutorialList)
            {
                bubbles[i].transform.localPosition += new Vector3(0, 0, -1f);
            }

            textTakeOffTheWeb.DOScale(1, .3f).SetDelay(.3f);
            BubblePuzzleLogic.I.startBalls.transform.position += new Vector3(0, 0, -1f);
        };
        BubblePuzzleLogic.OnShoot += (bubble, bubble1, arg3) =>
        {
            if (textTakeOffTheWeb.gameObject.activeInHierarchy)
            {
                textTakeOffTheWeb.gameObject.SetActive(false);
                _black.DOFade(0, .1f);
            }
        };
        base.Setup();
  }

    public void OnEnable()
    {
        foreach (GameObject o in objectToDisableList)
        {
            o.SetActive(false);
        }
    }
}