using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleColorSwapper : MonoBehaviour
{
    public int[] ogColors;
    public int[] newColors;
    public bool IsTwoWaySwap = false;

    private void Awake()
    {
        BubblePuzzleLogic.OnReadFileDone += () =>
        {
            for (int i = 0; i < ogColors.Length; i++)
            {
                if (i < newColors.Length)
                {
                    Swap(ogColors[i], newColors[i]);
                }
            }
        };
    }

    private void Swap(int ogColor, int newColor)
    {
        var levelConfig = BubblePuzzleLogic.I.levelConfig;
        foreach (var rowInfo in levelConfig.rows)
        {
            foreach (var cell in rowInfo.Cells)
            {
                if (cell.C == ogColor)
                {
                    cell.C = newColor;
                }
                else if (IsTwoWaySwap)
                {
                    if (cell.C == newColor)
                    {
                        cell.C = ogColor;
                    }
                }
            }
        }

        for (var index = 0; index < levelConfig.startBalls.Count; index++)
        {
            var startBall = levelConfig.startBalls[index];
            if (startBall == ogColor)
            {
                levelConfig.startBalls[index] = newColor;
            }
            else if (IsTwoWaySwap)
            {
                if (startBall == newColor)
                {
                    levelConfig.startBalls[index] = ogColor;
                }
            }
        }

        for (var index = 0; index < levelConfig.colors.Count; index++)
        {
            var startBall = levelConfig.colors[index];
            if (startBall == ogColor)
            {
                levelConfig.colors[index] = newColor;
            }
            else if (IsTwoWaySwap)
            {
                if (startBall == newColor)
                {
                    levelConfig.colors[index] = ogColor;
                }
            }
        }
    }

}
