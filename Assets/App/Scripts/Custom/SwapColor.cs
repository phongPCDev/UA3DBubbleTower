using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwapColor : MonoBehaviour
{
    public int color1;
    public int color2;

    private void Awake()
    {
        BubblePuzzleLogic.OnReadFileDone += () =>
        {
            var levelConfig = BubblePuzzleLogic.I.levelConfig;
            foreach (var rowInfo in levelConfig.rows)
            {
                foreach (var cell in rowInfo.Cells)
                {
                    if (cell.C == color1)
                    {
                        cell.C = color2;
                    }
                    else if (cell.C == color2)
                    {
                        cell.C = color1;
                    }
                }
            }

            for (var index = 0; index < levelConfig.startBalls.Count; index++)
            {
                var startBall = levelConfig.startBalls[index];
                if (startBall == color1)
                {
                    levelConfig.startBalls[index] = color2;
                }
                else if (startBall == color2)
                {
                    levelConfig.startBalls[index] = color1;
                }
            }

            for (var index = 0; index < levelConfig.colors.Count; index++)
            {
                var startBall = levelConfig.colors[index];
                if (startBall == color1)
                {
                    levelConfig.colors[index] = color2;
                }
                else if (startBall == color2)
                {
                    levelConfig.colors[index] = color1;
                }
            }
        };
    }
}