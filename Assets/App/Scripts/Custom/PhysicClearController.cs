using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicClearController : BaseController
{
    [Header("Board")]
    public bool IsClearPhys = true;
    public bool ShowEndCard = true;
    public float SpawnRadius = 3;

    [Header("Board Positioning")]
    public float LimitBottomOffset = 4;
    public float LimitTopPos = 11;
    public float StartBallOffset = 4;
    public float DropCheckPointY = -22f;

    [Header("Shot Limit")]
    public int ShotBeforeEnd = 6;

    [Header("Feature Switches")]
    public bool IsUsing3DPhysics = true;
    public bool IsWaitForClearBeforeDrop = true;
    public bool DisableBubblePhysicsBeforeMove = false;

    [Header("Clear Delta")]
    public float ClearDelta = 0.03f;

    [Header("Board Move Settings")]
    public AnimationCurve MoveCurve;
    public float BoardMoveSpeed = 40;
    public float ShotBubbleSpeed = 100;

    [Header("Bubble Rigidbody Drag")]
    public float ClearPhysicsDrag = 0.1f;
    public float ClearPhysicsMass = 0.1f;

    [Header("Drop Check")]
    [Tooltip("If true, wait till all drop before create new shoot bubble")]
    public bool IsDropChecking = false;

    private int shotFired = 0;

    public override void Setup()
    {
        SetUpBoardPos();
        shotFired = 0;
        GameManager.I.level = level;

        Bubble.IsClearPhysic = IsClearPhys;

        Bubble.ClearPhysicsDrag = ClearPhysicsDrag;
        Bubble.ClearPhysicsMass = ClearPhysicsMass;

        BubblePuzzleLogic.I.ShootRemain = ShotBeforeEnd;
        BubblePuzzleLogic.isUse3D = IsUsing3DPhysics;
        BubblePuzzleLogic.I.isDropChecking = IsDropChecking;

        BubblePuzzleLogic.I.disableBubblePhysicsBeforeMove = DisableBubblePhysicsBeforeMove;
        BubblePuzzleLogic.I.clearDeltaTime = ClearDelta;

        BubblePuzzleLogic.I.moveCurve = MoveCurve;
        BubblePuzzleLogic.I.boardMoveDownSpeed = BoardMoveSpeed;
        BubblePuzzleLogic.I.ballMoveSpeed = ShotBubbleSpeed;
        BubblePuzzleLogic.I.showEndCard = ShowEndCard;
        BubblePuzzleLogic.I.SpawnRadius = SpawnRadius;

        base.Setup();
        BubblePuzzleLogic.I.boardContainer.position += Vector3.forward * SpawnRadius;
        SetLastShotClearDeltaEvent();
        SetLastShotFiredEvent();
        BubblePuzzleLogic.I.limitBottom.position += Vector3.up * LimitBottomOffset;
        BubblePuzzleLogic.I.limitTop.position = Vector3.up * LimitTopPos;
    }

    private void SetUpBoardPos()
    {
        BubblePuzzleLogic.I.startBallOffset -= StartBallOffset;
        BubblePuzzleLogic.I.dropCheckPoint.transform.position = Vector3.up * DropCheckPointY;
    }

    private void SetLastShotClearDeltaEvent()
    {

        BubblePuzzleLogic.OnShootChanged += () =>
        {
            shotFired += 1;
            if (shotFired >= ShotBeforeEnd && !PuzzleState.IsEndGame)
            {
                BubblePuzzleLogic.I.clearDeltaTime = 0;
            }
        };

    }

    private void SetLastShotFiredEvent()
    {
        BubblePuzzleLogic.OnDone += () =>
        {
            if (shotFired >= ShotBeforeEnd && !PuzzleState.IsEndGame)
            {
                BubblePuzzleLogic.I.TestWin();
            }
        };
    }
}
