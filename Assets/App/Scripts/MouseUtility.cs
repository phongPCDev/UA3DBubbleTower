using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MouseUtility
{
    public static Vector3 GetMousePosition()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = Camera.main.farClipPlane * 0.5F;
        return Camera.main.ScreenToWorldPoint(mousePos);
    }

    public static RaycastHit GetRayHitUnderMouse(bool showDebug = false)
    {
        Physics.Raycast(Camera.main.transform.position, GetMousePosition() - Camera.main.transform.position, out RaycastHit hit, Mathf.Infinity);
        if (showDebug)
        {
            Debug.DrawRay(Camera.main.transform.position, GetMousePosition() - Camera.main.transform.position);
            Debug.DrawRay(hit.point, -Vector3.forward * 10);
        }
        return hit;
    }

    public static RaycastHit GetRayHitUnderMouse(LayerMask targetMask, bool showDebug = false)
    {
        Physics.Raycast(Camera.main.transform.position, GetMousePosition() - Camera.main.transform.position, out RaycastHit hit, Mathf.Infinity, targetMask);
        if (showDebug)
        {
            Debug.DrawRay(Camera.main.transform.position, GetMousePosition() - Camera.main.transform.position);
            Debug.DrawRay(hit.point, -Vector3.forward * 10);
        }

        return hit;
    }
}

