// using System.Collections;
// using System.Collections.Generic;
// using AFramework;
// using UnityEngine;
//
// public class ObjectPool : SingletonMono<ObjectPool>
// {
//     private Dictionary<GameObject, List<GameObject>> objectPools = new Dictionary<GameObject, List<GameObject>>();
//
//     // Dictionary để lưu trữ các prefab cho từng loại đối tượng
//     private Dictionary<GameObject, GameObject> prefabDict;
//
//     // Khởi tạo pool cho một loại đối tượng
//     private void InitializePool(GameObject prefab, int initialPoolSize)
//     {
//         if (!objectPools.ContainsKey(prefab))
//         {
//             objectPools[prefab] = new List<GameObject>();
//
//             for (int i = 0; i < initialPoolSize; i++)
//             {
//                 GameObject obj = Instantiate(prefab);
//                 obj.SetActive(false);
//                 objectPools[prefab].Add(obj);
//             }
//         }
//     }
//
//     public GameObject GetObject(GameObject prefab)
//     {
//         if (objectPools.ContainsKey(prefab))
//         {
//             List<GameObject> pool = objectPools[prefab];
//
//             foreach (GameObject obj in pool)
//             {
//                 if (!obj.activeInHierarchy)
//                 {
//                     obj.SetActive(true);
//                     return obj;
//                 }
//             }
//
//             // Nếu không có đối tượng nào khả dụng trong pool, tạo mới và trả về
//             GameObject newObj = Instantiate(prefab);
//             pool.Add(newObj);
//             newObj.SetActive(true);
//             return newObj;
//         }
//
//         // Nếu pool cho loại đối tượng chưa được khởi tạo, khởi tạo và trả về đối tượng mới
//         InitializePool(prefab, 1);
//         return GetObject(prefab);
//     }
//
//     public Transform Spawn(Transform prefab)
//     {
//         if (objectPools.ContainsKey(prefab.gameObject))
//         {
//             List<GameObject> pool = objectPools[prefab.gameObject];
//
//             foreach (GameObject obj in pool)
//             {
//                 if (!obj.activeInHierarchy)
//                 {
//                     obj.SetActive(true);
//                     return obj.transform;
//                 }
//             }
//
//             // Nếu không có đối tượng nào khả dụng trong pool, tạo mới và trả về
//             GameObject newObj = Instantiate(prefab.gameObject);
//             pool.Add(newObj);
//             newObj.SetActive(true);
//             return newObj.transform;
//         }
//
//         // Nếu pool cho loại đối tượng chưa được khởi tạo, khởi tạo và trả về đối tượng mới
//         InitializePool(prefab.gameObject, 1);
//         return GetObject(prefab.gameObject).transform;
//     }
//
//     public Transform Spawn(Transform prefab, Vector3 position, Quaternion quaternion)
//     {
//         var t = Spawn(prefab);
//         t.position = position;
//         t.rotation = quaternion;
//         return t;
//     }
//
//     public Transform Spawn(Transform prefab, Vector3 position, Quaternion quaternion, Transform parent)
//     {
//         var t = Spawn(prefab);
//         t.position = position;
//         t.rotation = quaternion;
//         t.SetParent(parent);
//         return t;
//     }
//
//     public Transform Spawn(GameObject prefab, Vector3 position, Quaternion quaternion, Transform parent)
//     {
//         var t = GetObject(prefab).transform;
//         t.position = position;
//         t.rotation = quaternion;
//         t.SetParent(parent);
//         return t;
//     }
//
//     public Transform Spawn(GameObject prefab, Vector3 position, Quaternion quaternion)
//     {
//         var t = GetObject(prefab).transform;
//         t.position = position;
//         t.rotation = quaternion;
//         return t;
//     }
//
//     public Transform Spawn(GameObject prefab, Transform parent)
//     {
//         var t = GetObject(prefab).transform;
//         t.SetParent(parent);
//         return t;
//     }
//
//     // Trả đối tượng về pool
//     public void ReturnObject(GameObject obj)
//     {
//         obj.SetActive(false);
//     }
//
//     public void Despawn(Transform obj)
//     {
//         obj.gameObject.SetActive(false);
//     }
//
//     // Khởi tạo các pool cho các prefab đã chỉ định
//     // public void InitializePools(Dictionary<GameObject, int> prefabDict)
//     // {
//     //     objectPools = new Dictionary<GameObject, List<GameObject>>();
//     //     this.prefabDict = prefabDict;
//     //
//     //     foreach (KeyValuePair<GameObject, int> entry in prefabDict)
//     //     {
//     //         GameObject prefab = entry.Key;
//     //         int initialPoolSize = entry.Value;
//     //         InitializePool(prefab, initialPoolSize);
//     //     }
//     // }
// }