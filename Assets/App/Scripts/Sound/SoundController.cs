﻿using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;

using UnityEditor;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#if ODIN_INSPECTOR
using Sirenix.OdinInspector.Editor;
#endif
#endif

public partial class SoundController : MonoBehaviour
{
    public static SoundController I;
    private static bool _isVibration;

    public static bool IsVibration
    {
        get => _isVibration;
        set
        {
            _isVibration = value;
            PlayerPrefs.SetInt("IsVibration", value ? 1 : 0);
        }
    }

    private static bool _isSoundOn;

    public static bool IsSoundOn
    {
        get => _isSoundOn;
        set
        {
            _isSoundOn = value;
            PlayerPrefs.SetInt("IsSoundOn", value ? 1 : 0);
        }
    }

    private static bool _isMusicOn;

    public static bool IsMusicOn
    {
        get => _isMusicOn;
        set
        {
            _isMusicOn = value;
            if (!_isMusicOn)
            {
                I.StopBackgroundMusic();
            }
            else
            {
                I.PlayBackgroundMusic();
            }

            PlayerPrefs.SetInt("IsMusicOn", value ? 1 : 0);
            OnBgmVolumeChanged?.Invoke();
        }
    }

    private static bool _isColorblind;

    public static bool IsColorblind
    {
        get => _isColorblind;
        set
        {
            PuzzleState.IsColorblind = value;
            _isColorblind = value;
            PlayerPrefs.SetInt("IsColorblind", value ? 1 : 0);
            OnColorblindChanged?.Invoke(value);
        }
    }

    public static float _bgmVolume;
    public static Action OnBgmVolumeChanged;
    public static Action<bool> OnColorblindChanged;

    private List<AudioSource> _backgroundSource = new List<AudioSource>();
    private AudioSource _currentBackgroundSource;

    public AudioSource currentBackgroundSource
    {
        get { return _currentBackgroundSource; }
    }

    private readonly List<AudioSource> _audioSources = new List<AudioSource>();
    private SoundInfo _curBackgroundMusic = new SoundInfo();
    public Dictionary<SoundInfo, float> playOneDict = new Dictionary<SoundInfo, float>();

    private void Awake()
    {
        I = this;
        IsMusicOn = PlayerPrefs.GetInt("IsMusicOn", 1) == 1;
        IsSoundOn = PlayerPrefs.GetInt("IsSoundOn", 1) == 1;
        IsVibration = PlayerPrefs.GetInt("IsVibration", 1) == 1;
        
        PlaySoundEffect(new SoundInfo {Clip = clear_bubbles1.Clip, Volume = 0});
        PlaySoundEffect(new SoundInfo {Clip = clear_bubbles2.Clip, Volume = 0});
        PlaySoundEffect(new SoundInfo {Clip = clear_bubbles3.Clip, Volume = 0});
        PlaySoundEffect(new SoundInfo {Clip = clear_bubbles4.Clip, Volume = 0});
        PlaySoundEffect(new SoundInfo {Clip = clear_bubbles5.Clip, Volume = 0});
        PlaySoundEffect(new SoundInfo {Clip = clear_bubbles6.Clip, Volume = 0});
        PlaySoundEffect(new SoundInfo {Clip = bubble_shoot.Clip, Volume = 0});
        PlaySoundEffect(new SoundInfo {Clip = bubble_pop1.Clip, Volume = 0});
        PlayBackgroundMusic(menu);
    }

    private void Start()
    {
        IsColorblind = PlayerPrefs.GetInt("IsColorblind", 0) == 1;
        PuzzleState.IsColorblind = IsColorblind;
    }

    public void PlayBackgroundMusic(SoundInfo sound = null)
    {
        if (sound != null)
            _curBackgroundMusic = sound;
        else
        {
            sound = _curBackgroundMusic;
        }

        if (_currentBackgroundSource != null)
            _currentBackgroundSource.Stop();

        _currentBackgroundSource = GetBgAudioSource();
        _currentBackgroundSource.clip = sound.Clip;
        _currentBackgroundSource.Play();
        if (_isMusicOn)
        {
            _currentBackgroundSource.volume = sound.Volume * .5f;
            _currentBackgroundSource.Play();
            var s = _currentBackgroundSource;
        }
        else
        {
            _currentBackgroundSource.volume = 0;
        }
    }

    public AudioSource GetBgAudioSource()
    {
        AudioSource source;
        for (int i = 0; i < _backgroundSource.Count; i++)
        {
            if (!_backgroundSource[i].isPlaying)
            {
                return _backgroundSource[i];
            }
        }

        source = new GameObject("bgSound").AddComponent<AudioSource>();
        source.transform.SetParent(transform);
        _backgroundSource.Add(source);
        source.loop = true;
        return source;
    }

    public void StopBackgroundMusic()
    {
        foreach (var source in _backgroundSource)
        {
            source.Stop();
        }
    }

    public void StopSoundEffect(SoundInfo t)
    {
        foreach (var audioSource in _audioSources)
        {
            if (audioSource.clip == t.Clip)
                audioSource.Stop();
        }
    }

    public void PlaySoundEffect(SoundInfo sound)
    {
        if (!IsSoundOn)
            return;
        if (sound.Clip == null)
        {
#if UNITY_EDITOR
            Debug.Log("clip null");
#endif
            return;
        }

        AudioSource audioSource = GetAudioSource();
        audioSource.clip = sound.Clip;
        audioSource.volume = sound.Volume * .5f;
#if UNITY_EDITOR
        audioSource.gameObject.name = sound.Clip.name;
#endif
        audioSource.Play();
    }

    public void StopAllSoundEffect()
    {
    }

    private AudioSource GetAudioSource()
    {
        if (_audioSources.Count > 0)
            for (int i = 0; i < _audioSources.Count; i++)
                if (!_audioSources[i].isPlaying)
                    return _audioSources[i];

        var s = GetNewAudioSource();
        _audioSources.Add(s);
        return s;
    }

    private AudioSource GetNewAudioSource()
    {
        var obj = new GameObject();
        obj.transform.SetParent(transform);
        return obj.AddComponent<AudioSource>();
    }
}

public static class SoundExt
{
    public static void PlaySound(this SoundInfo t)
    {
        SoundController.I.PlaySoundEffect(t);
    }

    public static void PlaySoundOne(this SoundInfo t, float delay = .1f)
    {
        if (SoundController.I.playOneDict.ContainsKey(t) && (Time.time - SoundController.I.playOneDict[t]) < delay)
        {
            return;
        }

        SoundController.I.playOneDict[t] = Time.time;
        SoundController.I.PlaySoundEffect(t);
    }

    public static void StopSoundOne(this SoundInfo t)
    {
        SoundController.I.StopSoundEffect(t);
    }

    public static void PlayMusicLoop(this SoundInfo t)
    {
        SoundController.I.PlayBackgroundMusic(t);
    }
}

[Serializable]
public class SoundInfo
{
    public AudioClip Clip;
    public float Volume = 1;
}

#if UNITY_EDITOR
[CustomEditor(typeof(SoundController))]
public class SoundControllerEditor :
#if ODIN_INSPECTOR
    OdinEditor
#else
        Editor
#endif
{
    public override void OnInspectorGUI()
    {
        if (GUILayout.Button("Update References"))
        {
            var soundController = target as SoundController;
            var listSoundInfos = soundController.GetType().GetFields().ToList()
                .FindAll(s => s.FieldType == typeof(SoundInfo));

            var assets = GetAllAssets<AudioClip>();
            foreach (var soundInfo in listSoundInfos)
            {
                var clip = assets.Find(s =>
                    s.name.ToLower().Replace(" ", "") == soundInfo.Name.ToLower().Replace(" ", ""));
                if (clip != null)
                {
                    var clipField = soundInfo.FieldType.GetField("Clip");
                    clipField.SetValue(soundInfo.GetValue(soundController), clip);
                    EditorUtility.SetDirty(soundController);
                }
            }
        }

        base.OnInspectorGUI();
    }

    private static List<T> GetAllAssets<T>() where T : class
    {
        string[] paths = { "Assets" };
        var assets = AssetDatabase.FindAssets(null, paths);
        var assetsObj = assets.Select(s => AssetDatabase.LoadMainAssetAtPath(AssetDatabase.GUIDToAssetPath(s))).ToList()
            .FindAll(s => s.GetType() == typeof(T));
        List<T> lst = new List<T>();
        foreach (var o in assetsObj)
            lst.Add(o as T);

        return lst;
    }
}
#endif