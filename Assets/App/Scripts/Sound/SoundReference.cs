﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class SoundController
{
    [Header("Background Music")] public SoundInfo menu;
    public SoundInfo song1;
    public SoundInfo song2;

    [Header("Sound Effect")] public SoundInfo charging_Booster_max;
    public SoundInfo item_rainbow_paint;
    public SoundInfo common_fill;
    public SoundInfo bubble_beam;
    public SoundInfo bubble_bounce;
    public SoundInfo bubble_fall;
    public SoundInfo bubble_fire;
    public SoundInfo bubble_ice;
    public SoundInfo bubble_join;
    public SoundInfo bubble_lightning;
    public SoundInfo bubble_magic;
    public SoundInfo bubble_pop1;
    public SoundInfo bubble_shoot;
    public SoundInfo bubble_switch;
    public SoundInfo clear_bubbles1;
    public SoundInfo clear_bubbles2;
    public SoundInfo clear_bubbles3;
    public SoundInfo clear_bubbles4;
    public SoundInfo clear_bubbles5;
    public SoundInfo clear_bubbles6;
    public SoundInfo clear_gift;
    public SoundInfo clear_highscore;
    public SoundInfo clear_star1;
    public SoundInfo clear_star2;
    public SoundInfo clear_star3;
    public SoundInfo common_button;
    public SoundInfo common_getcoin;
    public SoundInfo item_fire;
    public SoundInfo item_firecracker;
    public SoundInfo item_lightning;
    public SoundInfo item_rainbow;
    public SoundInfo play_10move;
    public SoundInfo play_clear;
    public SoundInfo play_fail;
    public SoundInfo play_progress;
    public SoundInfo play_ready;
    public SoundInfo play_star1;
    public SoundInfo play_star2;
    public SoundInfo play_star3;
    public SoundInfo popup_in;
    public SoundInfo popup_out;
    public SoundInfo popup_outofmoves;
    public SoundInfo reward_get;
    public SoundInfo reward_spin;
    public SoundInfo reward_spin2;
    public SoundInfo open_pack;
    
    public SoundInfo bubble_spark;
    public SoundInfo bubble_colorbomb;
    public SoundInfo bubble_waterbomb;
    public SoundInfo common_getitem;
    
    
}