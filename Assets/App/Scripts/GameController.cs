using AFramework;
using UnityEngine;

public class GameController : SingletonMono<GameController>
{
    public bool isGlobal = true;
    public GameObject completePopup;
    [Header("Global")]
    public string androidLink;
    public string iosLink;
    
    [Header("China")]
    public string androidLinkCn;
    public string iosLinkCn;

    private void Awake()
    {
        Debug.Log("Enter here");
        completePopup.SetActive(false);
    }

    public void OnDownload()
    {
        //if (isGlobal)
        //{
        //    Luna.Unity.Playable.InstallFullGame(iosLink, androidLink);
        //}
        //else
        //{
        //    Luna.Unity.Playable.InstallFullGame(iosLinkCn, androidLinkCn);
        //}
    }
}