// using System;
// using System.Collections;
// using System.Collections.Generic;
// using UnityEngine;
// using UnityEngine.Serialization;
//
// public class StretchSpriteRendererWithCamera : MonoBehaviour
// {
// #if UNITY_EDITOR
//     [SerializeField] private bool isRealTimeUpdate;
// #endif
//
//     public Camera Camera;
//
//     private SpriteRenderer _spriteRenderer;
//
//     private SpriteRenderer spriteRenderer
//     {
//         get
//         {
//             if (_spriteRenderer == null)
//             {
//                 _spriteRenderer = GetComponent<SpriteRenderer>();
//             }
//
//             return _spriteRenderer;
//         }
//     }
//
//     private void Start()
//     {
//         CalculatorSize();
//     }
//
//     private void OnEnable()
//     {
//         BubblePuzzleLogic.OnLevelLoaded += CalculatorCameraSize;
//         UpdateCameraSizeByBoardWidth.OnCameraSizeChanged += CalculatorCameraSize;
//     }
//
//     private void OnDisable()
//     {
//         BubblePuzzleLogic.OnLevelLoaded -= CalculatorCameraSize;
//         UpdateCameraSizeByBoardWidth.OnCameraSizeChanged -= CalculatorCameraSize;
//     }
//     
// // #if UNITY_EDITOR
// //     private void OnDrawGizmos()
// //     {
// //         if (isRealTimeUpdate)
// //         {
// //             CalculatorSize();
// //         }
// //     }
// // #endif
//
//
//     private void CalculatorCameraSize()
//     {
//         float cameHeight = Camera.orthographicSize * 2;
//         float cameWidth = BubblePuzzleLogic.I.levelConfig.col;
//         spriteRenderer.size = new Vector2(cameWidth, cameHeight);
//     }
//     
//     private void CalculatorSize()
//     {
//         float cameHeight = Camera.orthographicSize * 2;
//         float cameWidth = cameHeight * Camera.aspect;
//         spriteRenderer.size = new Vector2(cameWidth, cameHeight);
//     }
// }