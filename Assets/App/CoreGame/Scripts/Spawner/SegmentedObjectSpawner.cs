using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SegmentedObjectSpawner : MonoBehaviour
{
    public GameObject SegmentPrefab;
    public int NumSegment = 1;
    public Vector3 SegmentOffset;

    protected List<GameObject> spawnedSegments = new List<GameObject>();

    private void Start()
    {
        CreateSegmentedObject();
    }

    [ContextMenu("Create Object")]
    public virtual void CreateSegmentedObject()
    {
        DeleteUnusedSegments();
        for (int i = 0; i < NumSegment; i++)
        {
            var segment = GetSegment(i);
            segment.SetActive(true);
            segment.transform.position = i == 0 ? transform.position : spawnedSegments[i - 1].transform.position + SegmentOffset;
            Debug.Log(i);
        }
    }

    public virtual GameObject GetSegment(int index = 0)
    {
        if (index < spawnedSegments.Count)
            return spawnedSegments[index];
        var segment = Instantiate(SegmentPrefab, transform.position, Quaternion.identity, transform);
        spawnedSegments.Add(segment);
        return segment;
    }

    public virtual void DeleteUnusedSegments()
    {
        for (int i = NumSegment; i < spawnedSegments.Count; i++)
        {
            spawnedSegments[i].SetActive(false);
        }
    }
}
