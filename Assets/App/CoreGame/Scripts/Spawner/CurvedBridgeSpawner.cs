using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurvedBridgeSpawner : SegmentedObjectSpawner
{
    public AnimationCurve BridgeCurveType;
    public AnimationCurve BridgeRotationCurve;

    public override void CreateSegmentedObject()
    {
        base.CreateSegmentedObject();
        UpdateCurviness();
    }

    public void UpdateCurviness()
    {
        foreach (var item in spawnedSegments)
        {
            int index = spawnedSegments.IndexOf(item);
            item.transform.localRotation = Quaternion.Euler(BridgeRotationCurve.Evaluate(index), 0, 0);
            item.transform.localPosition += Vector3.forward * BridgeCurveType.Evaluate(index);
        }
    }
}
