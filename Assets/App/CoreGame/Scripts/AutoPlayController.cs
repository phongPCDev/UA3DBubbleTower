using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using AFramework;
using UnityEngine;
using UnityEngine.UI;

public class AutoPlayController : ManualSingletonMono<AutoPlayController>
{
    public bool isRunning;
    public int deltaAngle = 1;
    public bool hasCloudBubble;

    private void OnEnable()
    {
        BubblePuzzleLogic.OnLevelLoaded += LevelLoaded;
        BubblePuzzleLogic.OnPreWin += OnPreWin;
    }

    private void OnDisable()
    {
        BubblePuzzleLogic.OnLevelLoaded -= LevelLoaded;
        BubblePuzzleLogic.OnPreWin -= OnPreWin;
    }

    private void OnPreWin()
    {
        StopAllCoroutines();
    }
    
    private void LevelLoaded()
    {
        isRunning = false;
        StopAllCoroutines();
        hasCloudBubble = BubblePuzzleLogic.I.cloudBubbles.Count > 0;
    }

    public void RunAutoPlay()
    {
        if (isRunning) return;
        isRunning = true;
        StartCoroutine(CRAutoPlay());
    }

    private IEnumerator CRAutoPlay()
    {
        yield return null;
        // yield return new WaitUntil(() => BubblePuzzleLogic.I.IsReady);
        // while (BubblePuzzleLogic.I.GetShootRemain() > 0)
        // {
        //     _canNext = false;
        //     bool needSwapBall = false;
        //     var defaultAngle = UnityEngine.Random.Range(30, 150) * Mathf.Deg2Rad;
        //     Vector3 direct = new Vector3(Mathf.Cos(defaultAngle), Mathf.Sin(defaultAngle)).normalized;
        //     BoosterUi boosterToUse = null;
        //     bool isShootCloud = false;
        //     bool isRandom = true;
        //     foreach (BoosterUi ui in InGameMenu.I.booster)
        //     {
        //         if (ui.canUseFree)
        //         {
        //             boosterToUse = ui;
        //             break;
        //         }
        //     }
        //
        //     if (boosterToUse == null)
        //     {
        //         var dict = BubblePuzzleLogic.I.GetSuggestionList(BubblePuzzleLogic.I.currentBubble.ballInfo.C,
        //             deltaAngle);
        //         if (dict.Keys.Count > 0)
        //         {
        //             var angle = dict.Keys.ToList()[0] * Mathf.Deg2Rad;
        //             direct = new Vector3(Mathf.Cos(angle), Mathf.Sin(angle)).normalized;
        //             isRandom = false;
        //         }
        //         else
        //         {
        //             if (hasCloudBubble)
        //             {
        //                 var angle = GetCloudDirect();
        //                 if (angle > 0)
        //                 {
        //                     isShootCloud = true;
        //                     direct = GetDirectFromAngle(angle);
        //                     isRandom = false;
        //                 }
        //             }
        //
        //             if (!isShootCloud && BubblePuzzleLogic.I != null && BubblePuzzleLogic.I.nextBubble != null &&
        //                 BubblePuzzleLogic.I.currentBubble.ballInfo.C != BubblePuzzleLogic.I.nextBubble.ballInfo.C)
        //             {
        //                 dict = BubblePuzzleLogic.I.GetSuggestionList(BubblePuzzleLogic.I.nextBubble.ballInfo.C,
        //                     deltaAngle);
        //                 if (dict.Keys.Count > 0)
        //                 {
        //                     needSwapBall = true;
        //                     var angle = dict.Keys.ToList()[0] * Mathf.Deg2Rad;
        //                     direct = new Vector3(Mathf.Cos(angle), Mathf.Sin(angle)).normalized;
        //                     isRandom = false;
        //                 }
        //             }
        //         }
        //
        //         if (needSwapBall)
        //         {
        //             BubblePuzzleLogic.I.SwapBubble();
        //             yield return new WaitForSeconds(0.3f);
        //         }
        //     }
        //     else
        //     {
        //         boosterToUse.GetComponent<Button>().onClick.Invoke();
        //         yield return new WaitForSeconds(0.2f);
        //         var list = CheckBestAngleForBooster(BubblePuzzleLogic.I.booster);
        //         list = list.OrderByDescending(s => s.bubbles.Count).ToList();
        //         direct = GetDirectFromAngle(list[0].angle);
        //         isRandom = false;
        //     }
        //
        //     if (isRandom)
        //     {
        //         direct = GetDirectFromAngle(GetRandomDirect());
        //     }
        //
        //     BubblePuzzleLogic.I.Aim(BubblePuzzleLogic.I.currentShootHolder.position + direct);
        //     yield return new WaitForSeconds(0.2f);
        //     BubblePuzzleLogic.I.HideAim();
        //     BubblePuzzleLogic.I._isAiming = true;
        //     BubblePuzzleLogic.I.MouseUp(BubblePuzzleLogic.I.currentShootHolder.position + direct);
        //     yield return new WaitUntil(() => _canNext);
        //     yield return new WaitForSeconds(0.5f);
        //     yield return null;
        // }
    }

    private static readonly RaycastHit2D[] _raycastHit2Ds = new RaycastHit2D[10];

    private int GetCloudDirect()
    {
        List<int> angles = new List<int>();
        for (int i = 30; i <= 150; i += deltaAngle)
        {
            BubblePuzzleLogic.I.Aim(BubblePuzzleLogic.I.currentShootHolder.position + GetDirectFromAngle(i));
            var lastPoint = BubblePuzzleLogic.I.aimController.lastPoint;

            var hitCount = Physics2D.CircleCastNonAlloc(lastPoint, 1, Vector2.zero, _raycastHit2Ds, 0,
                BubblePuzzleLogic.I.ballNeighborLayer);
            for (int j = 0; j < hitCount; j++)
            {
                var bubbleHit = _raycastHit2Ds[j].collider.GetComponent<Bubble>();
                if (bubbleHit.ballInfo.T == BubbleType.Cloud)
                {
                    angles.Add(i);
                    break;
                }
            }
        }

        // if (angles.Count > 0)
        // {
        //     return angles.RandomItem();
        // }

        return -1;
    }

    private int GetRandomDirect()
    {
        List<int> angles = new List<int>();
        for (int i = 30; i <= 150; i += deltaAngle)
        {
            BubblePuzzleLogic.I.Aim(BubblePuzzleLogic.I.currentShootHolder.position + GetDirectFromAngle(i));
            var lastPoint = BubblePuzzleLogic.I.aimController.lastPoint;

            var hitCount = Physics2D.CircleCastNonAlloc(lastPoint, 1, Vector2.zero, _raycastHit2Ds, 0,
                BubblePuzzleLogic.I.ballNeighborLayer);
            for (int j = 0; j < hitCount; j++)
            {
                // var bubbleHit = _raycastHit2Ds[j].collider.GetComponent<Bubble>();
                angles.Add(i);
            }
        }

        // if (angles.Count > 0)
        // {
        //     return angles.RandomItem();
        // }

        return -1;
    }

    private List<AngleToShootInfo> CheckBestAngleForBooster(Bubble boosterUi)
    {
        List<AngleToShootInfo> list = new List<AngleToShootInfo>();
        for (int i = 30; i <= 150; i += deltaAngle)
        {
            if (boosterUi.ballInfo.T == BubbleType.Fire)
            {
                BubblePuzzleLogic.I.Aim(BubblePuzzleLogic.I.currentShootHolder.position + GetDirectFromAngle(i));
                list.Add(new AngleToShootInfo
                {
                    angle = i,
                    bubbles = BubblePuzzleLogic.I.GetListNeighborsForBoosterBomb(BubblePuzzleLogic.I.aimController
                        .lastPoint)
                });
            }
            else if (boosterUi.ballInfo.T == BubbleType.Beam)
            {
                list.Add(new AngleToShootInfo
                {
                    angle = i,
                    bubbles = BubblePuzzleLogic.I.GetListNeighborsForBoosterBeam(
                        BubblePuzzleLogic.I.currentShootHolder.position, GetDirectFromAngle(i))
                });
            }
        }

        return list;
    }

    private Vector3 GetDirectFromAngle(float angle)
    {
        angle *= Mathf.Deg2Rad;
        return new Vector3(Mathf.Cos(angle), Mathf.Sin(angle)).normalized;
    }
}

public class AngleToShootInfo
{
    public int angle;
    public List<Bubble> bubbles;
}