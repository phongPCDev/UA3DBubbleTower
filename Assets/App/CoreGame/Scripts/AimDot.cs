using UnityEngine;

public class AimDot : MonoBehaviour
{
    private SpriteRenderer _render;
    public int Id { get; set; }
    public Transform Trans { get; set; }

    private void Awake()
    {
        _render = GetComponent<SpriteRenderer>();
        Trans = transform;
    }

    public void SetSprite(Sprite sprite)
    {
        _render.sprite = sprite;
    }

    public void SetPosition(Vector3 position)
    {
        Trans.position = position;
    }

    public void SetAlpha(float alpha)
    {
        var color = _render.color;
        color.a = alpha;
        _render.color = color;
    }
}