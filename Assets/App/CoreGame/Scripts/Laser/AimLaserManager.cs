using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimLaserManager : MonoBehaviour, IAim
{
    [SerializeField] private AimLaser beamPrefab;
    [SerializeField] private List<Color> lineColors;

    private List<AimLaser> laserBeams = new List<AimLaser>();
    private int _beamIndex;
    private Color currentColor;

    private int lastLoopCount = -1;

    private AimController aimController;

    private void Awake()
    {
        aimController = GetComponent<AimController>();
        if (aimController.iAim == null)
            aimController.iAim = this;
    }

    private AimLaser GetBeam()
    {
        if (_beamIndex < laserBeams.Count)
        {
            return laserBeams[_beamIndex];
        }
        else
        {
            var laserBeam = Instantiate(beamPrefab, transform);
            laserBeams.Add(laserBeam);
            laserBeam.SetColor(currentColor, currentColor);
            return laserBeam;
        }
    }

    public void BeginDraw()
    {
        if (_beamIndex - 1 <= lastLoopCount)
        {
            for (int i = _beamIndex; i < laserBeams.Count; i++)
            {
                laserBeams[i].gameObject.SetActive(false);
            }
        }
        _beamIndex = 0;
    }

    public void Draw(Vector3 startPosition, Vector3 endPosition, Vector3 direct, bool needExtend, int maxDot)
    {
        if (!BubblePuzzleLogic.I._isAiming)
        {
            return;
        }
        var laserBeam = GetBeam();
        laserBeam.gameObject.SetActive(true);
        laserBeam.SetPositions(startPosition, endPosition);
        SetLaserBeamsColor();
        _beamIndex += 1;
        lastLoopCount = aimController.LoopCount;
    }

    public void EndDraw()
    {

    }

    public void Hide()
    {
        for (int i = 0; i < laserBeams.Count; i++)
        {
            laserBeams[i].gameObject.SetActive(false);
        }
    }

    public void SetLine(int id, bool isBooster)
    {
        if(id >=0 && id < lineColors.Count)
        {
            currentColor = lineColors[id];
            SetLaserBeamsColor();
        }
        else
        {
            Debug.LogError($"[LINE]: No color of index {id} is assigned!");
        }
    }

    private void SetLaserBeamsColor()
    {
        for (int i = 0; i < laserBeams.Count; i++)
        {
            laserBeams[i].SetColor(currentColor, currentColor);
        }
    }

}
