using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimLaser : MonoBehaviour
{
    public LineRenderer ColorLine;
    public LineRenderer WhiteLine;

    public void SetPositions(Vector3 firstPosition, Vector3 secondPosition)
    {
        ColorLine.SetPosition(0, firstPosition);
        ColorLine.SetPosition(1, secondPosition);

        WhiteLine.SetPosition(0, firstPosition);
        WhiteLine.SetPosition(1, secondPosition);
    }

    public void SetColor(Color startColor, Color endColor)
    {
        ColorLine.startColor = startColor;
        ColorLine.endColor = endColor;
    }
}
