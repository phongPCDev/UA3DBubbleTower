using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

public partial class Bubble : MonoBehaviour
{
    public Action OnNormalClear;

    public static Dictionary<BubbleType, string> bubbleName = new Dictionary<BubbleType, string>()
    {
        { BubbleType.Metal, "obs_metal" },
        { BubbleType.Frozen, "obs_frozen" },
        { BubbleType.BlackHole, "obs_hole" },
        { BubbleType.Morph, "obs_morph" },
        { BubbleType.Lightning, "ba_lightning" },
        { BubbleType.Fire, "ba_bomb" },
        { BubbleType.Rainbow, "ba_rainbow" },
        { BubbleType.Beam, "ba_beam" },
        { BubbleType.Cloud, "nt_cloud" },
        { BubbleType.BlackHoleOn, "obs_hole_on_off" },
        { BubbleType.BlackHoleOff, "obs_hole_on_off" },
        { BubbleType.Plus, "bnf_plus" },
        { BubbleType.Minus, "obs_minus" },
        { BubbleType.Paint, "obs_paint" },
        { BubbleType.Rocket, "ba_beam_bg" },
        { BubbleType.ThunderBolt, "pw_bolt_full" },
        { BubbleType.ColorBrush, "pw_brush_full" },
        { BubbleType.Gem, "nt_gem_bg" },
        { BubbleType.Chain, "obs_chain" },
    };

    private static readonly RaycastHit2D[] _raycastHit2Ds = new RaycastHit2D[10];
    private static readonly RaycastHit[] _raycastHit = new RaycastHit[10];
    private static readonly Collider[] colliderHit = new Collider[10];

    public static bool IsClearPhysic;

    public static RandomRangeInfo ClearShiftHeightRange;
    public static float ClearShiftTime = 0.1f;
    public static bool IsUpdateOnCurve = false;

    public static float ClearPhysicsDrag = 0.1f;
    public static float ClearPhysicsMass = 0.1f;



    public BallInfo ballInfo;
    public Transform center;
    public GameObject centerObj;
    public SpriteRenderer bubbleIn;
    public SpriteRenderer bubble;
    public SpriteRenderer bubbleUp;
    public List<MeshRenderer> bubbleMeshs;
    public Rigidbody rigid;
    public SphereCollider col;
    public LayerMask onBoardLayer;
    // public Transform trail;
    public Bubble AgentCache { get; set; }

    public bool isRoot;
    public bool hasConnectedWithRoot;
    public List<Bubble> neighbors;

    public float clearDelay;
    public bool isInClearList;
    public bool isLoseConnectOnMatched;
    public bool isConnectChecked;
    public BallState state;
    public Transform bubbleObject;
    public Transform rotateObject;
    public int duoC1;
    public int duoC2;
    public bool isLive;
    public bool canTarget;
    public bool isPowerIgnore;
    public BlackHoleController blackHole;
    public bool isDrop;
    public bool ignoreCombo;
    public bool ignoreMulti;
    private int comboCount;
    public bool canCombo { get; set; }
    [SerializeField] private Dictionary<SpriteRenderer, bool> _colorDict = new Dictionary<SpriteRenderer, bool>();
    public List<Transform> objects = new List<Transform>();

    public Transform Trans { get; set; }


    public Vector3 Pos
    {
        get => Trans.position;
        set => Trans.position = value;
    }

    private void Awake()
    {
        Trans = transform;
    }

    private void OnEnable()
    {
        AgentCache = null;
        clearDelay = 0;
        ignoreMulti = false;
        isDrop = false;
        isLive = true;
        isRoot = false;
        state = BallState.None;
        neighbors.Clear();
        Trans.DOKill();
        Trans.localScale = Vector3.one;
        ResetAlpha();
        //center.gameObject.SetActive(true);
        transform.eulerAngles = Vector3.zero;
        center.localPosition = Vector3.zero;
        OnNormalClear = null;
    }

    private void OnDisable()
    {
        RemoveObject();
    }

    public void RemoveObject()
    {
        RemoveTrail();
        foreach (var o in objects)
        {
            if (o.gameObject.activeInHierarchy)
            {
                Destroy(o.gameObject);
            }
        }

        objects.Clear();
    }

    private void OnDestroy()
    {
        transform.DOKill();
    }

    public void RemoveTrail()
    {

        if (bubbleObject != null && bubbleObject.gameObject.activeInHierarchy)
        {
            Destroy(bubbleObject.gameObject);
        }

        bubbleObject = null;

        if (blackHole != null && blackHole.gameObject.activeInHierarchy)
        {
            Destroy(blackHole.gameObject);
        }

        blackHole = null;
    }

    private void ResetAlpha()
    {

    }

    private void Update()
    {
        if (state == BallState.Drop || state == BallState.Clear)
        {
            UpdateDrop();
        }
        else if (state == BallState.SequenceDrop)
        {
            UpdateWinDrop();
        }
        Trans.forward = -(Camera.main.transform.position - Pos).normalized;
        //if (Camera.main.WorldToScreenPoint(Pos).x > Screen.width + 200)
        //    Destroy(gameObject);
    }

    public void SetData(BallInfo ballInfo, bool isKeepRigid = false, bool isResetName = false)
    {
        if (!isKeepRigid && rigid != null)
        {
            Destroy(rigid);
            rigid = null;
        }

        canCombo = false;
        ignoreCombo = false;
        isPowerIgnore = false;
        canTarget = true;
        bubbleObject = null;
        _colorDict.Clear();
        duoC2 = duoC1 = -1;

        this.ballInfo = ballInfo;
        var type = ballInfo.T;
#if UNITY_EDITOR
        if (isResetName) name = string.Empty;
        name += $"Bubble _{type} _ {ballInfo.C} [{BubblePuzzleLogic.BallCount++}] ";
#endif
        switch (type)
        {
            case BubbleType.Normal:
                SetNormal();
                break;
            case BubbleType.Metal:
                SetMetal();
                break;
            case BubbleType.Frozen:
                SetIce();
                break;
            case BubbleType.BlackHole:
                SetSpike();
                break;
            case BubbleType.Morph:
                SetMorph();
                break;
            case BubbleType.GhostEnable:
            case BubbleType.GhostDisable:
                SetGhost();
                break;
            case BubbleType.Cloud:
                SetCloud();
                break;
            case BubbleType.Wood:
                SetWood();
                break;
            case BubbleType.BlackHoleOn:
            case BubbleType.BlackHoleOff:
                SetSwitchSpike();
                break;
            case BubbleType.Minus:
                SetMinus();
                break;
            case BubbleType.Plus:
                SetPlus();
                break;
            case BubbleType.Duo:
                SetDuo();
                break;
            case BubbleType.Paint:
                SetPaint();
                break;
            case BubbleType.Chain:
                SetChain();
                break;
            case BubbleType.Fire:
                SetFire();
                break;
            case BubbleType.Rainbow:
                SetRainbow();
                break;
            case BubbleType.Beam:
                SetLaser();
                break;
            case BubbleType.Lightning:
                SetLighter();
                break;
            case BubbleType.Coin:
                SetCoin();
                break;
            case BubbleType.Gem:
                SetGem();
                break;
            case BubbleType.Rocket:
                SetRocket();
                break;
            case BubbleType.ThunderBolt:
                SetThunderBolt();
                break;
            case BubbleType.ColorBrush:
                SetColorBrush();
                break;
            default:
                Debug.LogError($"$Need handle for {type}, id = {ballInfo.C}");
                break;
        }

        SetCondition();
    }

    private void RandomColor()
    {
        ballInfo.C = BubblePuzzleLogic.I.GetRandomColorOnConfigList();
        SetMaterialColor(BubbleMaterialManager.GetColor(ballInfo.C));
        bubble.sprite = SpriteManager.GetNormalSprite(ballInfo.C);
        if (!_colorDict.ContainsKey(bubble))
            _colorDict.Add(bubble, false);
    }

    private void PlusMinusRandomColor()
    {
        ballInfo.C = BubblePuzzleLogic.I.GetRandomColorOnConfigList();
        SetMaterialColor(BubbleMaterialManager.GetColor(ballInfo.C));
        bubble.sprite = SpriteManager.GetNormalSprite(ballInfo.C);
        if (!_colorDict.ContainsKey(bubbleIn))
            _colorDict.Add(bubbleIn, false);
    }

    private void SetMaterialColor(Material material)
    {
        foreach (var mesh in bubbleMeshs)
        {
            mesh.material = BubbleMaterialManager.GetColor(ballInfo.C);
        }

    }

    #region Set Bubble

    public void UpdateSprite()
    {
        foreach (var b in _colorDict)
        {
            if (b.Key.gameObject.activeSelf)
            {
                if (ballInfo.T == BubbleType.Plus)
                {
                    Debug.Log("A");
                }

                if (ballInfo.T == BubbleType.Duo || ballInfo.C >= 10)
                {
                    b.Key.sprite = b.Value ? SpriteManager.GetDuoSprite(duoC2) : SpriteManager.GetNormalSprite(duoC1);
                }
                else
                {
                    b.Key.sprite = b.Value
                        ? SpriteManager.GetDuoSprite(duoC2)
                        : SpriteManager.GetNormalSprite(ballInfo.C);
                }
            }
        }
    }

    private void SetNormal()
    {
        var c = ballInfo.C;
        if (c == 99)
        {
            RandomColor();
        }
        else if (c >= 10) //Dou color
        {
            duoC1 = c / 10;
            duoC2 = c - duoC1 * 10;

            SetMaterialColor(BubbleMaterialManager.GetColor(duoC1));

            //   if (!_colorDict.ContainsKey(bubble))_colorDict.Add(bubble, false);

            SetMaterialColor(BubbleMaterialManager.GetColor(duoC2));

            //   if (!_colorDict.ContainsKey(bubbleUp)) _colorDict.Add(bubbleUp, true);
            BubblePuzzleLogic.I.AddColor(duoC1);
            BubblePuzzleLogic.I.AddColor(duoC2);
            return;
        }
        else
        {
            SetMaterialColor(BubbleMaterialManager.GetColor(ballInfo.C));

            //  if (!_colorDict.ContainsKey(bubble)) _colorDict.Add(bubble, false);
        }

        AddColor();
    }

    private void SetMetal()
    {
        bubble.gameObject.SetActive(true);
        bubble.sprite = SpriteManager.GetSprite(bubbleName[BubbleType.Metal]);
    }

    private void SetIce()
    {
        bubble.gameObject.SetActive(true);
        bubble.sprite = SpriteManager.GetSprite(bubbleName[BubbleType.Frozen]);
        bubbleObject = PoolWrapper.I.GetIceShine(center);
        bubbleObject.transform.localScale = Vector3.one;
    }

    private void SetSpike()
    {
        blackHole = PoolWrapper.I.GetBlackHole(center);
        blackHole.transform.localScale = Vector3.one;
    }

    private void SetMorph()
    {
        bubbleIn.gameObject.SetActive(true);
        bubble.gameObject.SetActive(true);
        bubble.sprite = SpriteManager.GetSprite(bubbleName[BubbleType.Morph]);
        if (ballInfo.C == 99)
        {
            Debug.Log($"99 Morphy {gameObject.name}");
            ballInfo.C = BubblePuzzleLogic.I.GetRandomColorOnConfigList();
        }

#if !UNITY_STANDALONE
            ballInfo.C = BubblePuzzleLogic.I.GetRandomColorOnConfigList();
#else
        var isRandomMorph = PlayerPrefs.GetInt("isRandomMorph", 0) == 1;
        if (isRandomMorph)
        {
            ballInfo.C = BubblePuzzleLogic.I.GetRandomColorOnConfigList();
        }
#endif
        AddColor();
        bubbleIn.sprite = SpriteManager.GetNormalSprite(ballInfo.C);
        if (!_colorDict.ContainsKey(bubbleIn))
            _colorDict.Add(bubbleIn, false);
        BubblePuzzleLogic.I.AddMorphBubble(this);
    }

    private void SetGhost()
    {
        bubble.gameObject.SetActive(true);
        bubbleUp.gameObject.SetActive(true);
        if (ballInfo.C == 99)
        {
            ballInfo.C = BubblePuzzleLogic.I.GetRandomColorOnConfigList();
        }

        bubble.sprite = SpriteManager.GetNormalSprite(ballInfo.C);
        if (!_colorDict.ContainsKey(bubble))
            _colorDict.Add(bubble, false);
        if (ballInfo.T != BubbleType.GhostEnable)
        {
            bubble.color = new Color(1, 1, 1, PuzzleConfig.GhostAlpha);
            gameObject.layer = LayerDefine.BallHidden;
            bubbleUp.sprite = SpriteManager.GetSprite(SpriteManager.obs_ghost_off);
        }
        else
        {
            gameObject.layer = LayerDefine.BallOnBoard;
            bubbleUp.sprite = SpriteManager.GetSprite(SpriteManager.obs_ghost_on);
        }

        BubblePuzzleLogic.I.AddGhostBubble(this);
        AddColor();
    }

    private void SetCloud()
    {
        isRoot = true;
        bubbleObject = PoolWrapper.I.GetCloud(center);
        bubbleObject.localScale = Vector3.one;
        BubblePuzzleLogic.I.AddCloudBubble(this);
    }

    private void SetWood()
    {
        bubble.gameObject.SetActive(true);
        bubble.sprite = SpriteManager.GetWoodSprite(ballInfo.C);
    }

    private void SetSwitchSpike()
    {
        blackHole = PoolWrapper.I.GetBlackHole2(center);
        BubblePuzzleLogic.I.AddSwitchSpikeBubble(this);
        blackHole.transform.localScale = Vector3.one;

        if (ballInfo.T == BubbleType.BlackHoleOff)
        {
            blackHole.sprite.color = new Color(1, 1, 1, 0.2f);
            blackHole.fx.SetActive(false);
        }
        else
        {
            blackHole.sprite.color = Color.white;
            blackHole.fx.SetActive(true);
        }
    }

    private void SetMinus()
    {
        if (ballInfo.C == 99)
        {
            PlusMinusRandomColor();
        }

        AddColor();
        bubbleIn.gameObject.SetActive(true);
        bubble.gameObject.SetActive(true);
        bubbleIn.sprite = SpriteManager.GetNormalSprite(ballInfo.C);
        if (!_colorDict.ContainsKey(bubbleIn))
            _colorDict.Add(bubbleIn, false);
        bubble.sprite = SpriteManager.GetMinusSprite();
    }

    private void SetPlus()
    {
        if (ballInfo.C == 99)
        {
            PlusMinusRandomColor();
        }

        AddColor();
        bubbleIn.gameObject.SetActive(true);
        bubble.gameObject.SetActive(true);
        bubbleIn.sprite = SpriteManager.GetNormalSprite(ballInfo.C);
        if (!_colorDict.ContainsKey(bubbleIn))
            _colorDict.Add(bubbleIn, false);
        bubble.sprite = SpriteManager.GetPlusSprite();
    }

    private void SetDuo()
    {
        bubbleUp.gameObject.SetActive(true);
        bubble.gameObject.SetActive(true);

        if (ballInfo.C == 99)
        {
            duoC1 = BubblePuzzleLogic.I.GetRandomColorOnConfigList();
            while (duoC1 == 0)
            {
                duoC1 = BubblePuzzleLogic.I.GetRandomColorOnConfigList();
            }

            duoC2 = BubblePuzzleLogic.I.GetRandomColorOnConfigList();
            while (duoC2 == duoC1)
            {
                duoC2 = BubblePuzzleLogic.I.GetRandomColorOnConfigList();
            }

            ballInfo.C = duoC1 * 10 + duoC2;
        }
        else
        {
            duoC1 = ballInfo.C / 10;
            duoC2 = ballInfo.C - duoC1 * 10;
        }

        BubblePuzzleLogic.I.AddColor(duoC1);
        BubblePuzzleLogic.I.AddColor(duoC2);
        bubble.sprite = SpriteManager.GetNormalSprite(duoC1);
        if (!_colorDict.ContainsKey(bubble))
            _colorDict.Add(bubble, false);
        bubbleUp.sprite = SpriteManager.GetDuoSprite(duoC2);
        if (!_colorDict.ContainsKey(bubbleUp))
            _colorDict.Add(bubbleUp, false);
        ballInfo.T = BubbleType.Normal;
    }

    private void SetPaint()
    {
        bubbleIn.gameObject.SetActive(true);
        bubble.gameObject.SetActive(true);
        bubble.sprite = SpriteManager.GetSprite(bubbleName[BubbleType.Paint]);
        if (ballInfo.C == 99)
        {
            ballInfo.C = BubblePuzzleLogic.I.GetRandomColorOnConfigList();
        }

        bubbleIn.sprite = SpriteManager.GetNormalSprite(ballInfo.C);
        if (!_colorDict.ContainsKey(bubbleIn))
            _colorDict.Add(bubbleIn, false);
        AddColor();
    }

    private void SetChain()
    {
        bubbleIn.gameObject.SetActive(true);
        bubble.gameObject.SetActive(true);
        bubble.sprite = SpriteManager.GetSprite(bubbleName[BubbleType.Chain]);
        if (ballInfo.C == 99)
        {
            ballInfo.C = BubblePuzzleLogic.I.GetRandomColorOnConfigList();
        }

        bubbleIn.sprite = SpriteManager.GetNormalSprite(ballInfo.C);
        if (!_colorDict.ContainsKey(bubbleIn))
            _colorDict.Add(bubbleIn, false);
        AddColor();
    }

    private void SetFire()
    {
        bubble.gameObject.SetActive(true);
        bubble.sprite = SpriteManager.GetSprite(bubbleName[BubbleType.Fire]);
    }

    private void SetLaser()
    {
        bubble.gameObject.SetActive(true);
        bubble.sprite = SpriteManager.GetSprite(bubbleName[BubbleType.Beam]);
        // bubbleIn.sprite = SpriteManager.GetSprite(bubbleName[BubbleType.Beam]);
        // bubbleIn.transform.eulerAngles = new Vector3(0, 0, ballInfo.C);
        //
        // bubble.gameObject.SetActive(true);
        // bubble.sprite = SpriteManager.GetSprite(SpriteManager.beam1_arrow);
        // bubble.transform.eulerAngles = new Vector3(0, 0, ballInfo.C);
    }

    private void SetRainbow()
    {
        bubble.gameObject.SetActive(true);
        bubble.sprite = SpriteManager.GetSprite(bubbleName[BubbleType.Rainbow]);
    }

    private void SetLighter()
    {
        bubble.gameObject.SetActive(true);
        bubble.sprite = SpriteManager.GetSprite(bubbleName[BubbleType.Lightning]);
    }

    private void SetCoin()
    {
        bubbleObject = PoolWrapper.I.bubbleGem.GetFxPool(center);
        bubbleObject.transform.localScale = Vector3.one;
        bubbleObject.transform.localPosition = Vector3.zero;
        var gem = bubbleObject.GetComponent<BubbleGem>();
        gem.SetDataCoin();
    }

    private void SetGem()
    {
        AddColor();
        bubbleObject = PoolWrapper.I.bubbleGem.GetFxPool(center);
        bubbleObject.transform.localScale = Vector3.one;
        bubbleObject.transform.localPosition = Vector3.zero;
        var gem = bubbleObject.GetComponent<BubbleGem>();
        gem.SetData(ballInfo.C);
        AddColorDict(gem.GetIcon());
    }

    public void AddColorDict(SpriteRenderer renderer, bool isDouble = false)
    {
        if (!_colorDict.ContainsKey(renderer))
            _colorDict.Add(renderer, isDouble);
    }

    private void SetRocket()
    {
        BubblePuzzleLogic.I.AddBeamRocket(this);
        bubbleObject = PoolWrapper.I.GetFxObject(PoolWrapper.PowerRocket, Trans.position, center);
        bubbleObject.transform.localScale = Vector3.one;
        rotateObject = bubbleObject.transform.GetChild(0).Find("Icon");
        rotateObject.eulerAngles = new Vector3(0, 0, ballInfo.C);
    }

    private void SetColorBrush()
    {
        bubbleObject = PoolWrapper.I.GetFxObject(PoolWrapper.PowerColorBrush, Trans.position, center);
        bubbleObject.transform.localScale = Vector3.one;
    }

    private void SetThunderBolt()
    {
        bubbleObject = PoolWrapper.I.GetFxObject(PoolWrapper.PowerThunderBolt, Trans.position, center);
        bubbleObject.transform.localScale = Vector3.one;
    }

    #endregion

    #region INTERACT AFTER SHOOT

    public void MorphRandomColor()
    {
        var t = bubbleIn.transform;
        RemoveColor();
        ballInfo.C = BubblePuzzleLogic.I.GetRandomColorOnConfigList(false, isInsideView: true);
        AddColor();
        t.DOScale(0, .2f).OnComplete(() =>
        {
            bubbleIn.sprite = SpriteManager.GetNormalSprite(ballInfo.C);
            if (!_colorDict.ContainsKey(bubbleIn))
                _colorDict.Add(bubbleIn, false);
            t.localScale = Vector3.zero;
            t.DOScale(1, .2f).SetTarget(Trans);
        });
    }

    public void SwitchSpikeSwitch()
    {
        if (ballInfo.T == BubbleType.BlackHoleOn)
        {
            ballInfo.T = BubbleType.BlackHoleOff;
            blackHole.sprite.DOFade(PuzzleConfig.GhostAlpha, 0.25f).SetEase(Ease.Linear).SetTarget(Trans);
            blackHole.fx.SetActive(false);
        }
        else
        {
            ballInfo.T = BubbleType.BlackHoleOn;
            blackHole.sprite.DOFade(1, 0.25f).SetEase(Ease.Linear).SetTarget(Trans);
            blackHole.fx.SetActive(true);
        }
    }

    public void RocketRotate()
    {
        ballInfo.C -= 60;
        if (ballInfo.C < 0)
        {
            ballInfo.C += 360;
        }

        rotateObject.DORotate(new Vector3(0, 0, ballInfo.C), 0.1f).SetTarget(Trans);
    }

    public void BubbleGhostSwitchState()
    {
        if (ballInfo.T == BubbleType.GhostEnable)
        {
            ballInfo.T = BubbleType.GhostDisable;
            bubble.DOFade(PuzzleConfig.GhostAlpha, 0.2f).SetTarget(Trans);
            bubbleUp.sprite = SpriteManager.GetSprite(SpriteManager.obs_ghost_off);
            bubble.gameObject.layer = gameObject.layer = LayerDefine.BallHidden;
        }
        else if (ballInfo.T == BubbleType.GhostDisable)
        {
            ballInfo.T = BubbleType.GhostEnable;
            bubble.DOFade(1, 0.2f).SetTarget(Trans);
            bubbleUp.sprite = SpriteManager.GetSprite(SpriteManager.obs_ghost_on);
            bubble.gameObject.layer = gameObject.layer = LayerDefine.BallOnBoard;
        }
        else
        {
#if UNITY_EDITOR
            Debug.LogError($"Need handle this case T = {ballInfo.T}");
#endif
        }
#if UNITY_EDITOR
        name = $"Bubble _{ballInfo.T} _ {ballInfo.C}";
#endif
        UpdateBoosterCanDestroy();
    }

    public void InteractWithNeighbors(Bubble agent, BubbleType type, int c)
    {
        var t = ballInfo.T;
        if (!(t == BubbleType.Normal || t == BubbleType.Duo || t == BubbleType.GhostEnable || t == BubbleType.Morph
              || t == BubbleType.Plus || t == BubbleType.Minus || t == BubbleType.Cloud || t == BubbleType.Wood))
            return;
        foreach (var neighbor in neighbors)
        {
            if (neighbor.ballInfo.T == BubbleType.Frozen)
            {
                if (!neighbor.isDrop)
                {
                    neighbor.ShowIceBreakFx();
                    neighbor.SetData(new BallInfo { C = c, T = 0 });
                }
            }
            else if (ballInfo.T == BubbleType.Paint && neighbor.IsPaintCanChange)
            {
                neighbor.ShowPaintChainFx();
            }
        }
    }

    public void InteractWithNeighbors_NoMatch()
    {
        if (!isLive) return;
        foreach (var neighbor in neighbors)
        {
            if (neighbor.ballInfo.T == BubbleType.BlackHole || neighbor.ballInfo.T == BubbleType.BlackHoleOn)
            {
                RemoveConnections(BubbleType.Normal, false);
                BubblePuzzleLogic.I.isBlackHoldRunning = true;
                BubblePuzzleLogic.I.RemoveBubble(this, false);
                Trans.DOScale(.4f, 0.2f);
                Trans.DOMove(neighbor.Trans.position, 0.2f).OnComplete(() =>
                {
                    BubblePuzzleLogic.I.RemoveColor(ballInfo.C);
                    BubblePuzzleLogic.I.isBlackHoldRunning = false;
                    Destroy(gameObject);
                });
                return;
            }
        }
    }

    #endregion

    #region Drop

    public void Drop(int combo, Bubble agent)
    {
        AgentCache = agent;
        if (ballInfo.T == BubbleType.Gem || ballInfo.T == BubbleType.Coin)
        {
            state = BallState.None;
            BubblePuzzleLogic.I._dropCount--;
            BubblePuzzleLogic.OnClearColor(ballInfo.T, ballInfo.C);
            BubblePuzzleLogic.I.RemoveBubbleFromBoard(this);

            if (ballInfo.T == BubbleType.Gem)
            {
                DestroyBubble_Gem();
            }
            else if (ballInfo.T == BubbleType.Coin)
            {
                DestroyBubble_Coin();
            }

            return;
        }

        RemoveColor();
        neighbors.Clear();
        comboCount = combo;
        Trans.SetParent(null);
        state = BallState.Drop;
        AddRigid();

        rigid.velocity = new Vector3(
            Random.Range(BubblePuzzleLogic.I.dropSpeedScaleX.min, BubblePuzzleLogic.I.dropSpeedScaleX.max),
            Random.Range(BubblePuzzleLogic.I.dropSpeedScaleY.min, BubblePuzzleLogic.I.dropSpeedScaleY.max),
            Random.Range(BubblePuzzleLogic.I.dropSpeedScaleZ.min, BubblePuzzleLogic.I.dropSpeedScaleZ.max));
        gameObject.layer = LayerDefine.BallColliction;
    }

    private void AddRigid()
    {
        if (rigid == null)
        {
            rigid = gameObject.AddComponent<Rigidbody>();
        }

        rigid.isKinematic = false;

        if (IsClearPhysic)
        {
            rigid.mass = ClearPhysicsMass;
            //rigid.constraints = RigidbodyConstraints.FreezeRotation;

            rigid.drag = 50;
            DOVirtual.Float(50, ClearPhysicsDrag, .2f, (v) => { rigid.drag = v; }).SetEase(Ease.OutQuad).SetTarget(Trans);
        }
    }

    private void UpdateDrop()
    {
        if (Trans.position.y < BubblePuzzleLogic.I.dropCheckPoint.position.y)
        {
            BubblePuzzleLogic.I._dropCount--;
            BubblePuzzleLogic.OnClearColor?.Invoke(ballInfo.T, ballInfo.C);
            state = BallState.None;
            BubblePuzzleLogic.PuzzleSound?.PlayDropCollectedSound();
            rigid.isKinematic = true;
            rigid.velocity = Vector2.zero;
            gameObject.layer = LayerDefine.BallDrop;
            var score = BubbleScore.dropScore[ballInfo.T] * comboCount * 2;
            // PoolWrapper.I.GetBubbleScore(Trans.position, score);
            BubblePuzzleLogic.PlusScore(score, true, Trans.position);
            FadeAll();
        }
    }

    private void UpdateWinDrop()
    {
        if (rigid == null)
        {
            AddRigid();
        }

        if (rigid.velocity.y <= 0)
        {
            state = BallState.None;
            BubblePuzzleLogic.PuzzleSound?.PlayShootOnWinSound();
            ShowMatchFx();
            SpawnScore_WinDrop();
            Trans.DOScale(1.5f, 0.1f).OnComplete(() => { Destroy(gameObject); });
        }
    }


    private void FadeAll()
    {
        if (IsClearPhysic)
        {
            Destroy(gameObject);
            return;
        }
        DOVirtual.DelayedCall(0.21f, () =>
        {
            ResetAlpha();
            if (IsCanCollect())
            {
                Trans.DOMove(BubblePuzzleLogic.I.boosterFireHolderPosition.position, 0.3f);

            }

            Trans.DOScale(0.2f, .3f).OnComplete(() =>
            {
                Destroy(gameObject);
                if (IsCanCollect())
                    BubblePuzzleLogic.OnDropCollect?.Invoke();
            });
        }).SetTarget(Trans);
    }

    private bool IsCanCollect()
    {
        return PuzzleState.CanCollectBomb && (AgentCache == null || !AgentCache.IsBooster);
    }

    #endregion

    #region Clear & Interact

    #region DESTROY BUBBLE

    public IEnumerator DestroyBubble(Bubble agent, int color, BubbleType clearType, bool onMatching)
    {
        //BubblePuzzleLogic.I.RemoveBubbleFromBoard(this);
        BubblePuzzleLogic.I.clearCount++;
        if (clearDelay > 0)
        {
            yield return new WaitForSeconds(clearDelay);
            // yield return new WaitForSeconds(clearDelay + Random.Range(0, BubblePuzzleLogic.I.randomDelay));
        }
        BubblePuzzleLogic.I.clearCount--;
        if (!IsClearByBooster(clearType) && ballInfo.T != BubbleType.Coin && !IsPower)
        {
            if (agent.ballInfo.T == BubbleType.ThunderBolt)
            {
                InteractWithNeighbors(this, clearType, ballInfo.C);
            }
            else
            {
                InteractWithNeighbors(this, clearType, color);
            }
        }

        BubblePuzzleLogic.PuzzleSound?.PlayClearSound();
        var type = ballInfo.T;
        switch (type)
        {
            case BubbleType.Morph:
            case BubbleType.Normal:
            case BubbleType.Duo:
                if (IsClearPhysic)
                {
                    DestroyBubble_DropPhysics(onMatching);
                }
                else
                {
                    DestroyBubble_Color(color);
                }

                break;
            case BubbleType.Paint:
                DestroyBubble_Paint(clearType, color, onMatching);
                break;
            case BubbleType.GhostEnable:
                if (onMatching || agent.IsPower)
                    DestroyBubble_Color(color);
                break;
            case BubbleType.GhostDisable:
                if (!onMatching)
                    DestroyBubble_Color(color);
                break;
            case BubbleType.Cloud:
                DestroyBubble_Cloud();
                break;
            case BubbleType.Minus:
                DestroyBubble_Color(color);
                BubblePuzzleLogic.I.SetShootRemain(BubblePuzzleLogic.I.GetShootRemain() - 2);
                PoolWrapper.I.GetFxPlusMinus(Trans.position + Vector3.up * .5f).SetMinus();
                break;
            case BubbleType.Plus:
                DestroyBubble_Color(color);
                BubblePuzzleLogic.I.SetShootRemain(BubblePuzzleLogic.I.GetShootRemain() + 2);
                PoolWrapper.I.GetFxPlusMinus(Trans.position + Vector3.up * .5f).SetPlus();
                break;
            case BubbleType.Chain:
                RemoveColor();
                DestroyBubble_Chain();
                break;
            case BubbleType.Wood:
                DestroyBubble_Wood();
                break;
            case BubbleType.Frozen:
                if (IsClearByBooster(clearType))
                    DestroyBubble_Ice();
                break;
            case BubbleType.BlackHole:
            case BubbleType.BlackHoleOn:
            case BubbleType.Metal:
                break;
            case BubbleType.Coin:
                DestroyBubble_Coin();
                break;
            case BubbleType.Gem:
                DestroyBubble_Gem();
                break;
            case BubbleType.Fire:
                DestroyBubble_Fire(agent, clearType);
                break;
            case BubbleType.Lightning:
                DestroyBubble_Lighting(agent, clearType);
                break;
            case BubbleType.ThunderBolt:
                {
                    DestroyBubble_ThunderBolt(agent);
                }
                break;
            case BubbleType.Beam:
                DestroyBubble_Beam(agent);
                break;
            case BubbleType.Rocket:
                DestroyBubble_Rocket(agent);
                break;
            case BubbleType.Rainbow:
                DestroyBubble_Rainbow(agent);
                break;
            case BubbleType.ColorBrush:
                DestroyBubble_ColorBrush(agent, clearType, color, false);
                break;
        }

        if (!isLive)
        {
            if (agent != null && agent.ballInfo.T == BubbleType.Rocket)
            {
                PoolWrapper.I.GetFxObject(PoolWrapper.Fx_BoosterFireMatching, Pos);
            }

            BubblePuzzleLogic.I.StartCoroutine(CRSpawnScore());
            //BubblePuzzleLogic.I.StartCoroutine(BubblePuzzleLogic.I.CheckDrop(this));
        }
    }

    private bool IsClearByBooster(BubbleType type)
    {
        return type == BubbleType.Beam || type == BubbleType.Lightning ||
               type == BubbleType.Rainbow || type == BubbleType.Fire || type == BubbleType.Rocket ||
               // type == BubbleType.ThunderBolt ||
               type == BubbleType.ColorBrush;
    }

    public bool IsColorBrushCanChange()
    {
        var t = ballInfo.T;
        return isLive && t == BubbleType.Normal || t == BubbleType.Frozen
                                                || t == BubbleType.Morph || t == BubbleType.Chain || t == BubbleType.Duo || t == BubbleType.Minus
            || t == BubbleType.Plus || t == BubbleType.GhostEnable || t == BubbleType.GhostDisable;
    }

    public void DestroyBubble_Wood()
    {
        PoolWrapper.I.GetFxWoodBreak(Pos);
        if (ballInfo.C > 0)
        {
            ballInfo.C--;
            bubble.sprite = SpriteManager.GetWoodSprite(ballInfo.C);
            isLive = true;
        }
        else
        {
            BubblePuzzleLogic.I.RemoveBubble(this);
            SpawnScore();
        }
    }

    public void DestroyBubble_Color(int color)
    {
        if (color < 0)
        {
            isLive = false;
            var type = (BubbleType)Mathf.Abs(color);
            if (ballInfo.C >= 10)
            {
                BubblePuzzleLogic.I.RemoveColor(duoC1);
                BubblePuzzleLogic.I.RemoveColor(duoC2);
                ballInfo.C = duoC1;
                BubblePuzzleLogic.OnClearColor?.Invoke(ballInfo.T, duoC1);
                BubblePuzzleLogic.OnClearColor?.Invoke(ballInfo.T, duoC2);
                BubblePuzzleLogic.OnColorChanged?.Invoke();
            }
            else
            {
                if (ballInfo.C >= 0)
                {
                    BubblePuzzleLogic.I.RemoveColor(ballInfo.C);
                    BubblePuzzleLogic.OnColorChanged.Invoke();
                    BubblePuzzleLogic.OnClearColor?.Invoke(ballInfo.T, ballInfo.C);
                    OnNormalClear?.Invoke();
                }
            }

            if (type == BubbleType.Fire)
            {
                Trans.SetParent(null);
                PoolWrapper.I.GetFxObject(PoolWrapper.Fx_BoosterFireMatching, Pos);
                Trans.DOKill();
                Trans.DOScale(1.5f, BubblePuzzleLogic.I.ballScaleDuration)
                    .OnComplete(() => { Destroy(gameObject); });
            }
            else if (type == BubbleType.Rainbow || type == BubbleType.ColorBrush)
            {
                Trans.SetParent(null);
                PoolWrapper.I.GetFxObject(PoolWrapper.Fx_RainbowMatching, Pos);
                Trans.DOScale(1.5f, BubblePuzzleLogic.I.ballScaleDuration)
                    .OnComplete(() => { Destroy(gameObject); });
            }
            else
            {
                ShowMatchFx();
                Trans.SetParent(null);
                Trans.DOKill();
                Trans.DOScale(1.5f, BubblePuzzleLogic.I.ballScaleDuration)
                    .OnComplete(() => { Destroy(gameObject); });
            }

            BubblePuzzleLogic.I.StartCoroutine(CRSpawnScore());
        }
        else
        {
            if (ballInfo.C >= 10) // Dou color
            {
                ballInfo.C = color;
                BubblePuzzleLogic.OnClearColor?.Invoke(ballInfo.T, ballInfo.C);
                BubblePuzzleLogic.I.RemoveColor(color);
                if (color == duoC1)
                {
                    ballInfo.C = duoC2;
                }
                else
                {
                    ballInfo.C = duoC1;
                }
                ShowMatchFx(color);

                ballInfo.T = BubbleType.Normal;
                bubbleUp.gameObject.SetActive(false);
                bubble.sprite = SpriteManager.GetNormalSprite(ballInfo.C);
                UpdateNeighbors();
                BubblePuzzleLogic.I.StartCoroutine(CRSpawnScore());
                UpdateDuo();
                return;
            }

            BubblePuzzleLogic.OnClearColor?.Invoke(ballInfo.T, ballInfo.C);
            RemoveColor();
            ShowMatchFx();
            Trans.DOScale(1.5f, BubblePuzzleLogic.I.ballScaleDuration)
                .OnComplete(() =>
                {
                    if (gameObject.activeInHierarchy)
                    {
                        Destroy(gameObject);
                    }
                });
            isLive = false;
            OnNormalClear?.Invoke();
        }
    }

    private void DestroyBubble_DropPhysics(bool onMatching)
    {
        state = BallState.Clear;
        FallBubble();
        RemoveConnections(BubbleType.Normal, false);
        BubblePuzzleLogic.I.RemoveColor(ballInfo.C);
        BubblePuzzleLogic.OnColorChanged?.Invoke();
        BubblePuzzleLogic.OnClearColor?.Invoke(ballInfo.T, ballInfo.C);
        BubblePuzzleLogic.I.RemoveBubbleFromBoard(this);
        OnNormalClear?.Invoke();
    }


    public void FallBubble()
    {
        gameObject.layer = LayerDefine.BallWeighted;
        Trans.SetParent(null);
        AddRigid();
    }


    private void DestroyBubble_Cloud()
    {
        PoolWrapper.I.GetFxCloudDead(Trans.position);
        Destroy(gameObject);
    }

    private void DestroyBubble_Chain()
    {
        ballInfo.T = BubbleType.Normal;
        SetData(ballInfo);
        PoolWrapper.I.GetFxObject(PoolWrapper.Fx_ChainCracker, Pos, Trans);
    }

    #region BOOSTER_INGAME

    private void DestroyBubble_Fire(Bubble agent, BubbleType clearType)
    {
        isLive = false;
        var hits = Physics2D.CircleCastAll(Pos, 2.6f, Vector2.zero, 0, onBoardLayer);
        int randomColor = BubblePuzzleLogic.I.GetRandomColorOnConfigList();
        foreach (var hit2D in hits)
        {
            if (hit2D.collider.gameObject != gameObject)
            {
                var bubble = hit2D.collider.GetComponent<Bubble>();
                if (bubble.isLive)
                {
                    if (Vector2.Distance(Pos, bubble.Pos) <= 2.1f)
                    {
                        bubble.RemoveConnections(ballInfo.T);
                        bubble.clearDelay = clearDelay + Vector2.Distance(Pos, bubble.Pos) / 50;
                        bubble.isLive = false;
                        bubble.StartCoroutine(bubble.DestroyBubble(agent, -(int)ballInfo.T, ballInfo.T, false));
                    }
                    else
                    {
                        if (clearType == BubbleType.Rainbow && bubble.IsBoosterCanChangeColor)
                        {
                            bubble.ChangeBallInfo(BubbleType.Normal, randomColor);
                        }
                    }
                }
            }
        }

        gameObject.layer = LayerDefine.BallDrop;
        BubblePuzzleLogic.I.RemoveBubble(this, false);
        PoolWrapper.I.GetFxObject(PoolWrapper.Fx_BoosterFireActive, Pos).localScale = Vector3.one;
        Trans.SetParent(null);
        Trans.DOScale(1.5f, BubblePuzzleLogic.I.ballScaleDuration).OnComplete(() => { Destroy(gameObject); });
    }

    private void DestroyBubble_Lighting(Bubble agent, BubbleType clearType)
    {
        isLive = false;
        PoolWrapper.I.GetFxLightingActive(Pos);
        var hits = Physics2D.LinecastAll(Pos + Vector3.left * 100, Pos + Vector3.right * 100, onBoardLayer);
        foreach (var hit2D in hits)
        {
            if (hit2D.collider.gameObject != gameObject)
            {
                var bubble = hit2D.collider.GetComponent<Bubble>();
                if (bubble.isLive && !bubble.IsRainbow)
                {
                    bubble.RemoveConnections(ballInfo.T);
                    bubble.clearDelay = 0;
                    bubble.isLive = false;
                    bubble.StartCoroutine(bubble.DestroyBubble(agent, -(int)ballInfo.T, BubbleType.Lightning, false));
                }
            }
        }

        if (clearType == BubbleType.Lightning && Vector2.Distance(agent.Pos, Pos) <= 1.01f)
        {
            var p = Pos + Vector3.up * BubblePuzzleLogic.I.GetRowDistance();
            var hits2 = Physics2D.LinecastAll(p + Vector3.left * 100, p + Vector3.right * 100, onBoardLayer);
            foreach (var hit2D in hits2)
            {
                if (hit2D.collider.gameObject != gameObject)
                {
                    var bubble = hit2D.collider.GetComponent<Bubble>();
                    if (bubble.isLive && !bubble.IsRainbow)
                    {
                        bubble.RemoveConnections(ballInfo.T);
                        bubble.clearDelay = 0;
                        bubble.isLive = false;
                        bubble.StartCoroutine(
                            bubble.DestroyBubble(agent, -(int)ballInfo.T, BubbleType.Lightning, false));
                    }
                }
            }

            PoolWrapper.I.GetFxLightingActive(p);

            p = Pos + Vector3.down * BubblePuzzleLogic.I.GetRowDistance();
            hits2 = Physics2D.LinecastAll(p + Vector3.left * 100, p + Vector3.right * 100, onBoardLayer);
            foreach (var hit2D in hits2)
            {
                if (hit2D.collider.gameObject != gameObject)
                {
                    var bubble = hit2D.collider.GetComponent<Bubble>();
                    if (bubble.isLive && !bubble.IsRainbow)
                    {
                        bubble.RemoveConnections(ballInfo.T);
                        bubble.clearDelay = 0;
                        bubble.isLive = false;
                        bubble.StartCoroutine(
                            bubble.DestroyBubble(agent, -(int)ballInfo.T, BubbleType.Lightning, false));
                    }
                }
            }

            PoolWrapper.I.GetFxLightingActive(p);
        }

        gameObject.layer = LayerDefine.BallDrop;
        BubblePuzzleLogic.I.RemoveBubble(this, false);
        PoolWrapper.I.GetFxObject(PoolWrapper.Fx_BoosterFireActive, Pos).localScale = Vector3.one;
        Trans.SetParent(null);
        Trans.DOScale(1.5f, BubblePuzzleLogic.I.ballScaleDuration).OnComplete(() => { Destroy(gameObject); });
        BubblePuzzleLogic.PuzzleSound?.PlayLightingSound();
    }


    private void DestroyBubble_Beam(Bubble agent)
    {
        if (ballInfo.T == BubbleType.Rocket)
        {
            BubblePuzzleLogic.I.RemoveRocketBubble(this);
            BubblePuzzleLogic.I.specialBallActiveCount--;
        }

        isLive = false;
        PoolWrapper.I.GetFxObject(PoolWrapper.FX_ObjBeam, Pos).eulerAngles = new Vector3(0, 0, ballInfo.C);
        float angle = (ballInfo.C + 90) * Mathf.Deg2Rad;
        var hits = Physics2D.CircleCastAll(Pos, 0.1f, new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)), 5, onBoardLayer);
        foreach (var hit2D in hits)
        {
            if (hit2D.collider.gameObject != gameObject)
            {
                var bubble = hit2D.collider.GetComponent<Bubble>();
                if (bubble.isLive && !bubble.IsRainbow)
                {
                    bubble.RemoveConnections(ballInfo.T);
                    bubble.clearDelay = 0;
                    bubble.isLive = false;
                    bubble.StartCoroutine(bubble.DestroyBubble(agent, -(int)ballInfo.T, ballInfo.T, false));
                }
            }
        }

        gameObject.layer = LayerDefine.BallDrop;
        BubblePuzzleLogic.I.RemoveBubble(this, false);
        PoolWrapper.I.GetFxObject(PoolWrapper.Fx_BoosterFireActive, Pos).localScale = Vector3.one;
        Trans.SetParent(null);
        Trans.DOScale(1.5f, BubblePuzzleLogic.I.ballScaleDuration).OnComplete(() => { Destroy(gameObject); });
        BubblePuzzleLogic.PuzzleSound?.PlayBeamSound();
    }

    private void DestroyBubble_Rocket(Bubble agent)
    {
        BubblePuzzleLogic.I.RemoveBubbleFromBoard(this);
        isLive = false;
        if (!ignoreCombo && agent != null && agent.canCombo && agent.IsPower)
        {
            if (agent.ballInfo.T == BubbleType.ThunderBolt)
            {
                var target = GetTargetThunderBoltBubble(true);
                if (target != null)
                {
                    BubblePuzzleLogic.I.specialBallActiveCount++;
                    target.RemoveConnections(ballInfo.T, true);
                    target.isLive = false;
                    target.isPowerIgnore = true;
                    target.canTarget = false;

                    var combo = PoolWrapper.I.GetFxObject(PoolWrapper.ComboThunderBolt_Rocket, transform.position);
                    var path = new Vector3[4];
                    var startPos = Pos;
                    var hitPos = target.Pos;
                    transform.position = startPos;

                    path[0] = startPos;
                    path[1] = (startPos * 2 + hitPos) / 3 + Vector3.right * Random.Range(-0.1f, 0.1f);
                    path[2] = (startPos + hitPos * 2) / 3 + Vector3.right * Random.Range(-0.1f, 0.1f);
                    path[3] = hitPos;
                    transform.localScale = Vector3.one;
                    Sequence s = DOTween.Sequence();
                    s.Insert(0, combo.DOScale(1.3f, 0.2f));
                    s.Insert(0.2f, combo.DOScale(1, 0.2f));
                    s.Insert(0.2f, combo.DOPath(path, 1, PathType.CatmullRom).SetEase(Ease.OutQuad));
                    s.OnComplete(() =>
                    {
                        Destroy(combo.gameObject);
                        for (int i = 0; i < 4; i++)
                        {
                            RocketActive(i * 90, target.Pos, target);
                        }

                        target.StartCoroutine(target.DestroyBubble(this, 0, BubbleType.ThunderBolt, false));
                        DOVirtual.DelayedCall(1, () => { BubblePuzzleLogic.I.specialBallActiveCount--; }).SetTarget(Trans);
                    }).SetTarget(Trans);
                }
            }
            else if (agent.ballInfo.T == BubbleType.Rocket)
            {
                BubblePuzzleLogic.I.specialBallActiveCount++;
                for (int i = 0; i < 360; i += 60)
                {
                    RocketActive(i + 30, Pos, this);
                }

                DOVirtual.DelayedCall(1, () => { BubblePuzzleLogic.I.specialBallActiveCount--; }).SetTarget(Trans);
            }
            else if (agent.ballInfo.T == BubbleType.ColorBrush)
            {
                BubblePuzzleLogic.I.specialBallActiveCount++;
                List<Bubble> convertedList = new List<Bubble>();
                foreach (var neighbor in neighbors)
                {
                    if (neighbor != this && neighbor.isLive && neighbor.IsColorBrushCanChange())
                    {
                        neighbor.RemoveOnChangeBubbleType();
                        var newBallInfo = new BallInfo { T = BubbleType.Rocket };
                        newBallInfo.C = 30 + 60 * Random.Range(0, 6);

                        neighbor.SetData(newBallInfo);
                        convertedList.Add(neighbor);
                        neighbor.ignoreCombo = true;
                        neighbor.canTarget = false;
                        neighbor.isLive = false;
                        neighbor.RemoveConnections(ballInfo.T, true);
                    }
                }

                RocketActive(ballInfo.C, Pos, this);
                foreach (var power in convertedList)
                {
                    power.StartCoroutine(power.DestroyBubble(this, 0, this.ballInfo.T, false));
                }

                DOVirtual.DelayedCall(1, () => { BubblePuzzleLogic.I.specialBallActiveCount--; }).SetTarget(Trans);
            }
        }
        else
        {
            BubblePuzzleLogic.I.specialBallActiveCount++;
            BubblePuzzleLogic.PuzzleSound?.PlayRocketActiveSound();
            RocketActive(ballInfo.C, Pos, this);
            DOVirtual.DelayedCall(1, () => { BubblePuzzleLogic.I.specialBallActiveCount--; }).SetTarget(Trans);
        }

        gameObject.layer = LayerDefine.BallDrop;
        BubblePuzzleLogic.I.RemoveBubble(this, false);
        Trans.SetParent(null);
        Trans.DOScale(1.5f, BubblePuzzleLogic.I.ballScaleDuration).OnComplete(() => { Destroy(gameObject); });
    }

    private void RocketActive(float angle, Vector3 pos, Bubble origin)
    {
        var radianAngle = (angle + 90) * Mathf.Deg2Rad;
        var hits = Physics2D.CircleCastAll(pos, 0.1f, new Vector2(Mathf.Cos(radianAngle), Mathf.Sin(radianAngle)), 5,
            onBoardLayer);
        var fx = PoolWrapper.I.GetFxObject(PoolWrapper.Fx_PowerRocket, pos);
        fx.eulerAngles = new Vector3(0, 0, angle);
        fx.GetComponent<FxPowerRocket>().SetData(hits, this);
    }

    public void DestroyBubble_Rainbow(Bubble agent)
    {
        var bubbles = BubblePuzzleLogic.I.GetMatchingWithRainbowBubble(this);
        foreach (var bubble1 in bubbles)
        {
            if (bubble1.isLive)
            {
                bubble1.isLive = false;
                bubble1.StartCoroutine(bubble1.DestroyBubble(agent, -(int)ballInfo.T, ballInfo.T, false));
            }
        }

        gameObject.layer = LayerDefine.BallDrop;
        BubblePuzzleLogic.I.RemoveBubble(this, false);
        PoolWrapper.I.GetFxObject(PoolWrapper.Fx_RainbowMatching, Pos).localScale = Vector3.one;
        Trans.SetParent(null);
        Trans.DOScale(1.5f, BubblePuzzleLogic.I.ballScaleDuration).OnComplete(() => { Destroy(gameObject); });
        BubblePuzzleLogic.PuzzleSound?.PlayRainbowSound();
    }

    public void BombCombineColorBrush()
    {
        isLive = false;
        var hits2 = Physics2D.CircleCastAll(Pos, 2.6f, Vector2.zero, 0, onBoardLayer);
        int randomColor = BubblePuzzleLogic.I.GetRandomColorOnConfigList();
        foreach (var hit2D in hits2)
        {
            if (hit2D.collider.gameObject != gameObject)
            {
                var bubble = hit2D.collider.GetComponent<Bubble>();
                if (bubble.isLive)
                {
                    if (Vector2.Distance(Pos, bubble.Pos) <= 2.1f)
                    {
                    }
                    else
                    {
                        if (bubble.IsBoosterCanChangeColor)
                        {
                            bubble.ChangeBallInfo(BubbleType.Normal, randomColor);
                        }
                    }
                }
            }
        }

        BubblePuzzleLogic.OnBubbleListChanged?.Invoke();
        BubblePuzzleLogic.PuzzleSound?.PlayFireSound();
    }

    private void DestroyBubble_ColorBrush(Bubble agent, BubbleType clearType, int c, bool onHit = false)
    {
        BubblePuzzleLogic.I.RemoveBubbleFromBoard(this);
        isLive = false;
        gameObject.layer = LayerDefine.BallDrop;
        BubblePuzzleLogic.I.RemoveBubble(this, false);
        Trans.SetParent(null);
        Trans.DOScale(1.5f, BubblePuzzleLogic.I.ballScaleDuration).OnComplete(() => { Destroy(gameObject); });
        BubblePuzzleLogic.OnBubbleListChanged?.Invoke();
        var fx = PoolWrapper.I.GetFxObject(PoolWrapper.Fx_PowerColorBrush, Pos)
            .GetComponent<PowerColorBrushController>();
        BubblePuzzleLogic.PuzzleSound?.PlayColorBrushSound();

        RaycastHit2D[] hits = null;

        if (!ignoreCombo)
        {
            if (agent != null && agent.canCombo && agent.IsPower)
            {
                if (agent.ballInfo.T == BubbleType.ThunderBolt || agent.ballInfo.T == BubbleType.Rocket)
                {
                    hits = Physics2D.CircleCastAll(Pos, .6f, Vector2.zero, 0, onBoardLayer);
                    fx.PlayFx(this, agent.ballInfo.T, c, hits, onHit);
                }
                else if (agent.ballInfo.T == BubbleType.ColorBrush)
                {
                    hits = Physics2D.CircleCastAll(Pos, 4.6f, Vector2.zero, 0, onBoardLayer);
                    fx.PlayFx(this, c, hits, onHit);
                }
            }
            else
            {
                hits = Physics2D.CircleCastAll(Pos, 1.6f, Vector2.zero, 0, onBoardLayer);
                fx.PlayFx(this, BubblePuzzleLogic.I.lastColor, hits, onHit);
            }
        }
        else
        {
            if (clearType != BubbleType.Rainbow)
            {
                hits = Physics2D.CircleCastAll(Pos, 2f, Vector2.zero, 0, onBoardLayer);
            }

            if (clearType == BubbleType.Fire || clearType == BubbleType.Lightning || clearType == BubbleType.Beam || clearType == BubbleType.Rainbow)
            {
                c = BubblePuzzleLogic.I.GetRandomColorOnConfigList(false);
            }

            fx.PlayFx(this, c, hits, onHit);
        }

        if (agent != null)
        {
            agent.canCombo = false;
        }
    }

    private void DestroyBubble_Paint(BubbleType t, int c, bool onHit = false)
    {
        RemoveColor();
        if (t != BubbleType.Normal && !IsClearByBooster(t) && t != BubbleType.ThunderBolt) return;
        List<Bubble> bubbles = new List<Bubble>(neighbors);
        foreach (var neighbor in bubbles)
        {
            if (neighbor.ballInfo.T == BubbleType.Frozen)
            {
                neighbor.ChangeBallInfo(BubbleType.Normal, ballInfo.C);
            }
            else if (neighbor.IsDou)
            {
                neighbor.ChangeDuoBubble(ballInfo.C);
            }
            else if (neighbor.IsPaintCanInteract)
            {
                if (!(neighbor.ballInfo.T == BubbleType.Normal && neighbor.ballInfo.C == c))
                {
                    neighbor.ChangeBallInfo(BubbleType.Normal, BubblePuzzleLogic.I.GetRandomColorOnConfigList(false));
                }
            }

            neighbor.neighbors.Remove(this);
        }

        BubblePuzzleLogic.OnBubbleListChanged?.Invoke();
        PoolWrapper.I.GetFxObject(PoolWrapper.Fx_BoosterFireActive, Pos);
        Destroy(gameObject);
    }

    private void ChangeDuoBubble(int clearColor)
    {
        BubblePuzzleLogic.I.RemoveColor(duoC1);
        BubblePuzzleLogic.I.RemoveColor(duoC2);
        if (duoC1 == clearColor)
        {
            duoC2 = BubblePuzzleLogic.I.GetRandomColorOnConfigList(false);
        }
        else if (duoC2 == clearColor)
        {
            duoC1 = BubblePuzzleLogic.I.GetRandomColorOnConfigList(false);
            while (duoC1 == 0)
            {
                duoC1 = BubblePuzzleLogic.I.GetRandomColorOnConfigList();
            }
        }
        else
        {
            duoC1 = BubblePuzzleLogic.I.GetRandomColorOnConfigList();
            while (duoC1 == 0)
            {
                duoC1 = BubblePuzzleLogic.I.GetRandomColorOnConfigList();
            }

            duoC2 = BubblePuzzleLogic.I.GetRandomColorOnConfigList();
            while (duoC2 == duoC1)
            {
                duoC2 = BubblePuzzleLogic.I.GetRandomColorOnConfigList();
            }
        }
        ballInfo.C = duoC1 * 10 + duoC2;
        SetData(ballInfo);
        var obj = PoolWrapper.I.GetFxObject(PoolWrapper.Fx_PaintChange, Pos, Trans);
        obj.localScale = Vector3.one;
        objects.Add(obj);
    }

    public void ChangeBallInfo(BubbleType t, int c)
    {
        if (ballInfo.T == BubbleType.Morph)
        {
            BubblePuzzleLogic.I.RemoveMorphBubble(this);
        }
        else if (ballInfo.T == BubbleType.GhostEnable)
        {
            BubblePuzzleLogic.I.RemoveGhostBubble(this);
        }
        else if (ballInfo.T == BubbleType.Chain || ballInfo.T == BubbleType.Plus || ballInfo.T == BubbleType.Minus)
        {
            t = ballInfo.T;
        }

        if (bubbleObject != null)
        {
            Destroy(bubbleObject.gameObject);
            bubbleObject = null;
        }

        if (ballInfo.T == BubbleType.GhostDisable)
        {
            Trans.DOKill();
            bubble.color = Color.white;
            BubblePuzzleLogic.I.RemoveGhostBubble(this);
        }

        if (t == BubbleType.Morph) BubblePuzzleLogic.I.RemoveMorphBubble(this);

        var obj = PoolWrapper.I.GetFxObject(PoolWrapper.Fx_PaintChange, Pos, Trans);
        obj.localScale = Vector3.one;
        objects.Add(obj);
        RemoveColor();
        ballInfo.T = t;
        ballInfo.C = c;
        SetData(ballInfo);
    }

    private void DestroyBubble_Ice()
    {
        Trans.DOKill();
        isLive = false;
        PoolWrapper.I.GetFxObject(PoolWrapper.Fx_Ice_Cracker2, Pos);
        Trans.SetParent(null);
        Trans.DOScale(1.5f, BubblePuzzleLogic.I.ballScaleDuration).OnComplete(() => { Destroy(gameObject); });
    }

    private void DestroyBubble_Coin()
    {
        PoolWrapper.I.GetFxObject(PoolWrapper.Fx_BoosterFireMatching, Pos);
        // Trans.SetParent(null);
        // Trans.DOScale(1.5f, BubblePuzzleLogic.I.ballScaleDuration).OnComplete(() => { ObjectPool.I.Despawn(Trans); });
        bubbleObject.SetParent(null);
        bubbleObject.GetComponent<BubbleGem>().CollectCoin();
        bubbleObject = null;
        Destroy(gameObject);
        BubblePuzzleLogic.OnCollectedCoin?.Invoke();
    }

    private void DestroyBubble_Gem()
    {
        RemoveColor();
        bubbleObject.SetParent(null);
        bubbleObject.GetComponent<BubbleGem>().CollectGem();
        bubbleObject = null;
        Destroy(gameObject);
    }

    private List<List<Bubble>> ThunderBolt_GetGroupBubble(List<Bubble> list)
    {
        List<List<Bubble>> groups = new List<List<Bubble>>();
        while (list.Count > 0)
        {
            var item = list[0];
            List<Bubble> group = new List<Bubble> { item };
            BubblePuzzleLogic.I.GetBubblesMatching(item.neighbors.ToArray(), item.ballInfo.C, ref group);
            foreach (var bubble1 in group)
            {
                list.Remove(bubble1);
            }

            foreach (var bubble1 in group.ToArray())
            {
                if (bubble1.ballInfo.T == BubbleType.Gem || bubble1.ballInfo.T == BubbleType.GhostDisable)
                {
                    group.Remove(bubble1);
                }
                else if (bubble1.Pos.y > PuzzleState.Camera.orthographicSize)
                {
                    group.Remove(bubble1);
                }
            }

            groups.Add(group);
        }

        return groups;
    }

    private Bubble GetTargetThunderBoltBubble(bool isIgnorePriority = false)
    {
        var list = BubblePuzzleLogic.I.ThunderBolt_GetListBubbleInsideScreen();
        list.Remove(this);
        if (list.Count > 0)
        {
            Bubble targetBubble;
            var groups = ThunderBolt_GetGroupBubble(list.ToList());
            if (Random.Range(0, 100) <= 70 && groups.Count > 0)
            {
                groups = groups.OrderByDescending(s => s.Count).ToList();
                groups[0].Shuffle();
                targetBubble = groups[0][0];
            }
            else
            {
                list.Shuffle();
                targetBubble = list[0];

                foreach (var bubble1 in list)
                {
                    if (bubble1.IsBooster && bubble1 != this)
                    {
                        targetBubble = bubble1;
                        break;
                    }
                }
            }

            if (!isIgnorePriority)
            {
                if (BubblePuzzleLogic.I.priorityChooseBooster && !ignoreCombo)
                {
                    foreach (var bubble1 in list)
                    {
                        if (bubble1.IsBooster && bubble1 != this)
                        {
                            targetBubble = bubble1;
                            break;
                        }
                    }
                }
                else if (BubblePuzzleLogic.I.priorityChooseCloud)
                {
                    foreach (var bubble1 in list)
                    {
                        if (bubble1.ballInfo.T == BubbleType.Cloud)
                        {
                            targetBubble = bubble1;
                            break;
                        }
                    }
                }
            }

            return targetBubble;
        }

        return null;
    }

    private void DestroyBubble_ThunderBolt(Bubble agent)
    {
        BubblePuzzleLogic.I.RemoveBubbleFromBoard(this);
        isLive = false;
        canTarget = false;
        if (agent != null && agent.canCombo && agent.IsPower && !ignoreCombo)
        {
            agent.canCombo = false;
            if (agent.ballInfo.T == BubbleType.ThunderBolt || agent.ballInfo.T == BubbleType.Rocket)
            {
                for (int i = 0; i < 3; i++)
                {
                    ActiveComboThunderBoltVsThunderBolt(true);
                }
            }
            else if (agent.ballInfo.T == BubbleType.ColorBrush)
            {
                List<Bubble> changeList = new List<Bubble>();
                foreach (var neighbor in neighbors.ToArray())
                {
                    if (neighbor.IsColorBrushCanChange() && !neighbor.isPowerIgnore && neighbor.canTarget &&
                        neighbor.isLive)
                    {
                        neighbor.RemoveOnChangeBubbleType();
                        neighbor.SetData(new BallInfo { T = BubbleType.ThunderBolt });
                        changeList.Add(neighbor);
                        neighbor.ignoreCombo = true;
                        neighbor.canTarget = false;
                        neighbor.isLive = false;
                        neighbor.RemoveConnections(ballInfo.T, true);
                    }
                }

                foreach (var power in changeList)
                {
                    power.StartCoroutine(power.DestroyBubble(this, ballInfo.C, ballInfo.T, false));
                }

                ActiveComboThunderBoltVsThunderBolt(false);
            }
            else
            {
                ActiveComboThunderBoltVsThunderBolt(false);
            }
        }
        else
        {
            ActiveComboThunderBoltVsThunderBolt(false);
        }

        Trans.SetParent(null);
        Destroy(gameObject);
        BubblePuzzleLogic.PuzzleSound.PlayThunderBoltSound();
    }

    private void ActiveComboThunderBoltVsThunderBolt(bool isIgnorePriority)
    {
        Bubble targetBubble = GetTargetThunderBoltBubble(isIgnorePriority);
        if (targetBubble != null)
        {
            BubblePuzzleLogic.I.specialBallActiveCount++;
            targetBubble.isPowerIgnore = true;
            targetBubble.canTarget = false;
            if (targetBubble.CanDestroyNow(BubbleType.Normal))
            {
                targetBubble.isLive = false;
            }

            List<Bubble> clearList = new List<Bubble>();
            if (targetBubble.CanMatch)
            {
                BubblePuzzleLogic.I.CheckMatches(targetBubble, this, true, 1);
                clearList.AddRange(BubblePuzzleLogic.I._clearList);
            }
            else
            {
                clearList.Add(targetBubble);
            }

            foreach (var clearBubble in clearList)
            {
                clearBubble.RemoveConnections(ballInfo.T, true);
            }

            var fx = PoolWrapper.I.GetFxObject(PoolWrapper.FX_PowerThunderBolt, Pos).GetComponent<FxThunderBolt>();
            fx.InitData(this, targetBubble, Random.Range(-.3f, .3f), () =>
            {
                if (fx.gameObject.activeSelf) Destroy(fx.gameObject);
                foreach (var clearBubble in clearList)
                {
                    int color = targetBubble.ballInfo.C;
                    if (targetBubble.IsDou)
                    {
                        color = targetBubble.duoC1;
                    }

                    clearBubble.StartCoroutine(clearBubble.DestroyBubble(this, -100, BubbleType.ThunderBolt, true));
                }

                BubblePuzzleLogic.I.specialBallActiveCount--;

                BubblePuzzleLogic.I.StartCoroutine(BubblePuzzleLogic.I.CRUpdateAfterDestroyMatched(this, 1));
            });
        }
    }

    #endregion

    public void RemoveConnections(BubbleType type, bool drop = false)
    {
        if (!drop && (!CanDestroyNow(type) || (type == 0 && IsDou))) return;
        BubblePuzzleLogic.I.RemoveBubbleFromBoard(this);
        foreach (var neighbor in neighbors)
        {
            neighbor.neighbors.Remove(this);
        }

        isLive = false;
        canTarget = false;
    }

    #endregion

    private void ShowMatchFx(int inColor = 0)
    {
        int c = ballInfo.C;
        if (IsDou)
        {
            c = inColor;
        }

        PoolWrapper.I.GetMatchedEffect(Trans.position, c);
    }

    public void ShowIceBreakFx()
    {
        if (bubbleObject == null) return;
        Destroy(bubbleObject.gameObject);
        bubbleObject = null;
        PoolWrapper.I.GetIceCrack(Pos, Trans);
    }

    private void ShowPaintChainFx()
    {
        if (state != BallState.Drop)
        {
            RemoveColor();
            ballInfo.C = BubblePuzzleLogic.I.GetRandomColorOnConfigList(false);
            AddColor();
            bubble.sprite = SpriteManager.GetNormalSprite(ballInfo.C);
            if (!_colorDict.ContainsKey(bubble))
                _colorDict.Add(bubble, false);
        }

        var obj = PoolWrapper.I.GetFxObject(PoolWrapper.Fx_PaintChange, Pos, Trans);
        obj.localScale = Vector3.one;
        objects.Add(obj);
    }

    public void DestroyCloudBubble()
    {
        PoolWrapper.I.GetFxCloudDead(Trans.position);
        BubblePuzzleLogic.I.RemoveCloud(this);
        Destroy(bubbleObject.gameObject);
        bubbleObject = null;
        Destroy(gameObject);
    }

    #endregion

    #region Remove & Add Color

    private void AddColor()
    {
#if UNITY_EDITOR
        if (PuzzleState.IsEndGame) return;
#endif
        if (ballInfo.C >= 10 || ballInfo.T == BubbleType.Duo)
        {
            BubblePuzzleLogic.I.AddColor(duoC1);
            BubblePuzzleLogic.I.AddColor(duoC2);
        }
        else
        {
            BubblePuzzleLogic.I.AddColor(ballInfo.C);
        }

        if (PuzzleState.IsPlaying)
            BubblePuzzleLogic.OnColorChanged?.Invoke();
    }

    public void RemoveColor()
    {
#if UNITY_EDITOR
        if (PuzzleState.IsEndGame) return;
#endif
        if (IsDou && ballInfo.T == 0)
        {
            BubblePuzzleLogic.I.RemoveColor(duoC1);
            BubblePuzzleLogic.I.RemoveColor(duoC2);
            return;
        }

        if (CanMatch || ballInfo.T == BubbleType.GhostDisable)
        {
            BubblePuzzleLogic.I.RemoveColor(ballInfo.C);
        }

        if (PuzzleState.IsPlaying)
            BubblePuzzleLogic.OnColorChanged?.Invoke();
    }

    #endregion

    #region Score

    private IEnumerator CRSpawnScore()
    {
        yield return new WaitForEndOfFrame();
        var score = BubbleScore.matchScore[ballInfo.T] * BubblePuzzleLogic.I.comboCount;
        // PoolWrapper.I.GetBubbleScore(Trans.position, score);
        //BubblePuzzleLogic.PlusScore(score, true, Trans.position);
    }

    private void SpawnScore()
    {
        var score = BubbleScore.matchScore[ballInfo.T] * BubblePuzzleLogic.I.comboCount;
        // PoolWrapper.I.GetBubbleScore(Trans.position, score);
        BubblePuzzleLogic.PlusScore(score, true, Trans.position);
    }

    private void SpawnScore_WinDrop()
    {
        var score = 500;
        BubblePuzzleLogic.PlusScore(score, true, Trans.position);
    }

    #endregion


    public void ShootOnWin()
    {
        state = BallState.SequenceDrop;
        gameObject.layer = 12;
        AddRigid();
        //rigid.gravityScale = 8;

        if (Random.Range(0, 100) < 30)
        {
            rigid.velocity = new Vector2(Random.Range(-5f, 5f), Random.Range(20f, 22f)) * 2f;
        }
        else
        {
            rigid.velocity = new Vector2(Random.Range(-4f, 4f), Random.Range(20f, 22f)) * 2f;
        }

        Trans.SetParent(null);
    }

    public void SetOnBoard()
    {
        gameObject.layer = LayerDefine.BallOnBoard;
    }

    public BoosterType BoosterType
    {
        get
        {
            if (ballInfo.T == BubbleType.Fire) return BoosterType.Bomb;
            if (ballInfo.T == BubbleType.Rainbow) return BoosterType.Rainbow;
            if (ballInfo.T == BubbleType.Lightning) return BoosterType.Thunder;
            if (ballInfo.T == BubbleType.Beam) return BoosterType.Laser;
            return BoosterType.None;
        }
    }

    public void PlayHitFx(Vector3 origin)
    {
        Vector3 direct = Pos - origin;
        if (direct.sqrMagnitude >= 0.1f)
        {
            center.DOLocalMove(direct.normalized * (1 / direct.sqrMagnitude / 5), 0.15f).SetRelative(true)
                .SetLoops(2, LoopType.Yoyo).SetTarget(Trans);
        }
    }


    public void UpdateNeighbors()
    {
        neighbors.Clear();
        if (BubblePuzzleLogic.isUse3D)
        {
            var hitCount = Physics.OverlapSphereNonAlloc(Pos, 1, colliderHit, BubblePuzzleLogic.I.ballNeighborLayer);
            for (int i = 0; i < hitCount; i++)
            {
                var bubbleHit = colliderHit[i].GetComponent<Bubble>();
                if (bubbleHit != this && bubbleHit.isLive) neighbors.Add(bubbleHit);
            }
        }
        else
        {
            var hitCount = Physics2D.CircleCastNonAlloc(Pos, 1, Vector2.zero, _raycastHit2Ds, 0,
            BubblePuzzleLogic.I.ballNeighborLayer);
            for (int i = 0; i < hitCount; i++)
            {
                var bubbleHit = _raycastHit2Ds[i].collider.GetComponent<Bubble>();
                if (bubbleHit != this && bubbleHit.isLive) neighbors.Add(bubbleHit);
            }
        }

    }

    public void RemoveOnChangeBubbleType()
    {
        BubblePuzzleLogic.I.RemoveFromSpecialList(this);
        RemoveColor();
        RemoveObject();
    }

    public void SetOrderInLayer(int newOrder)
    {

    }

    #region Editor

    private void OnDrawGizmos()
    {
        if (!BubblePuzzleLogic.useGizmos) return;
        if (Trans == null) return;
        if (isRoot)
        {
            Gizmos.DrawRay(Trans.position, Vector3.up);
        }
        foreach (var neighbor in neighbors)
        {
            if (neighbor != null)
                Gizmos.DrawRay(Trans.position, (neighbor.Trans.position - Trans.position).normalized * .4f);
        }
    }

    #endregion

}
