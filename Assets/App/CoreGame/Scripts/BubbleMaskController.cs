using System;
using System.Collections;
using System.Collections.Generic;
using AFramework.ExtensionMethods;
using UnityEngine;
using Random = UnityEngine.Random;

public class BubbleMaskController : MonoBehaviour
{
    public SpriteRenderer eyeLeft;
    public SpriteRenderer eyeRight;
    public SpriteRenderer mouth;

    public List<FaceRule> rules;

    public Sprite eyeOpen;
    public Sprite eyeClose;
    public Sprite mouthOpen;
    public Sprite mouthClose;
    private float _countTime;
    private float _timeToChange1 = 1f;
    private bool _isOpen = true;
    private FaceRule rule;


    private void OnEnable()
    {
        _countTime = Random.Range(0, _timeToChange1);
        _isOpen = true;
        rule = rules.RandomItem();
        SetState1();
    }

    private void Update()
    {
        _countTime += Time.deltaTime;
        if (_isOpen)
        {
            if (_countTime > _timeToChange1)
            {
                _countTime = 0;
                SetState2();
                _isOpen = false;
            }
        }
        else
        {
            if (_countTime > 0.2f)
            {
                _countTime = 0;
                _isOpen = true;
                SetState1();
            }
        }
    }

    private void SetState1()
    {
        eyeLeft.sprite = rule.eyeLeft1 == FaceType.Open ? eyeOpen : eyeClose;
        eyeRight.sprite = rule.eyeRight1 == FaceType.Open ? eyeOpen : eyeClose;
        mouth.sprite = rule.mouth1 == FaceType.Open ? mouthOpen : mouthClose;
    }

    private void SetState2()
    {
        eyeLeft.sprite = rule.eyeLeft2 == FaceType.Open ? eyeOpen : eyeClose;
        eyeRight.sprite = rule.eyeRight2 == FaceType.Open ? eyeOpen : eyeClose;
        mouth.sprite = rule.mouth2 == FaceType.Open ? mouthOpen : mouthClose;
    }

    public void SetEnable()
    {
        eyeLeft.color = Color.white;
        eyeRight.color = Color.white;
        mouth.color = Color.white;
    }

    public void SetDisable()
    {
        eyeLeft.SetAlpha(PuzzleConfig.GhostAlpha);
        eyeRight.SetAlpha(PuzzleConfig.GhostAlpha);
        mouth.SetAlpha(PuzzleConfig.GhostAlpha);
    }
}

[Serializable]
public class FaceRule
{
    public FaceType eyeLeft1;
    public FaceType eyeRight1;
    public FaceType mouth1;

    public FaceType eyeLeft2;
    public FaceType eyeRight2;
    public FaceType mouth2;
}

public enum FaceType
{
    Open,
    Close,
}