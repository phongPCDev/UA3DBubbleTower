
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Serialization;

public class AimDotManager : MonoBehaviour, IAim
{
    [SerializeField] private GameObject aimDotPrefab;

    [SerializeField] private List<Sprite> colorSprites;
    [SerializeField] private List<Sprite> boosterSprites;
    [SerializeField] private float aimDotDistance;

    [SerializeField] private float tweenDuration = 0.5f;

    [SerializeField] private float minScale = 0.3f;

    [SerializeField] private float maxScale = 1;

    [SerializeField] private Ease tweenEasy;

    public List<AimDot> _aimDots = new List<AimDot>();
    private int _dotIndex;
    private Tween _scale1Tween;
    private Tween _scale2Tween;
    private Tween _alphaTween;
    private float _scale1Value;
    private float _scale2Value;
    private Sprite _currentSprite;
    private bool isTween;
    private float currentAlpha;
    private float max = -1;
    private void Awake()
    {
        if (GetComponent<AimController>().iAim == null)
            GetComponent<AimController>().iAim = this;
        CreateTween();
    }

    private void OnEnable()
    {
        _scale1Tween.Restart();
        _scale2Tween.Restart();
    }

    private void OnDisable()
    {
        _scale1Tween.Pause();
        _scale2Tween.Pause();
    }

    private void CreateTween()
    {
        _scale1Tween?.Kill();
        _scale2Tween?.Kill();
        _scale1Tween = DOVirtual.Float(minScale, maxScale, tweenDuration, value => { _scale1Value = value; })
            .SetLoops(-1, LoopType.Yoyo).SetEase(tweenEasy).SetAutoKill(false);
        _scale2Tween = DOVirtual.Float(maxScale, minScale, tweenDuration, value => { _scale2Value = value; })
            .SetLoops(-1, LoopType.Yoyo).SetEase(tweenEasy).SetAutoKill(false);
        _alphaTween = DOVirtual.Float(0, 1, .1f, value =>
        {
            currentAlpha = value;
            foreach (var aimDot in _aimDots)
            {
                if (aimDot.gameObject.activeInHierarchy)
                    aimDot.SetAlpha(value);
            }
        }).SetEase(Ease.Linear).SetAutoKill(false);
    }

    private void Update()
    {
        int count = _aimDots.Count;
        if (max < 0)
        {
            var scale1 = new Vector3(_scale1Value, _scale1Value, _scale1Value);
            var scale2 = new Vector3(_scale2Value, _scale2Value, _scale2Value);
            for (int i = 0; i < count; i += 1)
            {
                if (i < _dotIndex)
                {
                    if (_aimDots[i].Id == 1)
                    {
                        _aimDots[i].Trans.localScale = scale1;
                    }
                    else
                    {
                        _aimDots[i].Trans.localScale = scale2;
                    }
                }
                else
                {
                    return;
                }
            }
        }
        else
        {
            var scale1 = new Vector3(minScale, minScale, minScale);
            for (int i = 0; i < count; i += 1)
            {
                if (i < _dotIndex)
                {
                    _aimDots[i].Trans.localScale = scale1;
                }
                else
                {
                    return;
                }
            }
        }
    }

    private AimDot GetAimDot()
    {
        if (_dotIndex < _aimDots.Count)
        {
            return _aimDots[_dotIndex];
        }
        else
        {
            var aimDot = Instantiate(aimDotPrefab, transform).GetComponent<AimDot>();
            _aimDots.Add(aimDot);
            aimDot.SetSprite(_currentSprite);
            return aimDot;
        }
    }

    public void BeginDraw()
    {
        _dotIndex = 0;
    }

    public void Draw(Vector3 startPosition, Vector3 endPosition, Vector3 direct, bool needExtend, int maxDot)
    {
        var distance = aimDotDistance;
        float bonus = 0;
        if (maxDot == 3)
        {
            distance = aimDotDistance * .8f;
            bonus = 0.2f;
        }
        max = maxDot;
        if (!isTween)
        {
            foreach (var aim in _aimDots)
            {
                aim.SetAlpha(0);
            }

            isTween = true;
            _alphaTween.Restart();
        }

        var length = Vector2.Distance(endPosition, startPosition);
        if (needExtend) length += distance * 0.5f;
        int amountOfDot = (int)(length / distance);
        int id = 1;
        for (int i = 0; i <= amountOfDot; i++)
        {
            AimDot dot = null;
            if (_dotIndex >= 1)
            {
                dot = GetAimDot();
                dot.SetPosition(startPosition + direct * (i * distance) + bonus * direct);
                dot.gameObject.SetActive(true);
                dot.SetAlpha(currentAlpha);
                dot.Id = id;
            }

            id = -id;
            _dotIndex++;
            if (maxDot > 0 && _dotIndex > maxDot)
            {
                break;
            }

            if (i == 0 && dot != null)
            {
                var dotEnd = GetAimDot();
                dotEnd.SetPosition(dot.Trans.position);
                dotEnd.gameObject.SetActive(true);
                dotEnd.SetAlpha(currentAlpha);
                dotEnd.Id = -dot.Id;
                _dotIndex++;
            }
        }
    }

    public void EndDraw()
    {
        int count = _aimDots.Count;
        for (int i = _dotIndex; i < count; i++) _aimDots[i].gameObject.SetActive(false);
    }

    public void Hide()
    {
        int count = _aimDots.Count;
        for (int i = 0; i < count; i++) _aimDots[i].gameObject.SetActive(false);
        isTween = false;
    }

    public void SetLine(int id, bool isBooster)
    {
        var listSprites = isBooster ? boosterSprites : colorSprites;
        if (_currentSprite == listSprites[id]) return;
        _currentSprite = listSprites[id];
        int count = _aimDots.Count;
        for (int i = 0; i < count; i++) _aimDots[i].SetSprite(_currentSprite);
    }
}