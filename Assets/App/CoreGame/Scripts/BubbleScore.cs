using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleScore
{
    public static Dictionary<BubbleType, int> matchScore;
    public static Dictionary<BubbleType, int> dropScore;

    public static void InitBubbleScore()
    {
        matchScore = new Dictionary<BubbleType, int>();
        matchScore.Add(BubbleType.Normal, 10);
        matchScore.Add(BubbleType.Metal, 0);
        matchScore.Add(BubbleType.Frozen, 10);
        matchScore.Add(BubbleType.BlackHole, 0);
        matchScore.Add(BubbleType.Morph, 10);
        matchScore.Add(BubbleType.Lightning, 10);
        matchScore.Add(BubbleType.Fire, 10);
        matchScore.Add(BubbleType.Rainbow, 10);
        matchScore.Add(BubbleType.Cloud, 10);
        matchScore.Add(BubbleType.Wood, 10);
        matchScore.Add(BubbleType.Duo, 10);
        matchScore.Add(BubbleType.Beam, 10);
        matchScore.Add(BubbleType.Chain, 10);
        matchScore.Add(BubbleType.GhostEnable, 10);
        matchScore.Add(BubbleType.GhostDisable, 10);
        matchScore.Add(BubbleType.BlackHoleOn, 10);
        matchScore.Add(BubbleType.BlackHoleOff, 10);
        matchScore.Add(BubbleType.Event, 10);
        matchScore.Add(BubbleType.Plus, 10);
        matchScore.Add(BubbleType.Minus, 10);
        matchScore.Add(BubbleType.Paint, 10);
        matchScore.Add(BubbleType.Coin, 10);
        matchScore.Add(BubbleType.Rocket, 10);
        matchScore.Add(BubbleType.ThunderBolt, 10);
        matchScore.Add(BubbleType.ColorBrush, 10);
        matchScore.Add(BubbleType.Gem, 10);

        dropScore = new Dictionary<BubbleType, int>();
        dropScore.Add(BubbleType.Normal, 20);
        dropScore.Add(BubbleType.Metal, 20);
        dropScore.Add(BubbleType.Frozen, 10);
        dropScore.Add(BubbleType.BlackHole, 20);
        dropScore.Add(BubbleType.Morph, 10);
        dropScore.Add(BubbleType.Lightning, 10);
        dropScore.Add(BubbleType.Fire, 10);
        dropScore.Add(BubbleType.Rainbow, 10);
        dropScore.Add(BubbleType.Cloud, 10);
        dropScore.Add(BubbleType.Wood, 10);
        dropScore.Add(BubbleType.Duo, 10);
        dropScore.Add(BubbleType.Beam, 10);
        dropScore.Add(BubbleType.Chain, 10);
        dropScore.Add(BubbleType.GhostEnable, 10);
        dropScore.Add(BubbleType.GhostDisable, 10);
        dropScore.Add(BubbleType.BlackHoleOn, 10);
        dropScore.Add(BubbleType.BlackHoleOff, 10);
        dropScore.Add(BubbleType.Event, 10);
        dropScore.Add(BubbleType.Plus, 10);
        dropScore.Add(BubbleType.Minus, 10);
        dropScore.Add(BubbleType.Paint, 10);
        dropScore.Add(BubbleType.Coin, 10);
        dropScore.Add(BubbleType.Rocket, 10);
        dropScore.Add(BubbleType.ThunderBolt, 10);
        dropScore.Add(BubbleType.ColorBrush, 10);
        dropScore.Add(BubbleType.Gem, 10);
    }
}