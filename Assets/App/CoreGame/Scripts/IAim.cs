using UnityEngine;

public interface IAim
{
   void BeginDraw();
   void Draw(Vector3 startPosition, Vector3 endPosition, Vector3 direct, bool needExtend, int maxDot);
   void EndDraw();
   void Hide();
   void SetLine(int id, bool isBooster);
}
