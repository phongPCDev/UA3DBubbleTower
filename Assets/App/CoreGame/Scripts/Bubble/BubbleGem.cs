using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

public class BubbleGem : MonoBehaviour
{
    public float offsetY = 0.5f;
    public int speed = 2;
    public bool isSpeedBase;
    public AnimationCurve curve;
    [SerializeField] private SpriteRenderer gemIcon;
    [SerializeField] private GameObject bg;
    [SerializeField] GameObject fxOnEndMove;
    private int _color;

    public void SetData(int c)
    {
        ResetData();
        BubblePuzzleLogic.I.gemCount++;
        _color = c;
        gemIcon.sprite = SpriteManager.GetGemSprite(c);
    }

    public void SetDataCoin()
    {
        ResetData();
        _color = 0;
        gemIcon.sprite = SpriteManager.GetSprite(SpriteManager.bn_coin);
    }

    private void ResetData()
    {
        bg.SetActive(true);
        transform.DOKill();
        bg.transform.localScale = Vector3.one;
        bg.GetComponent<SpriteRenderer>().color = Color.white;
    }
    
    public SpriteRenderer GetIcon()
    {
        return gemIcon;
    }

    public void CollectGem()
    {
        // SoundController.I.bubble_waterbomb.PlaySound();
        transform.SetParent(null);
        bg.transform.DOScale(1.2f, .2f).OnComplete(() => { bg.SetActive(false); }).SetTarget(transform);
        bg.GetComponent<SpriteRenderer>().DOFade(0, -.2f).SetTarget(transform).SetEase(Ease.Linear);
        var path = new Vector3[3];
        path[0] = transform.position;
        path[2] = BubblePuzzleLogic.I.gemCollectPosition;
        path[1] = path[0];

        var distanceX = BubblePuzzleLogic.I.gemCollectPosition.x - path[0].x;
        if (Mathf.Abs(distanceX) > 1)
        {
            var temp = 1;
            if (distanceX < 0)
            {
                temp = -1;
            }

            distanceX = temp;
        }

        path[1].x += distanceX;
        path[1].y -= offsetY;
        transform.DOScale(1.3f, .5f);

        BubblePuzzleLogic.I.collectingCount++;
        transform.DOPath(path, speed, PathType.CatmullRom, PathMode.TopDown2D).SetSpeedBased(isSpeedBase).OnComplete(
            () =>
            {
                BubblePuzzleLogic.I.collectingCount--;
                BubblePuzzleLogic.I.gemCount--;
                BubblePuzzleLogic.OnClearColor?.Invoke(BubbleType.Gem, _color);
                BubblePuzzleLogic.OnColorChanged?.Invoke();
                BubblePuzzleLogic.I.CollectionCheckResult();
                if (fxOnEndMove != null)
                {
                    Instantiate(fxOnEndMove.transform, transform.position, Quaternion.identity);
                }

                Destroy(gameObject);
                // SoundController.I.common_getitem.PlaySound();

            }).SetEase(curve).SetTarget(transform);
    }

    public void CollectCoin()
    {
        // SoundController.I.bubble_waterbomb.PlaySound();
        transform.SetParent(null);
        bg.transform.DOScale(1.2f, .2f).OnComplete(() => { bg.SetActive(false); }).SetTarget(transform);
        bg.GetComponent<SpriteRenderer>().DOFade(0, -.2f).SetTarget(transform).SetEase(Ease.Linear);
        var path = new Vector3[3];
        path[0] = transform.position;
        path[2] = BubblePuzzleLogic.I.coinCollectPosition;
        path[1] = path[0];

        var distanceX = BubblePuzzleLogic.I.gemCollectPosition.x - path[0].x;
        if (Mathf.Abs(distanceX) > 1)
        {
            var temp = 1;
            if (distanceX < 0)
            {
                temp = -1;
            }

            distanceX = temp;
        }

        path[1].x += distanceX;
        path[1].y -= offsetY;
        transform.DOScale(.8f, .5f);
        // BubblePuzzleLogic.I.collectingCount++;
        transform.DOPath(path, speed, PathType.CatmullRom, PathMode.TopDown2D).SetSpeedBased(isSpeedBase).OnComplete(
            () =>
            {
                // BubblePuzzleLogic.I.collectingCount--;
                BubblePuzzleLogic.I.coinCount++;
                BubblePuzzleLogic.OnCoinChanged?.Invoke();
                if (fxOnEndMove != null)
                {
                    Instantiate(fxOnEndMove, transform.position, Quaternion.identity);
                }

                Destroy(gameObject);
                // SoundController.I.common_getcoin.PlaySound();
            }).SetEase(curve).SetTarget(transform);
    }

    private void OnDisable()
    {
        transform.DOKill();
    }
}