using UnityEngine;

public class PuzzleState
{
    public static PlayingMode PlayingMode;
    public static bool IsTutorialShowing;
    public static bool IsTutorialBlockDrag;
    public static bool IsTutorialCollected;
    public static bool IsPlaying;
    public static bool IsEndGame;
    public static bool IsScrolling;
    public static bool BlockUserInput;
    public static bool IsRotate;
    public static bool IsSwapping;
    public static bool IsShooting;

    public static bool UseRandomColorBrushPrePlay;
    public static bool UseRandomThunderBoltPrePlay;
    public static bool UseRandomRocketPrePlay;
    public static bool IsColorblind;
    public static bool HasPopupOver;
    public static bool IsWaitToCheckTutorial;
    public static bool IsWaitForCloseSpecialUI;
    public static bool CanCollectBomb;
    public static bool CanCollectBeam;
    
    public static Camera Camera;
}

public enum PlayingMode
{
    Normal,
    Challenge,
    SubScription,
    SpecialLevel,
}

[System.Serializable]
public class RowCol
{
    public int Row;
    public int Col;
}