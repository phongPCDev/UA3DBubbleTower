using System;

[Serializable]
public class RandomRangeInfo
{
    public float min;
    public float max;
}
