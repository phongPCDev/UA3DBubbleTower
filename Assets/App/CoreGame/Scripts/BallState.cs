public enum BallState
{
    None,
    OnBoard,
    Drop,
    SequenceDrop,
    Clear
}