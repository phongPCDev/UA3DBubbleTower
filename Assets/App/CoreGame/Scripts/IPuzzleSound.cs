public interface IPuzzleSound
{
    void PlayWinSound();
    void PlayClearSound();
    void PlayLoseSound();
    void PlayShootSound();
    void PlayHitSound();
    void PlayShootOnWinSound();
    void PlayReadySound();
    void PlayDropCollectedSound();
    void PlayStarAppearSound();
    void PlayStarCollectedSound();
    void PlaySwapSound();
    void PlayFireSound();
    void PlayRainbowSound();
    void PlayLightingSound();
    void PlayBeamSound();
    void PlayColorBrushSound();
    void PlayRocketActiveSound();
    void PlayRocketSound(); 
    void PlayThunderBoltSound();

}
