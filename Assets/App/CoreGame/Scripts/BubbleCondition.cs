
public partial class Bubble
{
    #region Condition

    public bool IsColorBubble { get; set; }
    public bool CanMatch { get; set; }
    public bool IsPaintCanInteract { get; set; }
    public bool IsThunderBoltCanTarget { get; set; }
    public bool IsBoosterCanChangeColor { get; set; }
    public bool IsBoosterCanDestroy { get; set; }
    private bool IsPaintCanChange { get; set; }
    public bool IsDou { get; set; }
    public bool IsBooster { get; set; }
    public bool IsSingleBall { get; set; }
    public bool IsGhostBubble { get; set; }
    public bool IsPower { get; set; }
    public bool IsPowerCanReplace { get; set; }
    public bool IsRainbow => ballInfo.T == BubbleType.Rainbow || ballInfo.T == BubbleType.ColorBrush;

    private void SetCondition()
    {
        var t = ballInfo.T;
        UpdateDuo();
        UpdateCanMatch();
        IsColorBubble = t == BubbleType.Normal || t == BubbleType.Morph || t == BubbleType.Chain || t == BubbleType.Duo || t == BubbleType.GhostDisable || t == BubbleType.GhostEnable;
        IsPaintCanInteract = t == BubbleType.Normal || t == BubbleType.Duo;
        IsThunderBoltCanTarget = t != BubbleType.GhostDisable && t != BubbleType.BlackHole && t != BubbleType.BlackHoleOff && t != BubbleType.Metal && t != BubbleType.Frozen && t != BubbleType.Coin &&
                                 t != BubbleType.Gem;
        IsBoosterCanChangeColor = t == BubbleType.Normal || t == BubbleType.Morph || t == BubbleType.GhostEnable || t == BubbleType.Minus || t == BubbleType.Plus || IsDou;
        UpdateBoosterCanDestroy();
        IsPaintCanChange = t == BubbleType.Normal;
        IsBooster = t == BubbleType.Fire || t == BubbleType.Rainbow || t == BubbleType.Lightning || t == BubbleType.Beam || t == BubbleType.ThunderBolt || t == BubbleType.Rocket || t == BubbleType.ColorBrush;
        IsSingleBall = t == BubbleType.Normal || t == BubbleType.Morph || t == BubbleType.GhostEnable || t == BubbleType.GhostDisable || t == BubbleType.Plus || t == BubbleType.Minus;
        IsGhostBubble = t == BubbleType.GhostDisable || t == BubbleType.GhostEnable;
        IsPower = t == BubbleType.Rocket || t == BubbleType.ColorBrush || t == BubbleType.ThunderBolt;
        IsPowerCanReplace = !IsPower && !(t == BubbleType.Cloud || t == BubbleType.Coin || t == BubbleType.Gem);
    }

    private void UpdateDuo()
    {
        var t = ballInfo.T;
        IsDou = t == BubbleType.Duo || (t == BubbleType.Normal && ballInfo.C >= 10);
        IsPaintCanInteract = t == BubbleType.Normal || t == BubbleType.Duo;
    }
    public void UpdateBoosterCanDestroy()
    {
        var t = ballInfo.T;
        IsBoosterCanDestroy = t != BubbleType.GhostDisable && t != BubbleType.BlackHoleOff && t != BubbleType.Metal && t != BubbleType.BlackHoleOn;
    }

    public void UpdateCanMatch()
    {
        var t = ballInfo.T;
        CanMatch = t == BubbleType.Normal || t == BubbleType.Morph || t == BubbleType.GhostEnable || t == BubbleType.Minus || t == BubbleType.Plus || t == BubbleType.Paint || t == BubbleType.Chain || t == BubbleType.Gem || IsDou;
    }


    public bool CanDestroyNow(BubbleType type)
    {
        var t = ballInfo.T;
        return ballInfo.T == BubbleType.Morph || t == BubbleType.Normal || t == BubbleType.GhostEnable || t == BubbleType.Paint
               || t == BubbleType.Cloud || t == BubbleType.Plus || t == BubbleType.Minus || t == BubbleType.Gem || t == BubbleType.Coin ||
               IsBooster || (ballInfo.T == BubbleType.Wood && ballInfo.C == 0) ||
               (ballInfo.T == BubbleType.Frozen && IsClearByBooster(type));
    }


    public bool IsMatch(int color)
    {
        if (IsDou) return color == duoC1 || color == duoC2;
        return ballInfo.C == color;
    }

    #endregion
}