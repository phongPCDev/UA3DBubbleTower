using System.Collections.Generic;
using AFramework;
using UnityEngine;

public class PoolWrapper : ManualSingletonMono<PoolWrapper>
{
    public const string Fx_BoosterFireActive = "Fx_BoosterFireActive";
    public const string Fx_BoosterFireMatching = "Fx_BoosterFireMatching";
    public const string Fx_RainbowMatching = "Fx_RainbowMatching";
    public const string Fx_PaintChange = "Fx_PaintChange";
    public const string Fx_ChainCracker = "BallExplosiveSpiderEffect";
    public const string FX_ObjBeam = "FX_ObjBeam";
    public const string Fx_Ice_Cracker2 = "Fx_Ice_Cracker"; // Crack with out shine
    public const string Fx_Lighting2Explosive = "Fx_Lighting2Explosive";
    public const string Fx_Lighting2FindTarget = "Fx_Lighting2FindTarget";
    public const string FX_PowerThunderBolt = "Fx_PowerThunderBolt";
    public const string Fx_PowerColorBrush = "Fx_PowerColorBrush";
    public const string PowerColorBrush = "PowerColorBrush";
    public const string PowerRocket = "PowerRocket";
    public const string PowerThunderBolt = "PowerThunderBolt";
    public const string Fx_PowerRocket = "Fx_PowerRocket";
    public const string ComboThunderBolt_Rocket = "ComboThunderBolt_Rocket";
    public GameObject matchedEffect;
    public GameObject bubbleScore;
    public GameObject iceShine;
    public GameObject iceCrack;
    public GameObject cloud;
    public GameObject fxCloudDead;
    public GameObject blackHole;
    public GameObject blackHoleOnOff;
    public GameObject fxWoodBreak;
    public GameObject fxPlusMinus;
    public GameObject noticePrefab;
    public GameObject fxLightingActive;
    public GameObject fxFireActive;
    public GameObject fxTrail;
    public GameObject bubbleGem;

    public List<GameObject> listObjects;
    private readonly Dictionary<string, GameObject> _dictObjects = new Dictionary<string, GameObject>();

    protected override void Awake()
    {
        base.Awake();
        foreach (var dictObject in listObjects)
        {
            if (dictObject != null)
            {
                _dictObjects.Add(dictObject.name, dictObject);
            }
        }
    }

    public void GetMatchedEffect(Vector3 position, int colorIndex)
    {
        var fx = Instantiate(matchedEffect, position, Quaternion.identity)
            .GetComponent<BallExplosiveEffect>();
        fx.SetData(colorIndex);
    }

    public void GetBubbleScore(Vector3 position, int score)
    {
        Instantiate(bubbleScore, position, Quaternion.identity).GetComponent<BubbleScoreController>()
            .SetData(score, position);
    }

    public Transform GetIceShine(Transform parent)
    {
        var fx = Instantiate(iceShine, parent.position, Quaternion.identity, parent).transform;
        return fx;
    }

    public void GetIceCrack(Vector3 position, Transform parent)
    {
        Instantiate(iceCrack, position, Quaternion.identity, parent);
    }

    public BlackHoleController GetBlackHole(Transform parent)
    {
        return Instantiate(blackHole, parent.position, Quaternion.identity, parent)
            .GetComponent<BlackHoleController>();
    }

    public BlackHoleController GetBlackHole2(Transform parent)
    {
        return Instantiate(blackHoleOnOff, parent.position, Quaternion.identity, parent)
            .GetComponent<BlackHoleController>();
    }

    public Transform GetCloud(Transform parent)
    {
        return Instantiate(cloud, parent.position, Quaternion.identity, parent).transform;
    }

    public void GetFxCloudDead(Vector3 position)
    {
        Instantiate(fxCloudDead, position, Quaternion.identity);
    }

    public void GetFxWoodBreak(Vector3 position)
    {
        Instantiate(fxWoodBreak, position, Quaternion.identity);
    }

    public PlusMinusController GetFxPlusMinus(Vector3 position)
    {
        return Instantiate(fxPlusMinus, position, Quaternion.identity)
            .GetComponent<PlusMinusController>();
    }

    public void SpawnNotice(Vector3 position, bool anim, float alpha = 1)
    {
        var notice = GameObject.Instantiate(noticePrefab, position, Quaternion.identity);
        var animation = notice.GetComponent<Animation>();
        animation.enabled = anim;
        animation.GetComponent<SpriteRenderer>().SetAlpha(alpha);
    }

    public void RemoveAllNotices()
    {
        // GamePool.emptyNotificationPool.DespawnAll();
    }

    public void GetFxFireActive(Vector3 position)
    {
        GameObject.Instantiate(fxFireActive, position, Quaternion.identity);
    }

    public void GetFxLightingActive(Vector3 position)
    {
        GameObject.Instantiate(fxLightingActive, position, Quaternion.identity);
    }



    public Transform GetFxObject(string objectName, Vector3 position)
    {
        return GameObject.Instantiate(_dictObjects[objectName], position, Quaternion.identity).transform;
    }

    public Transform GetFxObject(string objectName, Vector3 position, Transform t)
    {
        return GameObject.Instantiate(_dictObjects[objectName], position, Quaternion.identity, t).transform;
    }

    public Transform GetObject(GameObject gameObject, Vector3 position, Transform t)
    {
        return GameObject.Instantiate(bubbleGem, position, Quaternion.identity, t).transform;
    }
}

public static class GameObjectHelper
{
    public static Transform GetBubblePool(this GameObject gameObject, Transform parent = null)
    {
        return GameObject.Instantiate(gameObject, parent).transform;
    }

    public static Transform GetFxPool(this GameObject gameObject, Transform parent = null)
    {
        return GameObject.Instantiate(gameObject, parent).transform;
    }
}