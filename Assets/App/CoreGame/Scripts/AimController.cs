using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.UI.Image;

public class AimController : MonoBehaviour
{
    public bool isOdd;
    public bool isHitTop;
    public List<bool> boundHits;
    [SerializeField] private GameObject top;
    [SerializeField] private float radius;
    [SerializeField] private Vector3 halfBoxExtends;
    [SerializeField] private bool showDebugLine = true;
    [SerializeField] private LayerMask checkLayer;
    [SerializeField] private LayerMask ballLayer;
    [SerializeField] public float minY = 0.3f;
    public List<Vector3> path;
    public Vector3 lastPoint;
    public float limitLeft;
    public float limitRight;

    public IAim iAim;

    private int _loopCount;
    public bool canShoot;

    public int LoopCount { get => _loopCount; }

    public void Aim(Vector3 startPosition, Vector3 direct, bool isDraw = true, int maxLine = 10, int maxDot = -1)
    {
        isHitTop = false;
        boundHits.Clear();
        direct.Normalize();
        if (/*direct.y < minY*/false)
        {
            canShoot = false;
            maxDot = 3;
            maxLine = 1;
            // path.Clear();
            // iAim.BeginDraw();
            // path.Add(startPosition);
            // CheckDirect(startPosition, direct, isDraw, maxLine, 3);
            // // iAim.EndDraw();
            // return;
        }
        else
        {
            canShoot = true;
        }

        path.Clear();
        path.Add(startPosition);
        if (isDraw)
        {
            iAim?.BeginDraw();
        }

        _loopCount = 0;
        DrawRay(startPosition, direct * 10, Color.cyan);
        CheckDirect(startPosition, direct, isDraw, maxLine, maxDot);
    }

    private void CheckDirect(Vector3 origin, Vector3 direct, bool isDraw, int maxLine = 10, int maxDot = -1)
    {
        direct.Normalize();
        Physics.BoxCast(origin, halfBoxExtends, direct, out RaycastHit hit, Quaternion.identity, Mathf.Infinity, checkLayer);

        if (hit.collider != null)
        {
            var checkPoint1 = hit.point + hit.normal * radius;
#if UNITY_EDITOR
            DrawRay(hit.point, hit.normal * 0.5f, Color.magenta);
#endif
            var angle = Vector3.Angle(hit.normal, -direct);
            var invertDistance = (0.5f - radius) / Mathf.Cos(angle * Mathf.Deg2Rad);
            var nextOrigin = (Vector3)checkPoint1 - direct * invertDistance;
            path.Add(nextOrigin);
#if UNITY_EDITOR
            DrawCircle(nextOrigin, .5f, 20, Color.red);
#endif
            if (hit.collider.gameObject.layer == LayerDefine.BallOnBoard)
            {
                float hitAngle = Vector3.Angle(Vector3.right, hit.normal);
                int angleIndex = Mathf.RoundToInt(hitAngle / 60);

                int row = (int)(hit.transform.position.y / BubblePuzzleLogic.I.GetRowDistance());
                if (row % 2 == 0)
                {
                    if (hit.transform.position.x < limitLeft + 1 && angleIndex == 3)
                    {
                        angleIndex--;
                    }
                    else if (hit.transform.position.x > limitRight - 1 && angleIndex == 0)
                    {
                        angleIndex++;
                    }
                }

                var angleWithHitBall = angleIndex * 60;

                var snapPosition = hit.transform.position + GetVector(angleWithHitBall);
                DrawCircle(snapPosition, .5f, 20, Color.yellow);
                var check = CorrectSnapPoint(nextOrigin, ref snapPosition);
                var endPos = hit.point + hit.normal * radius;
#if UNITY_EDITOR
                DrawRay(origin, direct * 100, Color.white);
                DrawLine(nextOrigin, snapPosition, Color.cyan);
                DrawCircle(snapPosition, .5f, 20, Color.black);
                DrawCircle(endPos, radius, 20, Color.black);
                DrawRay(hit.transform.position, (hit.point - hit.transform.position).normalized * 0.3f,
                    Color.red);
                DrawRay(hit.transform.position, Vector3.right * .3f, Color.red);
#endif

                if (isDraw)
                {
                    iAim?.Draw(origin, endPos, direct, !check, maxDot);
                    iAim?.EndDraw();
                }

                path.RemoveAt(path.Count - 1);
                path.Add(snapPosition);
                lastPoint = snapPosition;
            }
            else if (hit.collider.gameObject == top)
            {
                isHitTop = true;
#if UNITY_EDITOR
                DrawLine(origin, checkPoint1, Color.white);
#endif
                var snapPosition = nextOrigin;
                // snapPosition.x = (int)snapPosition.x;

                if (isOdd)
                {
                    snapPosition.x = (int)snapPosition.x + (snapPosition.x > 0 ? .5f : -.5f);
                }
                else
                {
                    snapPosition.x = Mathf.Round(snapPosition.x);
                }

                var check = CorrectSnapPoint(nextOrigin, ref snapPosition);
#if UNITY_EDITOR
                DrawCircle(snapPosition, .5f, 20, Color.black);
#endif
                if (isDraw)
                {
                    iAim?.Draw(origin, nextOrigin, direct, true, maxDot);
                    iAim?.EndDraw();
                }

                path.Add(snapPosition);
                lastPoint = snapPosition;
            }
            else if (_loopCount < maxLine)
            {
#if UNITY_EDITOR
                DrawLine(origin, checkPoint1, Color.white);
#endif
                if (isDraw)
                {
                    iAim?.Draw(origin, nextOrigin, direct, false, maxDot);
                    iAim?.EndDraw();
                    boundHits.Add(true);
                }

                _loopCount++;
                if (_loopCount < maxLine)
                {
                    CheckDirect(nextOrigin, Vector3.Reflect(direct, hit.normal), isDraw);
                }
                else
                {
                    if (isDraw)
                    {
                        iAim?.EndDraw();
                    }
                }
            }
            else
            {
                if (isDraw)
                {
                    iAim?.EndDraw();
                }
            }
        }
        else
        {
            DrawLine(origin, direct * 10, Color.white);
            if (maxDot > 0)
            {
                if (isDraw)
                {
                    iAim?.Draw(origin, Vector3.up * 3, direct, false, maxDot);
                    iAim?.EndDraw();
                    boundHits.Add(true);
                }
            }
        }
    }

    private bool CorrectSnapPoint(Vector3 origin, ref Vector3 snapPosition)
    {
        var hit = Physics2D.Raycast(snapPosition, Vector2.zero, 0, ballLayer);
        if (hit.collider != null)
        {
            if (origin.x < snapPosition.x)
            {
                snapPosition.x -= 1;
            }
            else
            {
                snapPosition.x += 1;
            }

            return true;
        }

        if (snapPosition.x < limitLeft)
        {
            snapPosition.x += 1;
        }
        else if (snapPosition.x > limitRight)
        {
            snapPosition.x -= 1;
        }

        return false;
    }

    private void DrawLine(Vector3 startPos, Vector3 endPos, Color color)
    {
        if (showDebugLine) Debug.DrawLine(startPos, endPos, color);
    }

    private void DrawRay(Vector3 startPos, Vector3 direct, Color color)
    {
        if (showDebugLine) Debug.DrawRay(startPos, direct, color);
    }

    private void DrawCircle(Vector3 position, float radius, int segments, Color color)
    {
        if (!showDebugLine) return;
        if (radius <= 0.0f || segments <= 0) return;
        float angleStep = (360.0f / segments);
        angleStep *= Mathf.Deg2Rad;
        Vector3 lineStart = Vector3.zero;
        Vector3 lineEnd = Vector3.zero;
        for (int i = 0; i < segments; i++)
        {
            lineStart.x = Mathf.Cos(angleStep * i);
            lineStart.y = Mathf.Sin(angleStep * i);
            lineEnd.x = Mathf.Cos(angleStep * (i + 1));
            lineEnd.y = Mathf.Sin(angleStep * (i + 1));
            lineStart *= radius;
            lineEnd *= radius;
            lineStart += position;
            lineEnd += position;
            Debug.DrawLine(lineStart, lineEnd, color);
        }
    }

    private Vector3 GetVector(float angle)
    {
        angle *= Mathf.Deg2Rad;
        return new Vector3(Mathf.Cos(angle), -Mathf.Sin(angle)).normalized;
    }

    public void Hide()
    {
        iAim?.Hide();
    }

    public float GetLength()
    {
        float length = 0;
        if (path.Count >= 2)
        {
            int count = path.Count;
            for (int i = 1; i < count; i++)
            {
                length += Vector2.Distance(path[i - 1], path[i]);
            }
        }

        return length;
    }
}