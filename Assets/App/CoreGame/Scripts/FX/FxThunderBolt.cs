using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

public class FxThunderBolt : MonoBehaviour
{
    public float randomX = 0.2f;
    public GameObject fxOnHit;

    public void InitData(Bubble originBubble, Bubble hitBubble, float offset, Action onComplete)
    {
        var path = new Vector3[4];
        var startPos = originBubble.Pos;
        var hitPos = hitBubble.Pos;
        transform.position = startPos;

        path[0] = startPos;
        path[1] = (startPos * 2 + hitPos) / 3 + Vector3.right * Random.Range(-randomX, randomX);
        path[2] = (startPos + hitPos * 2) / 3 + Vector3.right * Random.Range(-randomX, randomX);
        path[3] = hitPos;
        transform.localScale = Vector3.one;
        Sequence s = DOTween.Sequence();
        s.Insert(0, transform.DOScale(1.3f, 0.2f));
        s.Insert(0.2f, transform.DOScale(1, 0.2f));
        s.Insert(0.2f, transform.DOPath(path, 1, PathType.CatmullRom).SetEase(Ease.OutQuad));
        s.OnComplete(() =>
        {
            onComplete?.Invoke();
            if (fxOnHit != null)
            {
                Instantiate(fxOnHit).transform.position = transform.position;
            }
        }).SetTarget(originBubble.Trans);
    }
}