using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FxPowerRocket : MonoBehaviour
{
    [SerializeField] private float timeToFirstHit = 0.15f;
    [SerializeField] private float timeBetweenHit = 0.1f;

    public void SetData(RaycastHit2D[] hits, Bubble origin)
    {
        StartCoroutine(DelayActive(hits, origin));
    }

    IEnumerator DelayActive(RaycastHit2D[] hits, Bubble origin)
    {
        yield return new WaitForSeconds(timeToFirstHit);
        foreach (var hit2D in hits)
        {
            if (hit2D.collider.gameObject != gameObject)
            {
                var bubble = hit2D.collider.GetComponent<Bubble>();
                var t = bubble.ballInfo.T;
                bool rocketCanClear =
                    !(t == BubbleType.Rainbow ||t == BubbleType.GhostDisable ||t ==  BubbleType.Metal ||t ==  BubbleType.BlackHole ||t ==  BubbleType.BlackHoleOn ||t ==  BubbleType.BlackHoleOff);
                if (bubble.isLive && bubble.canTarget && rocketCanClear)
                {
                    if (bubble.IsDou)
                    {
                        bubble.clearDelay = Vector2.Distance(bubble.Pos, origin.Pos) * timeBetweenHit;
                        bubble.StartCoroutine(bubble.DestroyBubble(origin, bubble.duoC2, BubbleType.Normal, false));   
                    }
                    else
                    {
                        if ((bubble.ballInfo.T != BubbleType.Wood && bubble.ballInfo.T != BubbleType.Chain) || (bubble.ballInfo.T == BubbleType.Wood && bubble.ballInfo.C == 0))
                        {
                            bubble.isLive = false;
                            bubble.RemoveConnections(origin.ballInfo.T, true);
                        }
                        bubble.clearDelay = Vector2.Distance(bubble.Pos, origin.Pos) * timeBetweenHit;
                        bubble.StartCoroutine(bubble.DestroyBubble(origin, -(int)origin.ballInfo.T, origin.ballInfo.T, false));   
                    }
                }
            }
        }
    }
}