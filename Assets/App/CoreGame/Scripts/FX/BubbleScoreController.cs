using System.Collections;
using UnityEngine;

public class BubbleScoreController : MonoBehaviour
{
    [SerializeField] private TextMesh scoreText;
    private Transform _transform;
    private Animation _animation;

    private void Awake()
    {
        _transform = transform;
        _animation = GetComponent<Animation>();
    }

    public void SetData(int score, Vector3 position)
    {
        _transform.position = position;
        scoreText.text = score.ToString();
        StartCoroutine(CREndAnim());
    }

    IEnumerator CREndAnim()
    {
        yield return new WaitForSeconds(1);
        Destroy(gameObject);
    }
}