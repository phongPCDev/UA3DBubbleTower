using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoosterFxManager : MonoBehaviour
{
    public List<GameObject> objects;

    private void OnEnable()
    {
        BubblePuzzleLogic.OnBoosterEnable += OnBoosterEnable;
        BubblePuzzleLogic.OnBoosterDisable += OnBoosterDisable;
        BubblePuzzleLogic.OnLevelPreload += OnLevelPreload;
        HideAllFx();
    }

    private void OnDisable()
    {
        BubblePuzzleLogic.OnBoosterEnable -= OnBoosterEnable;
        BubblePuzzleLogic.OnBoosterDisable -= OnBoosterDisable;
        BubblePuzzleLogic.OnLevelPreload -= OnLevelPreload;
    }

    private void OnBoosterEnable(BubbleType obj)
    {
        HideAllFx();
        switch (obj)
        {
            case BubbleType.Fire:
                objects[0].SetActive(true);
                break;
            case BubbleType.Rainbow:
                objects[1].SetActive(true);
                break;
            case BubbleType.Lightning:
                objects[2].SetActive(true);
                break;
            case BubbleType.Beam:
                objects[3].SetActive(true);
                break;
        }
    }

    private void OnBoosterDisable()
    {
        HideAllFx();
    }

    private void OnLevelPreload()
    {
        HideAllFx();
    }

    private void HideAllFx()
    {
        foreach (var o in objects)
        {
            o.SetActive(false);
        }
    }
}