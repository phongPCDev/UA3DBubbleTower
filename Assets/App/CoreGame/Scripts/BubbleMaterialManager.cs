using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AFramework;

public class BubbleMaterialManager : ManualSingletonMono<BubbleMaterialManager>
{
    [SerializeField] private Material[] bubbleMaterials;
    private Dictionary<string, Material> _matDict;
    [SerializeField] private string[] bubbleName;

    protected override void Awake()
    {
        base.Awake();
        InitColorDict();
    }

    private void InitColorDict()
    {
        _matDict = new Dictionary<string, Material>();
        for (int i = 0; i < bubbleName.Length; i++)
        {
            if (bubbleMaterials[i] != null)
                _matDict.Add(bubbleName[i], bubbleMaterials[i]);
        }
    }

    public static Material GetColor(string name)
    {
        return I._matDict[name];
    }

    public static Material GetColor(int c)
    {
        return GetColor(I.bubbleName[c]);
    }
}
