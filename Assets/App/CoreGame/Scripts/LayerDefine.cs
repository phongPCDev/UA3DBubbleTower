public class LayerDefine
{
    public const int Ball = 8;
    public const int Bound = 9;
    public const int StartBall = 10;
    public const int BallOnBoard = 11;
    public const int BallDrop = 12;
    public const int BallColliction = 13;
    public const int BallHidden = 14;
    public const int BallWeighted = 15;
}