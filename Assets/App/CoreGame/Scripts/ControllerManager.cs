using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Serialization;

public class ControllerManager : MonoBehaviour
{
    public eControl controlType;
    public float fadeDuration = 0.5f;

    public List<BaseController> controllers;

    private void Start()
    {
        foreach (var baseController in controllers)
        {
            if (baseController.controlType == controlType)
            {
                baseController.gameObject.SetActive(true);
                baseController.Setup();
                break;
            }
        }
    }
}

public enum eControl
{
    SpiderControl,
    TriangleControl,
    PhysicClear
}