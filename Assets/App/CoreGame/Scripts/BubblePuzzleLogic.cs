using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using AFramework;
using AFramework.ExtensionMethods;
using DG.Tweening;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class BubblePuzzleLogic : ManualSingletonMono<BubblePuzzleLogic>
{
    public static bool isUse3D = true;
    public static float timeScale;
    public static bool useGizmos = true;
    public static IPuzzleSound PuzzleSound;
    public static Action OnLevelPreload;
    public static Action OnReadFileDone;
    public static Action OnLevelLoaded;
    public static Action OnReady;
    public static Action OnMouseDown;
    public static Action OnMouseUp;
    public static Action OnPreWin;
    public static Action OnWin;
    public static Action OnOutOfMove;
    public static Action OnLose;
    public static Action OnExit;
    public static Action OnSkip;
    public static Action<Bubble, Bubble, Vector3> OnShoot;
    public static Action OnShootChanged;
    public static Action<BubbleType> OnClear;
    public static Action OnShootWithoutClear;
    public static Action<BubbleType> OnBoosterEnable;
    public static Action OnBoosterDisable;
    public static Action<BubbleType> OnBoosterShoot;
    public static Action OnDropCollect;
    public static Action<BubbleType, int> OnClearColor;
    public static Action<bool, int> OnContinue;
    public static Action<bool, Vector2> OnScoreChanged;
    public static Action<int> OnAddMoreMove;
    public static Action OnClearLevel;
    public static Action OnBubbleListChanged;
    public static Action OnColorChanged;
    public static Action OnCollectedCoin;
    public static Action OnCoinChanged;
    public static Action OnDone;

    public static int Score;

    public bool showCountObject;
    public bool showEndCard;


    public float boosterScale = 1.22f;
    public AnimationCurve moveCurve;
    public AnimationCurve offsetCurve;
    public AnimationCurve scaleCurve;
    public bool foreAlternateLevel;
    public bool priorityChooseBooster;
    public bool priorityChooseCloud;
    public Vector3 gemCollectPosition { get; set; }
    public Vector3 coinCollectPosition { get; set; }
    public CollectMode collectMode { get; set; }

    public int clearCount;
    public int gemCount;
    public bool needCreateNewBall;
    public bool isShootBooster;
    public bool isClearChecking;
    public bool isDropChecking;
    public bool isBlackHoldRunning;
    public int collectingCount;

    public GameObject startBall_Rotate;

    [SerializeField] public bool disableBubblePhysicsBeforeMove = false;
    [SerializeField] private float boardMoveUpSpeed = 10;
    [SerializeField] public float boardMoveDownSpeed = 10;
    [SerializeField] public float clearDeltaTime = .1f;
    [SerializeField] private float dropDeltaTime = .1f;
    [SerializeField] private float delayBeforeBoardMove = 1f;
    [SerializeField] private float winDelay = 1f;
    [SerializeField] public float randomDelay = .05f;
    [SerializeField] public float ballMoveSpeed = 20;
    [SerializeField] public float ballScaleDuration = .1f;
    [SerializeField] public RandomRangeInfo dropSpeedScaleX;
    [SerializeField] public RandomRangeInfo dropSpeedScaleY;
    [SerializeField] public RandomRangeInfo dropSpeedScaleZ;
    [SerializeField] public float SpawnRadius = 3;

    private float highestY = -100;
    private float lowestY = -100;
    private float bubbleTotalDst = -100;

    public int numStars;

    [SerializeField] public Transform limitTop;
    [SerializeField] public Transform limitBottom;
    [SerializeField] public Transform dropCheckPoint;
    [SerializeField] private LayerMask startBallLayer;
    [SerializeField] private LayerMask hiddenBallLayer;
    [SerializeField] private LayerMask ballOnBoardLayer;
    [SerializeField] public LayerMask ballNeighborLayer;
    [SerializeField] public LayerMask mouseInputCastLayer;
    public List<RowCol> listRowColForBombs;
    public List<RowCol> listRowColForBombs2;
    public Transform leftCollider;
    public Transform rightCollider;

    [SerializeField] public Transform boardContainer;
    [SerializeField] public BoxCollider2D colliderBoardContainer;
    [SerializeField] private GameObject bubblePrefab;
    [SerializeField] public AimController aimController;
    [SerializeField] private GameObject objBeam;
    [SerializeField] private GameObject topBg;
    [SerializeField] private GameObject firework;
    public Transform boosterFireHolderPosition;
    public Transform shootArrow;

    [SerializeField] public Transform startBalls;
    [SerializeField] public Transform currentShootHolder;
    [SerializeField] private Transform nextShootHolder;
    [SerializeField] private TextMesh moveLeftText;
    [SerializeField] public int levelIndex;
    [SerializeField] public float startBallOffset = 4.36f;
    public int lastColor;

    private bool _isLevelLoad;

    private int levelView => levelIndex + 1;
    [SerializeField] public LevelConfigNew levelConfig;

    [SerializeField] private List<Bubble> bubbles = new List<Bubble>();
    [SerializeField] private List<Bubble> rootBubbles = new List<Bubble>();
    [SerializeField] private List<Bubble> morphBubbles = new List<Bubble>();
    [SerializeField] private List<Bubble> switchSpikes = new List<Bubble>();
    [SerializeField] private List<Bubble> ghostBubbles = new List<Bubble>();
    [SerializeField] public List<Bubble> cloudBubbles = new List<Bubble>();
    [SerializeField] public List<Bubble> delayedFallBubbles = new List<Bubble>();

    private List<Bubble> bubblesInsideView = new List<Bubble>();

    [SerializeField] private List<Bubble> rocketBubbles = new List<Bubble>();
    [SerializeField] private bool _isSuggestionShowing;
    [SerializeField] private float _suggestionCountTime;
    [SerializeField] private SwapSuggestController swapSuggestController;
    [SerializeField] private AnimationClip ball_notification;

    public float rowDistance;
    private BoardState _boardState;
    public BoardState boardState => _boardState;

    public int curRow;
    public int curCol;
    public Bubble currentBubble { get; set; }
    public Bubble nextBubble { get; set; }
    public Bubble booster { get; set; }
    public Dictionary<int, int> countDict;
    private Dictionary<int, float> countRate; // Tỉ lệ % sinh ra bóng
    private Dictionary<int, bool> bubbleInsideView; // Màu bóng có trong view
    private Dictionary<int, float> startBallRateRandom;
    private List<int> _starScore = new List<int>() { 0, 0, 1 };
    private int _spawnCount;
    private Camera _camera;
    public bool _isAiming;
    public bool _isMouseDown;
    public int _dropCount;
    public int specialBallActiveCount;

    private bool isMatchedThisShot;

    public int _shootRemain;
    private bool _needUpdateAfterShoot;
    private Vector3 _offset;
    public static int BallCount;

    private bool _needCheckAfterShoot;
    private bool _isDropChecked;
    public int coinCount { get; set; }
    public bool isUpdateBeamProgress;
    private int oldColor;
    private int shootColorCount;
    public bool IsSwitch { get; set; }
    public bool IsReady { get; set; }
    public bool IsMissedShot { get; set; }
    public bool IsDelayMissedShoot { get; set; }
    public Vector3 lastShootPosition { get; set; }

    public int ShootRemain
    {
        get => _shootRemain;
        set
        {
            _shootRemain = value;
            moveLeftText.text = _shootRemain.ToString();
        }
    }

    private bool _isFirstBoardMove;
    private int _comboCount;
    private bool _hasAddCombo;
    private float _delayDrop;
    private bool _isMoveDown;
    public int comboCount => _comboCount;
    private readonly int[] _targetStars = { 0, 0, 0 };
    public List<Bubble> _clearList = new List<Bubble>();
    private readonly List<Bubble> _dropSequenceList = new List<Bubble>();

    private bool _isBreakWinShoot;

    private static void SetScore(int value, bool showAnimStar, Vector2 pos)
    {
        Score = value;
        OnScoreChanged?.Invoke(showAnimStar, pos);
    }

    public static void PlusScore(int value, bool showAnimStar, Vector2 pos)
    {
        Score += value;
        OnScoreChanged?.Invoke(showAnimStar, pos);
    }

    protected override void Awake()
    {
        base.Awake();
        //rowDistance = 0.866f;
        _camera = Camera.main;
        countDict = new Dictionary<int, int>();
        countRate = new Dictionary<int, float>();
        bubbleInsideView = new Dictionary<int, bool>();
        for (int i = 0; i < 8; i++)
        {
            countDict.Add(i, 0);
            countRate.Add(i, 0);
            bubbleInsideView.Add(i, true);
        }

        Application.targetFrameRate = 60;
        BubbleScore.InitBubbleScore();
        gameObject.SetActive(false);
        startBalls.gameObject.SetActive(false);
    }

    private void Update()
    {
#if UNITY_EDITOR

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            ReloadLevel();
        }
#endif

        if (_boardState == BoardState.BoardMoving)
        {
            if (_isFirstBoardMove && Input.GetMouseButtonDown(0))
            {
                Time.timeScale = 2;
                // #if UNITY_EDITOR_WIN
                //                 Time.timeScale = 10;
                // #endif
            }
        }
        else
        {
            UserInput();
        }

        if (PuzzleState.IsPlaying && !PuzzleState.IsEndGame)
        {
            bool isCheck = clearCount <= 0 && _dropCount <= 0 && specialBallActiveCount <= 0;

            if (!isDropChecking)
                isCheck = clearCount <= 0 && specialBallActiveCount <= 0;


            // if (!_isAiming && !PuzzleState.BlockUserInput && isCheck) CheckShowSuggest();
            if (_needCheckAfterShoot)
            {
                if (isCheck && !isClearChecking && _isDropChecked)
                {
                    _needCheckAfterShoot = false;
                    if (!CheckWin())
                    {
                        if (ShootRemain <= 0)
                        {
                            if (needCreateNewBall || isShootBooster)
                            {
                                needCreateNewBall = false;
                                isShootBooster = false;
                                if (collectMode == CollectMode.Color ||
                                    (collectMode == CollectMode.Gem && collectingCount == 0))
                                    SetOutOfMove();
                            }
                        }
                        else
                        {
                            UpdateAfterShoot();
                            if (needCreateNewBall)
                            {
                                needCreateNewBall = false;
                                CreateNewShootBubbleAfterShoot();
                            }

                            MoveBoard(() =>
                            {
                                PuzzleState.BlockUserInput = false;
                                OnDone?.Invoke();
#if UNITY_EDITOR
                                Debug.Log("Move Board: DONE");
#endif
                            }, boardMoveDownSpeed);
                        }
                    }
                }
            }
        }
    }

    public void CollectionCheckResult()
    {
        if (collectMode == CollectMode.Gem)
        {
            if (gemCount == 0)
            {
                PuzzleState.IsPlaying = false;
                StartCoroutine(CRWin());
            }
        }

        if (ShootRemain == 0 && BubblePuzzleLogic.I.collectingCount <= 0)
        {
            SetOutOfMove();
        }
    }

    #region LOAD LEVEL

    public void ClearLevel()
    {
        var allBubbles = FindObjectsOfType<Bubble>().ToList();
        foreach (var item in allBubbles)
        {
            Destroy(item.gameObject);
        }

        booster = null;
        if (!_isLevelLoad) return;
        _isLevelLoad = false;
        StopAllCoroutines();
        PuzzleState.IsPlaying = false;
        aimController.iAim.BeginDraw();
        aimController.iAim.EndDraw();

        boardContainer.DOKill();
        transform.DOKill();
        OnClearLevel?.Invoke();
        startBalls.gameObject.SetActive(false);
    }

    private bool isLoaded;

    public IEnumerator LoadLevel()
    {
        if (isLoaded)
        {
            yield break;
        }

        isLoaded = true;
        Debug.Log("Load level 2");
        //Luna.Unity.Analytics.LogEvent("LoadLevel", 0);
        timeScale = 1;
        Time.timeScale = 1;
        foreach (var key in bubbleInsideView.Keys.ToList())
        {
            bubbleInsideView[key] = false;
        }

        _isMouseDown = false;
        IsMissedShot = false;
        IsDelayMissedShoot = false;
        IsReady = false;
        IsSwitch = false;
        shootColorCount = 0;
        oldColor = -1;
        isUpdateBeamProgress = false;
        isShootBooster = false;
        collectingCount = 0;
        coinCount = 0;
        PuzzleState.IsEndGame = false;
        isBlackHoldRunning = false;
        isClearChecking = false;
        _isDropChecked = false;
        gemCount = 0;
        specialBallActiveCount = 0;
        _isBreakWinShoot = false;
        needCreateNewBall = false;
        _needCheckAfterShoot = false;
        _isLevelLoad = true;
        _dropCount = 0;
        clearCount = 0;
        BallCount = 0;
        PuzzleState.Camera = _camera;
        RemoveBallSuggestion();
        swapSuggestController.ShowSuggest();
        PuzzleState.IsTutorialBlockDrag = false;
        PuzzleState.IsTutorialCollected = false;
        PuzzleState.IsPlaying = false;
        PuzzleState.IsSwapping = false;
        PuzzleState.IsShooting = false;

        firework.SetActive(false);
        topBg.gameObject.SetActive(false);
        SetScore(0, false, Vector2.zero);
        curRow = curCol = -1;
        _comboCount = 0;
        numStars = 0;
        Time.timeScale = 1;
        _isFirstBoardMove = true;
        PuzzleState.BlockUserInput = true;
        _spawnCount = 0;
        currentBubble = null;
        nextBubble = null;
        bubbles.Clear();
        rootBubbles.Clear();
        morphBubbles.Clear();
        ghostBubbles.Clear();
        cloudBubbles.Clear();
        switchSpikes.Clear();
        rocketBubbles.Clear();
        delayedFallBubbles.Clear();
        var keys = countDict.Keys.ToList();
        foreach (var key in keys)
        {
            countDict[key] = 0;
        }

        aimController.iAim.Hide();

        OnLevelPreload?.Invoke();
        LoadLevelConfig1();
        shootArrow.eulerAngles = new Vector3(0, 0, 0);
        _starScore = new List<int> { levelConfig.star1, levelConfig.star2, levelConfig.star3 };
        startBallRateRandom = levelConfig.GetRateRandom();
        SetShootRemainAndStars();
        InitBoard();
        yield return new WaitForEndOfFrame();
#if UNITY_EDITOR
        Physics2D.SyncTransforms();
#endif

        EnablePhysicBallAfterBoardMove();
        UpdateNeighbors();
        UpdateWallCollider();
        OnLevelLoaded?.Invoke();
        yield return new WaitUntil(() => !PuzzleState.IsWaitForCloseSpecialUI);
        MoveBoard(() =>
        {
            _isFirstBoardMove = false;
            PuzzleState.IsPlaying = true;
            // PuzzleSound?.PlayReadySound();
            OnReady?.Invoke();
            topBg.gameObject.SetActive(true);
            DOVirtual.DelayedCall(.5f, () => { PuzzleState.BlockUserInput = false; }).SetTarget(boardContainer);
            Aim(currentShootHolder.position + Vector3.up * 10, 3);
            IsReady = true;

            bubbleTotalDst = highestY - lowestY;
        }, 0);
        GetDropList();
        HideAim();
        MoveStartBall();
        CreateShootBubbles();
    }

    private void UpdateWallCollider()
    {
        leftCollider.position = new Vector3(-levelConfig.col / 2f, 0, -4.9f);
        rightCollider.position = new Vector3(levelConfig.col / 2f, 0, -4.9f);
        aimController.limitLeft = leftCollider.position.x + 0.3f;
        aimController.limitRight = rightCollider.position.x - 0.3f;
    }

    #region RANDOM BOOSTERS

    public Bubble lastRandomPowerItem;


    public void RemoveFromSpecialList(Bubble bubble)
    {
        var type = bubble.ballInfo.T;
        if (type == BubbleType.GhostEnable || type == BubbleType.GhostDisable)
        {
            ghostBubbles.Remove(bubble);
        }
        else if (type == BubbleType.Morph)
        {
            morphBubbles.Remove(bubble);
        }
        else if (type == BubbleType.BlackHoleOff || type == BubbleType.BlackHoleOn)
        {
            switchSpikes.Remove(bubble);
        }
    }

    #endregion

    private void SetShootRemainAndStars()
    {
        ShootRemain = levelConfig.moveLeft;
        _targetStars[0] = levelConfig.star1;
        _targetStars[1] = levelConfig.star2;
        _targetStars[2] = levelConfig.star3;
    }

    public void LoadLevelConfig1()
    {
        if (PuzzleState.PlayingMode == PlayingMode.Normal)
        {
            bool isUseNewLevel = false;
            int levelViewMapping = levelIndex + 1;
            Debug.Log("levelIndex: " + levelIndex);
            string levelPath;
            if (isUseNewLevel)
            {
                levelPath = $"AlternateLevel/Level {levelViewMapping}";
            }
            else
            {
                levelPath = $"Levels/Level {levelViewMapping}";
            }


            Debug.Log("levelPath: " + levelPath);
            var textAsset = Resources.Load<TextAsset>(levelPath);
            // if (isUseNewLevel && textAsset == null)
            // {
            //     levelPath = $"Levels/Level {levelViewMapping}";
            //     textAsset = Resources.Load<TextAsset>(levelPath);
            // }

            levelConfig = JsonConvert.DeserializeObject<LevelConfigNew>(textAsset.text);
        }
        else if (PuzzleState.PlayingMode == PlayingMode.Challenge)
        {
        }
    }

    private IEnumerator LoadLevelConfig()
    {
#if TOOL_VERSION
        var loading = true;
        levelIndex = PlayerPrefs.GetInt("EditorPlayLevel", 1);
        var filePath = PlayerPrefs.GetString("localPath") + $"/Level {levelView}.json";
        if (File.Exists(filePath))
        {
            var content = File.ReadAllText(filePath);
            var config = JsonConvert.DeserializeObject<LevelConfig>(content);
            levelConfig = config.GetLevelConfigNew();
            levelConfig.Init();
            loading = false;
        }

        while (loading)
        {
            yield return null;
        }
#endif
        if (levelConfig.colors.Count == 0)
            levelConfig.colors.Add(Random.Range(0, 6));
        yield return null;
    }

    public void SetLevel(LevelConfigNew level) => levelConfig = level;

    private Bubble SpawnBubble()
    {
        var bubble = Instantiate(bubblePrefab, boardContainer).GetComponent<Bubble>();
        return bubble;
    }

    #region BOARD

    private void InitBoard()
    {
        levelConfig.Init();
        OnReadFileDone?.Invoke();

        boardContainer.position = new Vector3(0, -_camera.orthographicSize);

        _offset = new Vector3((levelConfig.col - 1) / 2f, 0.5f);
        for (int i = 0; i < levelConfig.row; i++)
        {
            int amount = levelConfig.col;
            for (int j = 0; j < amount; j++)
            {
                var ballInfo = levelConfig.rows[i].Cells[j];
                if (ballInfo.C < 0 || ballInfo.T == BubbleType.Empty)
                {
                    if (j == amount - 1)
                        ballInfo = levelConfig.rows[i].Cells[j - 1];
                    else
                        continue;
                }

                var bubble = SpawnBubble();
                bubble.gameObject.layer = LayerDefine.BallOnBoard;
                bubbles.Add(bubble);

                SpawnBubblePosition(bubble, i, j);
                if (i == 0)
                {
                    bubble.isRoot = true;
                    if (ballInfo.T != BubbleType.Cloud)
                        rootBubbles.Add(bubble);
                }

                //position.x -= _offset.x;
                // bubble.transform.localPosition = position;
                bubble.SetData(ballInfo, isResetName: true);
            }
        }

        aimController.isOdd = levelConfig.col % 2 == 0;

        if (gemCount > 0)
        {
            collectMode = CollectMode.Gem;
        }
        else
        {
            collectMode = CollectMode.Color;
        }

        var _defaultAspect = 1080 / 1920f;
        if (_camera.aspect <= _defaultAspect)
        {
            var numColl = 11;
            if (BubblePuzzleLogic.I != null && BubblePuzzleLogic.I.levelConfig != null)
            {
                numColl = BubblePuzzleLogic.I.levelConfig.col;
            }

            var width = numColl;
            _camera.orthographicSize = width / _camera.aspect / 2;
        }
        else
        {
            _camera.orthographicSize = 12.83f;
        }

        limitTop.position = new Vector3(0, Camera.main.orthographicSize);
    }

    private void SpawnBubblePosition(Bubble bubble, int row, int col)
    {
        float radian = (2 * Mathf.PI / levelConfig.col * col);
        if (row % 2 != 0)
        {
            radian += ((2 * Mathf.PI / levelConfig.col * 1) / 2);
        }

        Vector3 spawnDir = new Vector3(Mathf.Sin(radian), 0, Mathf.Cos(radian));
        Vector3 pos = boardContainer.position + spawnDir * (SpawnRadius + offsetCurve.Evaluate((float)row / levelConfig.row));
        pos.y = -rowDistance * row;
        bubble.transform.localPosition = pos;
        bubble.transform.localScale = scaleCurve.Evaluate((float)row / levelConfig.row) * Vector3.one;
    }

    public float GetMinY()
    {
        float minY = float.MaxValue;
        foreach (var ball in bubbles)
        {
            if (ball.Pos.y < minY)
            {
                minY = ball.Pos.y;
            }
        }

        return minY;
    }

    private void MoveBoard(TweenCallback onComplete, float moveSpeed, Ease ease = Ease.OutQuad)
    {
        PuzzleState.BlockUserInput = true;
        _boardState = BoardState.BoardMoving;
        var minY = GetMinY();
        var distance = limitBottom.position.y - minY;
        var nextContentPos = boardContainer.position.y + distance;
        if (nextContentPos < limitTop.position.y)
        {
            float delta = limitTop.position.y - nextContentPos;
            distance += delta;
        }

        nextContentPos = boardContainer.position.y + distance;

        boardContainer.DOKill();
        //ShowBubbleBeforeBoardScroll(distance);

        if (disableBubblePhysicsBeforeMove)
            DisablePhysicBallBeforeBoardMove();

        var duration = Mathf.Abs(distance) / moveSpeed;
        PuzzleState.IsScrolling = true;

        if (moveSpeed == 0)
        {
            duration = 0;
        }

        if (duration == 0)
        {
            boardContainer.position += distance * Vector3.up;
            _boardState = BoardState.None;
            onComplete?.Invoke();
            EnablePhysicBallAfterBoardMove();
            Time.timeScale = timeScale;
            HideOutsideBubble();

#if UNITY_EDITOR
            CountObjects();
#endif
            PuzzleState.IsScrolling = false;
            // Physics2D.SyncTransforms();

            // Check các loại bóng có trong màn hình
            CheckBubbleInsideView();
        }

        else
        {
            StartCoroutine(Move(boardContainer, duration, nextContentPos, () =>
            {
                _boardState = BoardState.None;
                onComplete?.Invoke();
                EnablePhysicBallAfterBoardMove();
                Time.timeScale = timeScale;
                HideOutsideBubble();

#if UNITY_EDITOR
                CountObjects();
#endif
                PuzzleState.IsScrolling = false;
                // Physics2D.SyncTransforms();

                // Check các loại bóng có trong màn hình
                CheckBubbleInsideView();
            }));
        }
    }

    private void CheckBubbleInsideView()
    {
        float top = PuzzleState.Camera.orthographicSize;
        bubblesInsideView.Clear();
        foreach (var key in bubbleInsideView.Keys.ToList())
        {
            bubbleInsideView[key] = false;
        }

        foreach (Bubble bubble in bubbles)
        {
            if (bubble.Trans.position.y < top)
            {
                if (bubble.IsColorBubble)
                {
                    if (bubble.IsDou)
                    {
                        bubbleInsideView[bubble.duoC1] = true;
                        bubbleInsideView[bubble.duoC2] = true;
                    }
                    else
                    {
                        bubbleInsideView[bubble.ballInfo.C] = true;
                    }
                }

                bubblesInsideView.Add(bubble);
            }
        }
    }

    IEnumerator Move(Transform t, float time, float distance, Action onComplete)
    {
        if (isMatchedThisShot && delayBeforeBoardMove > 0)
            yield return new WaitForSeconds(delayBeforeBoardMove);
        if (!PuzzleState.IsPlaying)
            yield break;

        float currentTime = 0;

        Vector3 p1 = t.position;
        Vector3 p2 = new Vector3(0, distance, t.position.z);

        float screenHeight = Screen.height * 1.125f;
        float posY = 0;
        while (currentTime < time)
        {
            currentTime += Time.deltaTime;
            float lerpValue = moveCurve.Evaluate(currentTime / time);
            Vector3 result = Vector3.LerpUnclamped(p1, p2, lerpValue);
            t.position = result;
            // Debug.Log($"CurrentTime: {currentTime}, Pos: {result}");

            foreach (var bubble in bubbles)
            {
                posY = Camera.main.WorldToScreenPoint(bubble.Pos).y;
                if (posY > 0 && posY < screenHeight)
                {
                    if (!bubble.centerObj.activeInHierarchy)
                    {
                        bubble.centerObj.SetActive(true);
                    }
                }
                else
                {
                    if (bubble.centerObj.activeInHierarchy)
                    {
                        bubble.centerObj.SetActive(false);
                    }
                }
            }

            yield return null;
        }

        onComplete?.Invoke();
    }


    private void CountObjects()
    {
        if (!showCountObject) return;
        var dict = new Dictionary<string, int>
        {
            {
                "red",
                bubbles.Count(s =>
                    ((s.IsSingleBall || s.ballInfo.T == BubbleType.Paint) && s.ballInfo.C == 0) ||
                    (s.IsDou && (s.ballInfo.C == s.duoC1 || 0 == s.duoC1 || s.duoC2 == 0)))
            },
            {
                "gre",
                bubbles.Count(s =>
                    ((s.IsSingleBall || s.ballInfo.T == BubbleType.Paint) && s.ballInfo.C == 1) ||
                    (s.IsDou && (s.ballInfo.C == s.duoC1 || 1 == s.duoC1 || s.duoC2 == 1)))
            },
            {
                "blu",
                bubbles.Count(s =>
                    ((s.IsSingleBall || s.ballInfo.T == BubbleType.Paint) && s.ballInfo.C == 2) ||
                    (s.IsDou && (s.ballInfo.C == s.duoC1 || 2 == s.duoC1 || s.duoC2 == 2)))
            },
            {
                "ora",
                bubbles.Count(s =>
                    ((s.IsSingleBall || s.ballInfo.T == BubbleType.Paint) && s.ballInfo.C == 3) ||
                    (s.IsDou && (s.ballInfo.C == s.duoC1 || 3 == s.duoC1 || s.duoC2 == 3)))
            },
            {
                "pin",
                bubbles.Count(s =>
                    ((s.IsSingleBall || s.ballInfo.T == BubbleType.Paint) && s.ballInfo.C == 4) ||
                    (s.IsDou && (s.ballInfo.C == s.duoC1 || 4 == s.duoC1 || s.duoC2 == 4)))
            },
            {
                "sky",
                bubbles.Count(s =>
                    ((s.IsSingleBall || s.ballInfo.T == BubbleType.Paint) && s.ballInfo.C == 5) ||
                    (s.IsDou && (s.ballInfo.C == s.duoC1 || 5 == s.duoC1 || s.duoC2 == 5)))
            },
        };
        dict.Add("total", dict["red"] + dict["gre"] + dict["blu"] + dict["ora"] + dict["pin"] + dict["sky"]);
        Debug.Log(JsonConvert.SerializeObject(dict));
    }

    private void ShowBubbleBeforeBoardScroll(float range)
    {
        float outsizeY = _camera.orthographicSize + 1 - range;
        foreach (var bubble in bubbles)
        {
            if (bubble.Trans.position.y < outsizeY && !bubble.center.gameObject.activeInHierarchy)
            {
                bubble.center.gameObject.SetActive(true);
            }
        }
    }

    private void HideOutsideBubble()
    {
        float outsizeY = _camera.orthographicSize + 1;
        foreach (var bubble in bubbles)
        {
            if (bubble.Trans.position.y > outsizeY)
            {
                bubble.center.gameObject.SetActive(false);
            }
        }
    }

    private void DisablePhysicBallBeforeBoardMove()
    {
        colliderBoardContainer.enabled = false;
        foreach (var ball in bubbles)
        {
            ball.col.enabled = false;
        }
    }

    private void EnablePhysicBallAfterBoardMove()
    {
        foreach (var ball in bubbles)
        {
            ball.col.enabled = true;
        }

        colliderBoardContainer.enabled = true;
    }

    #endregion

    #region CREAT START BUBBLE

    private void CreateShootBubbles()
    {
        currentBubble = CreateShootBubble(currentShootHolder.position);
        currentBubble.Trans.localScale = Vector3.one;
        aimController.iAim.SetLine(currentBubble.ballInfo.C, false);
        if (_shootRemain > 1)
        {
            nextBubble = CreateShootBubble(nextShootHolder.position);
            nextBubble.Trans.localScale = Vector3.one * PuzzleConfig.NextBubbleScale;
        }

        if (_shootRemain <= 1)
        {
            GameController.I.completePopup.SetActive(true);
        }
    }

    private Bubble CreateShootBubble(Vector3 position)
    {
        var bubble = SpawnBubble();
        bubble.transform.position = position;
        var color = GetNextBubbleColor();
        bubble.SetData(new BallInfo { C = color, T = 0 });
        bubble.RemoveColor();
        bubble.Trans.SetParent(startBalls);
        bubble.Trans.gameObject.layer = LayerDefine.StartBall;
        bubble.SetOrderInLayer(-1);
        return bubble;
    }

    private int GetRandomRateValue()
    {
        int rand = Random.Range(0, 101);
        var keys = startBallRateRandom.Keys.ToList();
        for (int i = 0; i < keys.Count; i++)
        {
            if (rand < startBallRateRandom[keys[i]])
            {
                return keys[i];
            }
        }

        Debug.LogError("GetRandomRateValue Wrong");
        return levelConfig.colors[0];
    }

    public int GetRandomColorOnConfigList(bool isAll = true, bool useRandomRate = false, int ignoreColor = -1,
        bool isInsideView = false)
    {
        if (isAll)
        {
            return levelConfig.colors.RandomItem();
        }

        if (useRandomRate)
        {
            int value = GetRandomRateValue();
            if (countDict[value] > 0)
            {
                return value;
            }
        }

        var lst = new List<int>();
        for (var i = 0; i < countDict.Count; i++)
        {
            if (countDict[i] > 0 && i != ignoreColor)
            {
                lst.Add(i);
            }
        }

        if (isInsideView)
        {
            // Set % cho các màu
            for (int i = 0; i < 6; i++)
            {
                if (countDict[i] == 0)
                {
                    countRate[i] = 0;
                }
                else if (bubbleInsideView[i])
                {
                    countRate[i] = 100;
                }
                else
                {
                    countRate[i] = 50;
                }
            }

            float random = Random.Range(0, countRate.Sum(s => s.Value));
            for (int i = 0; i < 6; i++)
            {
                if (countDict[i] > 0)
                {
                    if (random < countRate[i])
                    {
                        return i;
                    }
                    else
                    {
                        random -= countRate[i];
                    }
                }
            }

            return lst.Count > 0 ? lst.RandomItem() :
                levelConfig.colors.Count > 0 ? levelConfig.colors.RandomItem() : Random.Range(0, 6);
        }
        else
        {
            return lst.Count > 0 ? lst.RandomItem() :
                levelConfig.colors.Count > 0 ? levelConfig.colors.RandomItem() : Random.Range(0, 6);
        }
    }

    /// <summary>
    /// Return shoot ball id
    /// </summary>
    /// <returns></returns>
    private int GetNextBubbleColor()
    {
        int c;
        if (_spawnCount < levelConfig.startBalls.Count)
        {
            // Get color by start ball config
            c = levelConfig.startBalls[_spawnCount];
            if (countDict[c] <= 0)
            {
                c = GetRandomColorOnConfigList(false, startBallRateRandom.Keys.Count > 0);
            }
        }
        else
        {
            c = GetRandomColorOnConfigList(false, startBallRateRandom.Keys.Count > 0, isInsideView: true);
        }

        // if (IsMissedShot)
        // {
        //     IsMissedShot = false;
        //     // spawn ra màu có thể bắn ăn
        //     var keys = countDict.Keys.ToList();
        //     keys.Shuffle();
        //     foreach (var key in keys)
        //     {
        //         if (countDict[key] > 0)
        //         {
        //             var dict = GetSuggestionList(key);
        //             if (dict.Keys.Count > 0 && dict.Values.ToList().FindAll(s => s.Count >= 2).Count > 0)
        //             {
        //                 c = key;
        //             }
        //         }
        //     }
        // }

        // if (c == oldColor)
        // {
        //     shootColorCount++;
        //     if (shootColorCount >= 2)
        //     {
        //         shootColorCount = 0;
        //         c = GetRandomColorOnConfigList(false, startBallRateRandom.Keys.Count > 0, oldColor, isInsideView: true);
        //         Debug.Log("Enter");
        //     }
        // }

        oldColor = c;

        _spawnCount++;
        return c;
    }

    private void MoveStartBall()
    {
        float minY = GetMinY();
        startBalls.position = new Vector3(0, minY - startBallOffset, -30);
        // startBalls.position = new Vector3(0, minY - 6.36f);
        startBalls.gameObject.SetActive(true);
        float distance = limitBottom.position.y - minY;
        startBalls.position += distance * Vector3.up + new Vector3(0, 0, 0);
        //startBalls.DOMoveY(distance, boardMoveUpSpeed).SetSpeedBased(true).SetRelative(true).SetTarget(boardContainer);
    }

    #endregion

    #endregion

    #region USER INPUT

    private void UserInput()
    {
        if (!PuzzleState.IsPlaying || PuzzleState.BlockUserInput || PuzzleState.IsTutorialBlockDrag ||
            PuzzleState.IsSwapping || PuzzleState.IsShooting || currentBubble == null ||
            PuzzleState.IsWaitToCheckTutorial) return;
        if (Input.GetMouseButtonDown(0) && !PuzzleState.IsRotate)
        {
            RemoveBallSuggestion();
            if (!IsOverUI())
            {
                _isMouseDown = true;
                OnMouseDown?.Invoke();
                var hit = MouseUtility.GetRayHitUnderMouse(mouseInputCastLayer, true);
                if (hit.collider != null)
                {
                    aimController.Hide();
                    Aim(hit.point);
                    _isAiming = true;
                }
            }
        }
        else if (Input.GetMouseButton(0) && !PuzzleState.IsRotate)
        {
            if (_isAiming)
            {
                Aim(MouseUtility.GetRayHitUnderMouse(mouseInputCastLayer, true).point);
            }
            else
            {
                if (!IsOverUI() && !PuzzleState.IsTutorialShowing)
                {
                    var hit = MouseUtility.GetRayHitUnderMouse(mouseInputCastLayer, true);
                    if (!(hit.collider != null))
                    {
                        Aim(hit.point);
                        _isAiming = true;
                    }
                }
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            if (PuzzleState.IsRotate)
            {
                PuzzleState.IsRotate = false;
            }
            else
            {
                MouseUp(GetMousePosition());
            }
        }
    }

    public void MouseUp(Vector3 mousePos)
    {
        OnMouseUp?.Invoke();
        if (_isAiming && aimController.path.Count > 0 && aimController.canShoot)
        {
            lastShootPosition = mousePos;
            var direct = mousePos - currentShootHolder.position;
            OnShoot?.Invoke(currentBubble, booster, direct);
            if (booster == null)
            {
                UserShoot(currentBubble, direct);
                currentBubble = null;
            }
            else
            {
                OnBoosterShoot?.Invoke(booster.ballInfo.T);
                UserShoot(booster, direct);
            }

            aimController.Hide();
        }
        else
        {
            if (_isMouseDown)
            {
                var hit = Physics2D.Raycast(mousePos, Vector2.zero, 1, startBallLayer);
                if (hit.collider != null && booster == null)
                {
                    SwapBubble();
                }
            }
        }

        _isMouseDown = false;
        _isAiming = false;
        RemoveBallSuggestion();
    }


    public void EnableUserInput()
    {
        PuzzleState.BlockUserInput = false;
    }

    public void DisableUserInput()
    {
        PuzzleState.BlockUserInput = true;
    }

    private Vector3 GetMousePosition()
    {
        Vector3 pos = _camera.ScreenToWorldPoint(Input.mousePosition);
        pos.z = 0;
        return pos;
    }

    private bool IsOverUI()
    {
#if !UNITY_EDITOR && !UNITY_STANDALONE_WIN
        return EventSystem.current.IsPointerOverGameObject(0);
#endif
        return EventSystem.current.IsPointerOverGameObject();
    }

    public void SwapBubble()
    {
        if (nextBubble == null || currentBubble == null || nextBubble == currentBubble)
        {
            return;
        }

        IsSwitch = !IsSwitch;
        PuzzleState.IsSwapping = true;
        var temp = currentBubble;
        currentBubble = nextBubble;
        nextBubble = temp;
        currentBubble.Trans.DOPath(GetPath(nextShootHolder.position, currentShootHolder.position, 1), .2f,
            PathType.CatmullRom).SetTarget(transform).OnComplete(() => { PuzzleState.IsSwapping = false; });
        currentBubble.Trans.DOScale(1, .2f).SetTarget(transform);
        nextBubble.Trans.DOPath(GetPath(currentShootHolder.position, nextShootHolder.position, -1), .2f,
            PathType.CatmullRom).SetTarget(transform);
        nextBubble.Trans.DOScale(PuzzleConfig.NextBubbleScale, .2f).SetTarget(transform);
        aimController.iAim.SetLine(currentBubble.ballInfo.C, false);
        PuzzleSound?.PlaySwapSound();
        RemoveBallSuggestion();
        swapSuggestController.HideSuggest();
    }

    public void AddMoreMove(int amount)
    {
        if (ShootRemain == 1)
        {
            nextBubble = CreateShootBubble(nextShootHolder.position);
            nextBubble.transform.DOScale(PuzzleConfig.NextBubbleScale, .2f).SetTarget(transform);
        }

        ShootRemain += amount;
        PuzzleState.IsSwapping = false;
        PuzzleState.BlockUserInput = false;
        OnAddMoreMove?.Invoke(amount);
    }

    #endregion

    #region SHOOT

    private void UserShoot(Bubble bubble, Vector3 direct)
    {
        isMatchedThisShot = false;
        _isDropChecked = false;
        // ShootRemain--;
        OnShootChanged?.Invoke();
        _needUpdateAfterShoot = true;
        PuzzleState.IsShooting = true;
        swapSuggestController.ShowSuggest();
        curRow = curCol = -1;
        PuzzleSound?.PlayShootSound();
        PuzzleState.BlockUserInput = true;

        if (booster != null)
        {
            isShootBooster = true;
            DecreaseBoosterWhenUsed(bubble);
            if (booster.ballInfo.T == BubbleType.Beam)
            {
                CheckClearForLaserBooster(booster, direct);
                PoolWrapper.I.RemoveAllNotices();
                return;
            }
        }
        else
        {
            lastColor = bubble.ballInfo.C;
        }

        if (bubble.ballInfo.T == BubbleType.Lightning)
        {
            var pos = aimController.path[aimController.path.Count - 1] + Vector3.up * rowDistance;
            if (pos.y < limitTop.position.y) aimController.path[aimController.path.Count - 1] = pos;
        }

        var path = aimController.path.ToArray();
        bubble.canCombo = true;
        bubble.Trans.DOPath(path, aimController.GetLength() / ballMoveSpeed).SetEase(Ease.Linear).SetTarget(transform)
            .OnComplete(() =>
            {
                // Physics2D.SyncTransforms();
                if (bubble.Pos.y < _camera.orthographicSize + 300)
                {
                    var pos = bubble.Pos;
                    PuzzleSound?.PlayHitSound();
                    PuzzleState.IsShooting = false;
                    _needCheckAfterShoot = true;
                    bubble.RemoveTrail();
                    if (ActiveBooster(bubble))
                    {
                        StartCoroutine(CheckColorAfterUseBooster());
                        RemoveBooster();
                        PoolWrapper.I.RemoveAllNotices();
                        return;
                    }

                    needCreateNewBall = true;
                    isClearChecking = true;
                    AddShootBubbleToBoard(bubble);
                    CheckMatches(bubble, bubble, false);
                }
                else
                {
                    PuzzleState.BlockUserInput = false;
                    PuzzleState.IsShooting = false;
                    if (bubble.IsBooster)
                    {
                        // StartCoroutine(CheckColorAfterUseBooster());
                        RemoveBooster();
                        PoolWrapper.I.RemoveAllNotices();
                        if (ShootRemain <= 0)
                        {
                            SetOutOfMove();
                        }
                    }
                    else
                    {
                        Destroy(bubble.gameObject);
                        if (ShootRemain <= 0)
                        {
                            SetOutOfMove();
                        }
                        else
                        {
                            CreateNewShootBubbleAfterShoot();
                        }
                    }
                }
            });
    }

    IEnumerator CheckColorAfterUseBooster()
    {
        yield return null;
        yield return new WaitUntil(() => clearCount <= 0 && _dropCount <= 0 && specialBallActiveCount <= 0);

        if (currentBubble != null && countDict[currentBubble.ballInfo.C] == 0)
        {
            currentBubble.SetData(new BallInfo { C = GetNextBubbleColor(), T = 0 });
            currentBubble.RemoveColor();
            aimController.iAim.SetLine(currentBubble.ballInfo.C, false);
        }

        if (nextBubble != null && countDict[nextBubble.ballInfo.C] == 0)
        {
            nextBubble.SetData(new BallInfo { C = GetNextBubbleColor(), T = 0 });
            nextBubble.RemoveColor();
        }

        CountObjects();
    }

    private void AddShootBubbleToBoard(Bubble bubble)
    {
        bubble.Trans.SetParent(boardContainer);
        bubbles.Add(bubble);
        OnBubbleListChanged?.Invoke();
        bubble.SetOnBoard();
        if (bubble.Trans.localPosition.y >= -0.6f)
        {
            bubble.isRoot = true;
            rootBubbles.Add(bubble);
        }

        AddColor(bubble.ballInfo.C);
        bubble.UpdateNeighbors();
        foreach (var neighbor in bubble.neighbors)
        {
            if (!neighbor.neighbors.Contains(bubble))
            {
                neighbor.neighbors.Add(bubble);
                if (neighbor.hasConnectedWithRoot) bubble.hasConnectedWithRoot = true;
            }
        }
    }

    private void CreateNewShootBubbleAfterShoot()
    {
        PuzzleState.IsSwapping = true;
        if (PuzzleState.IsPlaying == false)
        {
            PuzzleState.IsSwapping = false;
            return;
        }

        if (nextBubble == null && ShootRemain > 1)
        {
            nextBubble = CreateShootBubble(nextShootHolder.position);
            nextBubble.Trans.localScale = Vector3.zero;
            nextBubble.Trans.DOScale(PuzzleConfig.NextBubbleScale, .2f).SetTarget(transform);
        }

        currentBubble = nextBubble;
        if (countDict[currentBubble.ballInfo.C] == 0)
        {
            currentBubble.SetData(new BallInfo { C = GetNextBubbleColor(), T = 0 });
            currentBubble.RemoveColor();
            aimController.iAim.SetLine(currentBubble.ballInfo.C, false);
        }

        aimController.iAim.SetLine(currentBubble.ballInfo.C, false);
        currentBubble.Trans.DOMove(currentShootHolder.position, 1)
            .SetTarget(transform)
            .OnComplete(() =>
            {
                PuzzleState.IsSwapping = false;
                Aim(lastShootPosition, 3, 1);
            });
        currentBubble.transform.DOScale(1f, .2f).SetTarget(transform);
        nextBubble = null;
        if (ShootRemain > 1)
        {
            nextBubble = CreateShootBubble(nextShootHolder.position);
            nextBubble.Trans.localScale = Vector3.zero;
            nextBubble.Trans.DOScale(PuzzleConfig.NextBubbleScale, 1f).SetEase(Ease.OutQuad).SetTarget(transform);
        }
    }

    private Vector3[] GetPath(Vector3 start, Vector3 end, int sign)
    {
        Vector3[] path = new Vector3[3];
        path[0] = start;
        path[1] = (start + end) / 2 + new Vector3(.25f * sign, .5f * sign);
        path[2] = end;
        return path;
    }

    private void UpdateMorphAfterShot()
    {
        foreach (var bubble in morphBubbles)
        {
            bubble.MorphRandomColor();
        }
    }

    private void UpdateSwitchSpikeAfterShot()
    {
        foreach (var bubble in switchSpikes)
        {
            bubble.SwitchSpikeSwitch();
        }
    }

    private void UpdateGhostBubbleAfterShoot()
    {
        foreach (Bubble bubble in ghostBubbles)
        {
            bubble.BubbleGhostSwitchState();
            bubble.UpdateCanMatch();
        }
    }

    private void UpdateRocketAfterShoot()
    {
        float y = _camera.orthographicSize + 1;
        foreach (var bubble in rocketBubbles)
        {
            if (bubble.Pos.y < y && bubble.isLive)
                bubble.RocketRotate();
        }
    }

    private void UpdateDelayedFallBubbleAfterShoot()
    {
        foreach (var bubble in delayedFallBubbles)
        {
            if (bubble != null && bubble.gameObject != null)
                bubble.FallBubble();
        }

        delayedFallBubbles.Clear();
    }

    #endregion

    #region CHECK MATCHING

    private bool CheckNeighborIsPower(Bubble bubble)
    {
        bool neighborIsPower = false;
        foreach (var neighbor in bubble.neighbors)
        {
            if (neighbor.IsBooster && neighbor.isLive)
            {
                neighborIsPower = true;
                neighbor.isLive = false;
                neighbor.clearDelay = 0;
                neighbor.canCombo = true;
                _clearList.Add(neighbor);
            }
        }

        return neighborIsPower;
    }

    private void GetMatches(Bubble bubble, bool removeInteract)
    {
        if (bubble.ballInfo.T == BubbleType.Cloud)
            GetBubblesMatchingWithSpecialType(bubble.neighbors.ToArray(), clearDeltaTime, BubbleType.Cloud);
        else if (bubble.ballInfo.T != BubbleType.ColorBrush)
        {
            if (bubble.IsDou)
            {
                GetBubblesMatching(bubble, bubble.neighbors.ToArray(), bubble.duoC1, clearDeltaTime, removeInteract);
                ResetCheckCondition();
                GetBubblesMatching(bubble, bubble.neighbors.ToArray(), bubble.duoC2, clearDeltaTime, removeInteract);
                _clearList = _clearList.Distinct().ToList();
            }
            else
            {
                GetBubblesMatching(bubble, bubble.neighbors.ToArray(), bubble.ballInfo.C, clearDeltaTime,
                    removeInteract);
            }
        }
    }

    private void PlayHitFx(Bubble origin)
    {
        foreach (var neighbor in origin.neighbors)
            if (!_clearList.Contains(neighbor))
                neighbor.PlayHitFx(origin.Pos);
    }

    private bool GetCoinMatched(Bubble shootBubble)
    {
        bool hasCoin = false;
        var coinNeighbors = new List<Bubble>();
        foreach (var neighbor in shootBubble.neighbors)
        {
            if (neighbor.ballInfo.T == BubbleType.Coin)
            {
                hasCoin = true;
                coinNeighbors.Add(neighbor);
            }
        }

        GetBubblesMatchingWithSpecialType(coinNeighbors.ToArray(), clearDeltaTime, BubbleType.Coin);
        foreach (Bubble bubble1 in _clearList)
        {
            if (bubble1.ballInfo.T == BubbleType.Coin)
                bubble1.isLive = false;
        }

        return hasCoin;
    }

    private void GetCoinMatched(Bubble shootBubble, List<Bubble> resultList)
    {
        var coinNeighbors = new List<Bubble>();
        foreach (var neighbor in shootBubble.neighbors)
        {
            if (neighbor.ballInfo.T == BubbleType.Coin)
            {
                coinNeighbors.Add(neighbor);
            }
        }

        GetBubblesCoinMatching(coinNeighbors.ToArray(), clearDeltaTime, resultList);
        foreach (Bubble bubble1 in _clearList)
        {
            if (bubble1.ballInfo.T == BubbleType.Coin)
            {
                bubble1.isLive = false;
            }
        }
    }


    private void ResetCheckCondition()
    {
        foreach (var bubble1 in bubbles)
        {
            bubble1.isLoseConnectOnMatched = false;
            bubble1.isInClearList = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="bubble"></param>
    /// <param name="agent">Bóng gây tương tác lên bóng hện taại</param>
    /// <param name="range">Số lượng tối thiểu trong list để biết là có clear hay không</param>
    /// <param name="removeInteract">true nếu đó chỉ là hàm kiểm tra chứ không clear</param>
    public void CheckMatches(Bubble bubble, Bubble agent, bool removeInteract, int range = 3)
    {
        _clearList.Clear();
        _clearList.Add(bubble);
        ResetCheckCondition();
        bubble.isInClearList = true;
        var hasCoin = GetCoinMatched(bubble);
        GetMatches(bubble, removeInteract);
        if (removeInteract) return;
        _hasAddCombo = true;
        PlayHitFx(bubble);
        var neighborIsPower = CheckNeighborIsPower(bubble);
        if (_clearList.Count >= range || neighborIsPower || hasCoin)
        {
            // if (SoundController.IsVibration) GameRemoteConfigSingleton.I.UseVibration(1);
            bubble.clearDelay = 0;

            _hasAddCombo = false;
            _comboCount++;
            InteractWithNeighbors(bubble, true);

            DestroyTheMatchedBubbles(agent, _clearList.ToList(), bubble.ballInfo.C, bubble.ballInfo.T);
            IsDelayMissedShoot = false;

            bubble.Trans.DOKill();
            bubble.rigid.drag = 0.1f;
            bubble.rigid.AddForce(new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 0f), Random.Range(-1f, 0f)).normalized * 20,
                ForceMode.Impulse);


            isMatchedThisShot = true;
        }
        else
        {
            if (!IsDelayMissedShoot)
            {
                IsDelayMissedShoot = true;
                IsMissedShot = true;
            }
            else
            {
                IsDelayMissedShoot = false;
            }

            // if (SoundController.IsVibration) GameRemoteConfigSingleton.I.UseVibration(3);
            _delayDrop = 0;
            _comboCount = 1;
            CheckOutTheBubbleHiddenBelow(bubble);
            InteractWithNeighbors(bubble, false);
            if (isUpdateBeamProgress) isUpdateBeamProgress = false;
            else OnShootWithoutClear?.Invoke();
            bubble.InteractWithNeighbors_NoMatch();
            StartCoroutine(CRProcessNoneMatching(agent));
        }

        isClearChecking = false;
    }


    private IEnumerator CRProcessNoneMatching(Bubble agent)
    {
        yield return null;
        yield return new WaitUntil(() => specialBallActiveCount <= 0 && !isBlackHoldRunning);
        yield return CheckDrop(agent);
        _comboCount = 0;
    }

    private void UpdateAfterShoot()
    {
        if (!_needUpdateAfterShoot) return;
        _needUpdateAfterShoot = false;
        // UpdateRocketAfterShoot();
        // UpdateMorphAfterShot();
        // UpdateSwitchSpikeAfterShot();
        // UpdateGhostBubbleAfterShoot();
        UpdateDelayedFallBubbleAfterShoot();
    }

    void CheckIncreaseBeam(bool isClear)
    {
        if (!isClear)
        {
            OnClear?.Invoke(BubbleType.Normal);
        }
    }

    private void InteractWithNeighbors(Bubble bubble, bool isMatching)
    {
        if (bubble.ballInfo.T == BubbleType.Cloud || bubble.ballInfo.T == BubbleType.Fire) return;
        bool needDestroyShootBubble = false;
        bool isNearCloudOrWoodBubble = false;
        bool isBreak = false;
        foreach (Bubble bubble1 in bubble.neighbors.ToArray())
        {
            if (bubble1.ballInfo.T == BubbleType.Cloud)
            {
                isNearCloudOrWoodBubble = true;
                isBreak = true;
                needDestroyShootBubble = true;
            }
            else if (bubble1.ballInfo.T == BubbleType.Wood)
            {
                isNearCloudOrWoodBubble = true;
                isBreak = bubble1.ballInfo.C == 0;
                needDestroyShootBubble = true;
            }
        }

        if (isBreak)
        {
            isUpdateBeamProgress = true;
            CheckIncreaseBeam(isMatching);
        }

        List<Bubble> cloudList = new List<Bubble>();
        foreach (var neighbor in bubble.neighbors.ToArray())
        {
            if (neighbor.ballInfo.T == BubbleType.Cloud)
            {
                neighbor.DestroyCloudBubble();
                cloudList.Add(neighbor);
            }
            else if (neighbor.ballInfo.T == BubbleType.Wood)
            {
                if (_hasAddCombo)
                {
                    _hasAddCombo = false;
                    _comboCount++;
                }

                neighbor.RemoveConnections(bubble.ballInfo.T);
                neighbor.DestroyBubble_Wood();
                if (_clearList.Count < 3 && bubble.isLive)
                {
                    bubble.RemoveConnections(bubble.ballInfo.T);
                    if (bubble.ballInfo.T == BubbleType.Fire)
                    {
                    }
                    else
                    {
                        bubble.DestroyBubble_Color(bubble.ballInfo.C);
                    }

                    bubbles.Remove(bubble);
                    OnBubbleListChanged?.Invoke();
                }

                if (nextBubble != null && nextBubble.ballInfo.C < 0)
                {
                    bubble.isLoseConnectOnMatched = true;
                }
            }
            else if (neighbor.ballInfo.T == BubbleType.Frozen)
            {
                if (isNearCloudOrWoodBubble)
                {
                    neighbor.ShowIceBreakFx();
                    neighbor.SetData(new BallInfo { C = bubble.ballInfo.C, T = 0 });
                }
            }
        }

        if (cloudList.Count > 0)
        {
            CheckConnectWhenDestroyCloudBubble(cloudList, bubble);
        }

        if (needDestroyShootBubble && !isMatching && bubble.isLive)
        {
            bubble.RemoveConnections(BubbleType.Normal, false);
            bubble.StartCoroutine(bubble.DestroyBubble(bubble, bubble.ballInfo.C, BubbleType.Normal, false));
        }
    }

    private void CheckConnectWhenDestroyCloudBubble(List<Bubble> lst, Bubble agent)
    {
        foreach (var bubble1 in bubbles)
        {
            if (!bubble1.hasConnectedWithRoot)
            {
                bubble1.isConnectChecked = false;
            }
            else
            {
                var neighbors = bubble1.neighbors;
                if (neighbors.Count == 1 && neighbors[0].ballInfo.T == BubbleType.Cloud)
                {
                    bubble1.isConnectChecked = false;
                }
                else
                {
                    bubble1.isConnectChecked = true;
                }
            }
        }

        var connectedList = new List<Bubble>(lst);
        foreach (var root in lst)
        {
            GetAllBubbleHasConnectedWithRoot(root, ref connectedList);
        }

        var cloudAliveList = new List<Bubble>();
        foreach (var bubble in connectedList)
        {
            if (bubble.ballInfo.T == BubbleType.Cloud && !lst.Contains(bubble))
            {
                cloudAliveList.Add(bubble);
            }
        }

        foreach (var bubble in bubbles) bubble.isConnectChecked = false;
        foreach (var bubble in lst) bubble.isConnectChecked = true;
        var connectedWithActiveCloudList = new List<Bubble>(cloudAliveList);
        foreach (var bubble in cloudAliveList)
        {
            GetAllBubbleHasConnectedWithRoot(bubble, ref connectedWithActiveCloudList);
        }

        foreach (Bubble bubble in connectedWithActiveCloudList)
        {
            connectedList.Remove(bubble);
        }

        if (connectedList.Count > 0)
        {
            foreach (var bubble in connectedList)
            {
                bubble.RemoveConnections(bubble.ballInfo.T, true);

                if (bubble.ballInfo.T == BubbleType.Cloud)
                {
                    continue;
                }

                _dropCount++;
                bubble.isDrop = true;
                bubble.clearDelay = 0;
                bubble.Drop(comboCount, agent);
            }
        }
    }

    private void CheckOutTheBubbleHiddenBelow(Bubble bubble)
    {
        var hit = Physics2D.Raycast(bubble.Pos, Vector2.zero, 1, hiddenBallLayer);
        if (hit.collider != null)
        {
            var hiddenBubble = hit.collider.GetComponent<Bubble>();
            bubbles.Remove(hiddenBubble);
            OnBubbleListChanged?.Invoke();
            ghostBubbles.Remove(hiddenBubble);

            // Update neighbors
            foreach (var neighbor in bubble.neighbors)
            {
                neighbor.UpdateNeighbors();
            }

            foreach (var neighbor in hiddenBubble.neighbors)
            {
                neighbor.neighbors.Remove(hiddenBubble);
            }

            hiddenBubble.RemoveColor();
            Destroy(hiddenBubble.gameObject);
        }
    }

    public void RemoveBubbleFromBoard(Bubble bubble)
    {
        bubbles.Remove(bubble);
        if (bubble.isRoot) rootBubbles.Remove(bubble);
        if (bubble.ballInfo.T == BubbleType.Cloud)
        {
            cloudBubbles.Remove(bubble);
            rootBubbles.Remove(bubble);
        }

        if (bubble.ballInfo.T == BubbleType.Morph) morphBubbles.Remove(bubble);
        if (bubble.ballInfo.T == BubbleType.BlackHoleOn || bubble.ballInfo.T == BubbleType.BlackHoleOff)
            switchSpikes.Remove(bubble);
        if (bubble.ballInfo.T == BubbleType.Rocket) rocketBubbles.Remove(bubble);
        if (bubble.IsGhostBubble) ghostBubbles.Remove(bubble);
        OnBubbleListChanged?.Invoke();
    }

    private void DestroyTheMatchedBubbles(Bubble agent, List<Bubble> clearList, int color, BubbleType type,
        float multipleTime = 1, bool isCheckDrop = true)
    {
        OnClear?.Invoke(agent == null ? BubbleType.Normal : agent.ballInfo.T);
        foreach (var bubble1 in clearList)
        {
            bubble1.RemoveConnections(type);
            if (bubble1.ignoreMulti) multipleTime = 1;
            bubble1.clearDelay *= multipleTime;
            if (bubble1.clearDelay > _delayDrop) _delayDrop = bubble1.clearDelay;
        }

        foreach (var bubble1 in clearList)
        {
            StartCoroutine(bubble1.DestroyBubble(agent, color, type, true));
        }

        if (isCheckDrop)
        {
            StartCoroutine(CRUpdateAfterDestroyMatched(agent, multipleTime));
        }
    }

    public IEnumerator CRUpdateAfterDestroyMatched(Bubble agent, float multipleTime)
    {
        yield return new WaitUntil(() => clearCount <= 0 && specialBallActiveCount <= 0 && !isBlackHoldRunning);
        yield return CheckDrop(agent, multi: multipleTime);
        yield return new WaitUntil(() => clearCount <= 0);
        UpdateAfterShoot();
        DestroyAllCloudBubblesDoNotHaveAnyConnections();
        CheckWin();
    }

    private void DestroyAllCloudBubblesDoNotHaveAnyConnections()
    {
        foreach (var bubble in cloudBubbles.ToArray())
            if (bubble.neighbors.FindAll(s => s.hasConnectedWithRoot && s.ballInfo.T != BubbleType.Cloud).Count == 0)
            {
                bubble.RemoveConnections(BubbleType.Normal);
                bubble.DestroyCloudBubble();
            }
    }

    public void UpdateDrop()
    {
        foreach (var bubble in bubbles)
        {
            bubble.hasConnectedWithRoot = false;
            bubble.isConnectChecked = false;
        }

        GetDropList();
    }

    public List<Bubble> GetDropList()
    {
        var connectedList = new List<Bubble>(rootBubbles);
        //foreach (var root in rootBubbles)
        //{
        //    GetAllBubbleHasConnectedWithRoot(root, ref connectedList);
        //}
        connectedList.AddRange(GetConnectedWithRoot());
        var dropList = new List<Bubble>(bubbles);
        foreach (var bubble in connectedList)
        {
            bubble.hasConnectedWithRoot = true;
            dropList.Remove(bubble);
        }

        foreach (Bubble bubble in dropList.ToList())
        {
            if (bubble.ballInfo.T == BubbleType.Cloud)
            {
                dropList.Remove(bubble);
            }
        }

        return dropList;
    }

    private IEnumerator CheckDrop(Bubble agent, float multi = 1)
    {
        if (_comboCount == 0) _comboCount++;
        _dropSequenceList.Clear();
        foreach (var bubble in bubbles)
        {
            bubble.hasConnectedWithRoot = false;
            bubble.isConnectChecked = false;
        }

        var dropList = GetDropList();
        if (dropList.Count > 0)
        {
            //Debug.Log("Drop count = " + dropList.Count);
            foreach (var bubble in dropList)
            {
                if (bubble.isLoseConnectOnMatched)
                {
                    bubble.clearDelay = 0;
                    _dropSequenceList.Add(bubble);
                }
            }

            var tempList = new List<Bubble>(_dropSequenceList);
            List<Bubble> neighbors = new List<Bubble>();
            foreach (var bubble in tempList)
            {
                neighbors.AddRange(bubble.neighbors);
            }

            neighbors = neighbors.Distinct().ToList();
            GetDroppedBubblesWithDelayTime(neighbors, dropDeltaTime, multi);


            List<Bubble> powers = new List<Bubble>();
            foreach (var bubble in dropList)
            {
                if (bubble.IsPower)
                {
                    powers.Add(bubble);
                }
            }

            if (powers.Count > 0)
            {
                foreach (Bubble power in powers)
                {
                    dropList.Remove(power);
                    if (power.gameObject.activeInHierarchy)
                    {
                        power.clearDelay = 0;
                        power.canCombo = true;
                        power.StartCoroutine(power.DestroyBubble(null, 0, BubbleType.Normal, false));
                    }
                }

                yield return null;
            }


            _dropCount = dropList.Count;
            foreach (var bubble in dropList)
            {
                bubble.isLive = false;
                bubble.RemoveConnections(bubble.ballInfo.T, true);
            }


            yield return new WaitUntil(() => specialBallActiveCount == 0);

            if (!isDropChecking)
                _isDropChecked = true;
            // Check before drop
            foreach (var bubble in dropList)
            {
                bubble.isDrop = true;
                bubble.RemoveConnections(bubble.ballInfo.T, true);
                StartCoroutine(CRDropSequence(bubble, agent));
            }

            MoveBoard(() => { PuzzleState.BlockUserInput = false; }, boardMoveDownSpeed);
        }

        _isDropChecked = true;
        yield return null;
    }

    public IEnumerator CheckDropOnDestroy(Bubble agent, float multi = 1)
    {
        if (_comboCount == 0) _comboCount++;
        foreach (var bubble in bubbles)
        {
            bubble.hasConnectedWithRoot = false;
            bubble.isConnectChecked = false;
        }

        var dropList = GetDropList();

        if (dropList.Count > 0)
        {
            foreach (var bubble in dropList)
            {
                if (bubble.isLoseConnectOnMatched)
                {
                    bubble.clearDelay = 0;
                    _dropSequenceList.Add(bubble);
                }
            }

            var tempList = new List<Bubble>(_dropSequenceList);
            List<Bubble> neighbors = new List<Bubble>();
            foreach (var bubble in tempList)
            {
                neighbors.AddRange(bubble.neighbors);
            }

            neighbors = neighbors.Distinct().ToList();
            GetDroppedBubblesWithDelayTime(neighbors, dropDeltaTime, multi);

            _dropCount = dropList.Count;
            foreach (var bubble in dropList)
            {
                bubble.isLive = false;
                bubble.RemoveConnections(bubble.ballInfo.T, true);
            }


            if (!isDropChecking)
                _isDropChecked = true;

            foreach (var bubble in dropList)
            {
                bubble.isDrop = true;
                bubble.RemoveConnections(bubble.ballInfo.T, true);
                StartCoroutine(CRDropSequence(bubble, agent));
            }

            // MoveBoard(() => { PuzzleState.BlockUserInput = false; }, boardMoveDownSpeed);
        }

        //  _isDropChecked = true;
        yield return null;
    }

    private IEnumerator CRDropSequence(Bubble bubble, Bubble agent)
    {
        yield return new WaitForSeconds(bubble.clearDelay);
        bubble.Drop(comboCount, agent);
    }

    private List<Bubble> GetConnectedWithRoot()
    {
        var result = new List<Bubble>();
        foreach (Bubble bubble in bubbles)
        {
            if (bubble.neighbors.Find(x => x.hasConnectedWithRoot || x.isRoot))
            {
                result.Add(bubble);
                bubble.hasConnectedWithRoot = true;
                foreach (Bubble neighbor in bubble.neighbors.FindAll(nb => !nb.hasConnectedWithRoot))
                {
                    neighbor.hasConnectedWithRoot = true;
                    result.Add(neighbor);
                }
            }
        }

        return result.Distinct().ToList();
    }

    private void GetAllBubbleHasConnectedWithRoot(Bubble bubble, ref List<Bubble> bubbleHasConnectedList)
    {
        foreach (var neighbor in bubble.neighbors)
        {
            if (!neighbor.isConnectChecked && !neighbor.isRoot)
            {
                neighbor.isConnectChecked = true;
                bubbleHasConnectedList.Add(neighbor);
                GetAllBubbleHasConnectedWithRoot(neighbor, ref bubbleHasConnectedList);
            }
        }
    }


    private void GetBubblesMatching(Bubble owner, Bubble[] neighborList, int c, float delayTime,
        bool removeInteract = false)
    {
        List<Bubble> lst = new List<Bubble>();
        foreach (var bubble in neighborList)
        {
            if (!bubble.isInClearList && bubble.IsMatch(c) && bubble.CanMatch)
            {
                if (removeInteract) bubble.isPowerIgnore = true;
                bubble.isInClearList = true;
                bubble.clearDelay = delayTime;
                _clearList.Add(bubble);
                lst.AddRange(bubble.neighbors);
                foreach (var neighbor in bubble.neighbors) neighbor.isLoseConnectOnMatched = true;
            }
        }

        delayTime += clearDeltaTime;
        if (lst.Count > 0)
        {
            GetBubblesMatching(owner, lst.ToArray(), c, delayTime, removeInteract);
        }
    }

    public void GetBubblesMatching(Bubble[] neighborList, int c, ref List<Bubble> result)
    {
        List<Bubble> lst = new List<Bubble>();
        foreach (var bubble in neighborList)
        {
            if (bubble.IsMatch(c) && bubble.CanMatch && !result.Contains(bubble))
            {
                result.Add(bubble);
                lst.AddRange(bubble.neighbors);
            }
        }

        if (lst.Count > 0)
        {
            GetBubblesMatching(lst.ToArray(), c, ref result);
        }
    }

    // private void GetBubblesCloudMatching(Bubble[] neighborList, float delayTime)
    // {
    //     List<Bubble> lst = new List<Bubble>();
    //     foreach (var bubble in neighborList)
    //     {
    //         if (!bubble.isInClearList && bubble.ballInfo.T == BubbleType.Cloud)
    //         {
    //             bubble.isInClearList = true;
    //             bubble.clearDelay = delayTime;
    //             _clearList.Add(bubble);
    //             lst.AddRange(bubble.neighbors);
    //             foreach (var neighbor in bubble.neighbors)
    //             {
    //                 neighbor.isLoseConnectOnMatched = true;
    //             }
    //         }
    //     }
    //
    //     delayTime += clearDeltaTime;
    //     if (lst.Count > 0)
    //     {
    //         GetBubblesCloudMatching(lst.ToArray(), delayTime);
    //     }
    // }

    private void GetBubblesMatchingWithSpecialType(Bubble[] neighborList, float delayTime, BubbleType type)
    {
        List<Bubble> lst = new List<Bubble>();
        foreach (var bubble in neighborList)
        {
            if (!bubble.isInClearList && bubble.ballInfo.T == type)
            {
                bubble.isInClearList = true;
                bubble.clearDelay = delayTime;
                _clearList.Add(bubble);
                lst.AddRange(bubble.neighbors);
                foreach (var neighbor in bubble.neighbors)
                {
                    neighbor.isLoseConnectOnMatched = true;
                }
            }
        }

        delayTime += clearDeltaTime;
        if (lst.Count > 0)
        {
            GetBubblesMatchingWithSpecialType(lst.ToArray(), delayTime, type);
        }
    }

    private void GetBubblesCoinMatching(Bubble[] neighborList, float delayTime, List<Bubble> resultList)
    {
        List<Bubble> lst = new List<Bubble>();
        foreach (var bubble in neighborList)
        {
            if (bubble.ballInfo.T == BubbleType.Coin && !resultList.Contains(bubble) && !_clearList.Contains(bubble))
            {
                bubble.clearDelay = delayTime;
                resultList.Add(bubble);
                lst.AddRange(bubble.neighbors);
                foreach (var neighbor in bubble.neighbors)
                {
                    neighbor.isLoseConnectOnMatched = true;
                }
            }
        }

        delayTime += clearDeltaTime;
        if (lst.Count > 0)
        {
            GetBubblesCoinMatching(lst.ToArray(), delayTime, resultList);
        }
    }

    private void GetDroppedBubblesWithDelayTime(List<Bubble> neighborList, float delayTime, float multi)
    {
        List<Bubble> lst = new List<Bubble>();
        foreach (var bubble in neighborList)
        {
            if (!_dropSequenceList.Contains(bubble))
            {
                bubble.clearDelay = delayTime * multi;
                _dropSequenceList.Add(bubble);
                lst.AddRange(bubble.neighbors);
            }
        }

        delayTime += dropDeltaTime;
        if (lst.Count > 0)
        {
            GetDroppedBubblesWithDelayTime(lst, delayTime, multi);
        }
    }

    private void UpdateNeighbors()
    {
        foreach (var bubble in bubbles)
        {
            bubble.UpdateNeighbors();
        }
    }

    #endregion

    #region COLOR COUNT

    public void AddColor(int colorId)
    {
        // Debug.Log("Add color " + colorId);
        countDict[colorId]++;
    }

    public void RemoveColor(int colorId)
    {
        if (colorId >= 0 && colorId <= 5)
            countDict[colorId]--;
    }

    public void RemoveBubble(Bubble bubble, bool isDespawn = true)
    {
        bubbles.Remove(bubble);
        OnBubbleListChanged?.Invoke();
        if (isDespawn)
            Destroy(bubble.gameObject);
    }

    public void AddMorphBubble(Bubble bubble)
    {
        morphBubbles.Add(bubble);
    }

    public void RemoveMorphBubble(Bubble bubble)
    {
        morphBubbles.Remove(bubble);
    }

    public void AddGhostBubble(Bubble bubble)
    {
        ghostBubbles.Add(bubble);
    }

    public void RemoveGhostBubble(Bubble bubble)
    {
        ghostBubbles.Remove(bubble);
    }

    public void AddCloudBubble(Bubble bubble)
    {
        rootBubbles.Add(bubble);
        cloudBubbles.Add(bubble);
    }

    public void RemoveCloud(Bubble bubble)
    {
        rootBubbles.Remove(bubble);
        cloudBubbles.Remove(bubble);
    }

    public void AddSwitchSpikeBubble(Bubble bubble)
    {
        switchSpikes.Add(bubble);
    }

    public void AddBeamRocket(Bubble bubble)
    {
        rocketBubbles.Add(bubble);
    }

    public void RemoveRocketBubble(Bubble bubble)
    {
        rocketBubbles.Remove(bubble);
    }

    public void AddDelayedFallBubble(Bubble bubble)
    {
        delayedFallBubbles.Add(bubble);
    }

    public void RemoveDelayedFallBubble(Bubble bubble)
    {
        delayedFallBubbles.Remove(bubble);
    }

    #endregion

    public void Aim(Vector3 position, int maxDot = -1, int maxLine = -1)
    {
        Vector3 direct = position - currentShootHolder.position;
        //direct.z = 0;
        //if (direct.y < 0)
        //{
        //    direct.y = 0;
        //}

        shootArrow.eulerAngles = new Vector3(0, 0, direct.GetAngleInDegree() - 90);

        int loop = booster != null && booster.ballInfo.T == BubbleType.Beam ? 1 : 10;
        if (maxLine != -1)
        {
            loop = 1;
        }

        aimController.Aim(currentShootHolder.position, direct, maxLine: loop, maxDot: maxDot);

        var v = GetRowCol(aimController.lastPoint);
        if (v.Row == curCol && v.Col == curCol)
            return;

        PoolWrapper.I.RemoveAllNotices();
        if (aimController.path.Count == 0)
            return;
        curRow = v.Row;
        curCol = v.Col;

        UpdateNotifyBooster(currentShootHolder.position, direct);
    }

    #region BOOSTER

    readonly RaycastHit2D[] _results = new RaycastHit2D[20];

    #region BOOSTER - SPAWN & REMOVE

    public void SpawnBooster(BoosterConfigItem config)
    {
        startBall_Rotate.SetActive(false);
        moveLeftText.gameObject.SetActive(false);
        RemoveBallSuggestion();
        // aimController.Hide();

        booster = Instantiate(bubblePrefab).GetComponent<Bubble>();
        if (config.Type == BoosterType.Bomb)
        {
            booster.SetData(new BallInfo { T = BubbleType.Fire });
            aimController.iAim.SetLine(0, true);
        }
        else if (config.Type == BoosterType.Laser)
        {
            booster.SetData(new BallInfo { T = BubbleType.Beam });
            aimController.iAim.SetLine(2, true);
        }
        else if (config.Type == BoosterType.Rainbow)
        {
            booster.SetData(new BallInfo { T = BubbleType.Rainbow });
            aimController.iAim.SetLine(1, true);
        }
        else if (config.Type == BoosterType.Thunder)
        {
            booster.SetData(new BallInfo { T = BubbleType.Lightning });
            aimController.iAim.SetLine(3, true);
        }

        booster.Trans.DOKill();
        booster.Trans.position = currentShootHolder.position;
        booster.Trans.localScale = Vector3.zero;
        booster.Trans.DOScale(boosterScale, 0.1f).SetEase(Ease.OutBack);
        booster.gameObject.layer = LayerDefine.StartBall;
        if (currentBubble != null)
        {
            currentBubble.Trans.DOKill();
            currentBubble.Trans.DOScale(0, .01f).OnComplete(() => { });
            PuzzleState.IsSwapping = false;
        }

        if (nextBubble != null)
        {
            nextBubble.Trans.DOKill();
            nextBubble.Trans.DOScale(0, .1f);
        }

        OnBoosterEnable.Invoke(booster.ballInfo.T);
    }

    public void RemoveBooster(bool isDisable = true, bool isShoot = true)
    {
        if (_shootRemain == 0)
        {
            if (currentBubble != null)
            {
                Destroy(currentBubble.gameObject);
                currentBubble = null;
            }
        }

        if (_shootRemain == 1)
        {
            if (nextBubble != null)
            {
                currentBubble = nextBubble;
                currentBubble.Pos = currentShootHolder.position;
                nextBubble = null;
            }
        }

        startBall_Rotate.SetActive(true);
        moveLeftText.gameObject.SetActive(true);
        PuzzleState.IsSwapping = true;
        OnBoosterDisable?.Invoke();
        if (booster == null) return;
        if (isDisable)
        {
            if (booster.gameObject.activeInHierarchy)
                Destroy(currentBubble.gameObject);
        }

        booster = null;

        if (currentBubble != null)
        {
            currentBubble.transform.DOScale(1, .2f).SetEase(Ease.OutBack, 1.01f).OnComplete(() =>
            {
                PuzzleState.BlockUserInput = false;
                PuzzleState.IsSwapping = false;
            });
            if (countDict[currentBubble.ballInfo.C] == 0)
            {
                currentBubble.SetData(new BallInfo { C = GetNextBubbleColor(), T = 0 });
                currentBubble.RemoveColor();
            }

            aimController.iAim.SetLine(currentBubble.ballInfo.C, false);
        }
        else
        {
            PuzzleState.IsSwapping = false;
        }

        if (nextBubble != null)
        {
            if (countDict[nextBubble.ballInfo.C] == 0)
            {
                nextBubble.SetData(new BallInfo { C = GetNextBubbleColor(), T = 0 });
                nextBubble.RemoveColor();
            }

            nextBubble.transform.DOScale(.8f, .2f).SetEase(Ease.OutBack, 1.01f);
        }
    }

    #endregion

    #region BOOSTER ACTIVE

    private bool ActiveBooster(Bubble bubble)
    {
        float multi = 1;
        bool isBooster = false;
        BubbleType bubbleType = bubble.ballInfo.T;
        List<Bubble> clearList = new List<Bubble>();
        if (bubbleType == BubbleType.Fire)
        {
            bubble.UpdateNeighbors();
            PuzzleSound?.PlayFireSound();
            isBooster = true;
            clearList = GetListNeighborsForBoosterBomb(bubble.Pos);
            PoolWrapper.I.GetFxObject(PoolWrapper.Fx_BoosterFireActive, bubble.Pos);
            multi = 0;
            foreach (var neighbor in clearList.ToArray())
            {
                if (neighbor.ballInfo.T == BubbleType.Rainbow)
                {
                    neighbor.isLive = false;
                    neighbor.DestroyBubble_Rainbow(bubble);
                    clearList.Remove(neighbor);
                }
            }

            foreach (var neighbor in clearList.ToArray())
            {
                if (!neighbor.isLive) clearList.Remove(neighbor);
                else
                {
                    if (neighbor.CanDestroyNow(BubbleType.Fire))
                    {
                        neighbor.isLive = false;
                    }
                }
            }

            foreach (var neighbor in bubble.neighbors)
            {
                if (neighbor.ballInfo.T == BubbleType.ColorBrush) bubble.BombCombineColorBrush();
            }
        }
        else if (bubbleType == BubbleType.Rainbow)
        {
            PuzzleSound?.PlayRainbowSound();
            isBooster = true;
            clearList = GetListNeighborsForBoosterRainbow(bubble.Pos);
            bool hasColorBrush = false;
            foreach (var neighbor in clearList.ToList())
            {
                if (neighbor.CanDestroyNow(BubbleType.Rainbow))
                {
                    neighbor.isLive = false;
                }

                if (neighbor.ballInfo.T == BubbleType.ColorBrush) hasColorBrush = true;
            }

            if (hasColorBrush)
            {
                List<Bubble> comboActiveList = new List<Bubble>();
                foreach (var bubble1 in clearList)
                {
                    foreach (var neighbor in bubble1.neighbors)
                    {
                        if (neighbor.isLive && neighbor.CanMatch)
                        {
                            comboActiveList.Add(neighbor);
                        }
                    }
                }

                int color = GetRandomColorOnConfigList();
                foreach (var combo in comboActiveList)
                {
                    combo.ChangeBallInfo(BubbleType.Normal, color);
                }
            }

            PoolWrapper.I.GetFxObject(PoolWrapper.Fx_RainbowMatching, bubble.Pos);
            OnBubbleListChanged?.Invoke();
        }
        else if (bubbleType == BubbleType.Lightning)
        {
            PuzzleSound?.PlayLightingSound();
            isBooster = true;
            clearList = GetListNeighborsForBoosterLighting(bubble.Pos);
            foreach (var neighbor in clearList)
            {
                if (neighbor.CanDestroyNow(BubbleType.Rainbow))
                    neighbor.isLive = false;
            }

            PoolWrapper.I.GetFxLightingActive(bubble.Pos);
            multi = 0;
        }

        if (clearList.Count > 0)
        {
            _comboCount++;
            InteractWithNeighbors(bubble, false);
            List<Bubble> resultList = new List<Bubble>();
            foreach (var item in clearList)
            {
                if (item.ballInfo.T == BubbleType.Coin)
                {
                    GetCoinMatched(item, resultList);
                }

                if (item.IsPower)
                {
                    item.canCombo = true;
                }
            }

            foreach (var bubble1 in resultList)
            {
                bubble1.ignoreMulti = true;
                if (bubble1.CanDestroyNow(bubble.ballInfo.T))
                {
                    bubble.isLive = false;
                }
            }

            clearList.AddRange(resultList);
            clearList = clearList.Distinct().ToList();
            DestroyTheMatchedBubbles(bubble, clearList, -(int)bubbleType, bubble.ballInfo.T, multi);
            specialBallActiveCount++;
            StartCoroutine(DelayDecreaseSpecialCount());
        }
        else
        {
            PuzzleState.BlockUserInput = false;
        }

        if (isBooster)
        {
            if (ShootRemain <= 0 && clearList.Count <= 0)
            {
                SetOutOfMove();
            }
        }

        return isBooster;
    }

    IEnumerator DelayDecreaseSpecialCount()
    {
        yield return null;
        specialBallActiveCount--;
    }

    private void DecreaseBoosterWhenUsed(Bubble bubble)
    {
        // var boosterUI = InGameMenu.I.booster.ToList().Find(s => s.bubbleType == bubble.ballInfo.T);
        // if (boosterUI != null)
        // {
        //     if (boosterUI.canUseFree)
        //         boosterUI.UseFree();
        //     else
        //         GameData.I.AddBooster(boosterUI.type, -1);
        // }
        // else
        // {
        //     Debug.LogError("Not found boosterUI " + bubble.ballInfo.T);
        // }
    }

    private void CheckClearForLaserBooster(Bubble laser, Vector3 direct)
    {
        StartCoroutine(RunBoosterBeam(laser, direct));
    }

    private IEnumerator RunBoosterBeam(Bubble laser, Vector3 direct)
    {
        PuzzleState.BlockUserInput = true;
        PuzzleSound?.PlayBeamSound();
        objBeam.SetActive(true);
        objBeam.transform.eulerAngles = new Vector3(0, 0, direct.GetAngleInDegree() - 90);
        objBeam.GetComponent<Animator>().Play("play", 0, 0);
        var listBalls = GetListNeighborsForBoosterBeam(laser.Trans.position, direct);
        foreach (var bubble in listBalls)
            if (bubble.CanDestroyNow(BubbleType.Rainbow))
                bubble.isLive = false;
        List<Bubble> resultList = new List<Bubble>();
        foreach (var item in listBalls)
        {
            if (item.ballInfo.T == BubbleType.Coin)
            {
                GetCoinMatched(item, resultList);
            }

            if (item.IsPower)
            {
                item.canCombo = true;
            }
        }

        foreach (var bubble1 in resultList)
        {
            bubble1.ignoreMulti = true;
        }

        listBalls.AddRange(resultList);
        listBalls = listBalls.Distinct().ToList();

        if (listBalls.Count > 0)
        {
            _comboCount++;
            InteractWithNeighbors(laser, false);
            DestroyTheMatchedBubbles(laser, listBalls, -(int)laser.ballInfo.T, laser.ballInfo.T, 0);
        }

        _needCheckAfterShoot = true;
        laser.Trans.DOScale(1.3f, ballScaleDuration).OnComplete(() =>
        {
            if (booster.gameObject.activeInHierarchy)
                Destroy(laser.gameObject);
        });
        yield return new WaitForSeconds(ballScaleDuration);
        yield return CheckColorAfterUseBooster();
        RemoveBooster(true);
        yield return new WaitForSeconds(.5f);
        objBeam.SetActive(false);
        PuzzleState.BlockUserInput = false;
        PuzzleState.IsShooting = false;

        if (ShootRemain <= 0 && listBalls.Count <= 0)
        {
            SetOutOfMove();
        }
    }

    public List<Bubble> GetListNeighborsForBoosterBeam(Vector3 origin, Vector2 direct)
    {
        Debug.DrawRay(origin, direct * 10);
        List<Bubble> balls = new List<Bubble>();
        var size = Physics2D.CircleCastNonAlloc(origin, .3f, direct, _results, 10,
            ballOnBoardLayer);
        var top = _camera.orthographicSize;
        for (int i = 0; i < size; i++)
        {
            var bubble = _results[i].collider.GetComponent<Bubble>();
            if (bubble.IsBoosterCanDestroy && bubble.Trans.position.y <= top)
            {
                balls.Add(bubble);
            }
        }

        return balls;
    }

    #endregion

    #region BOOSTER NOTIFICATION

    private void UpdateNotifyBooster(Vector3 origin, Vector3 direct)
    {
        if (booster != null)
        {
            if (booster.ballInfo.T == BubbleType.Fire)
            {
                UpdateNotifyBoosterFire();
            }
            else if (booster.ballInfo.T == BubbleType.Rainbow)
            {
                UpdateNotifyBoosterRainbow();
            }
            else if (booster.ballInfo.T == BubbleType.Lightning)
            {
                UpdateNotifyBoosterLighting();
            }
            else if (booster.ballInfo.T == BubbleType.Beam)
            {
                UpdateNotifyBoosterBeam(origin, direct);
                curCol = curRow = -1;
            }
        }
    }

    private void UpdateNotifyBoosterRainbow()
    {
        var listBalls = GetListNeighborsForBoosterRainbow(aimController.lastPoint);
        ShowNotify(listBalls);
    }

    private void UpdateNotifyBoosterLighting()
    {
        var targetPos = aimController.lastPoint + Vector3.up * rowDistance;
        if (targetPos.y > limitTop.position.y)
        {
            targetPos = aimController.lastPoint;
        }

        var listBalls = GetListNeighborsForBoosterLighting(targetPos);
        ShowNotify(listBalls);
    }

    private void UpdateNotifyBoosterBeam(Vector3 origin, Vector2 direct)
    {
        var listBalls = GetListNeighborsForBoosterBeam(origin, direct);
        ShowNotify(listBalls);
    }

    private void UpdateNotifyBoosterFire()
    {
        var list = listRowColForBombs;
        if (curRow % 2 == 0)
            list = listRowColForBombs2;

        foreach (var info in list)
        {
            int row = curRow + info.Row;
            int col = curCol + info.Col;
            if (row < 0 || col > 11)
                continue;
            var notificationPosition = GetPositionByRowCol(row, col) + boardContainer.position;
            var hasBubbleUnder = Physics2D.Raycast(notificationPosition, Vector2.zero, 0, ballOnBoardLayer).collider !=
                                 null;
            PoolWrapper.I.SpawnNotice(notificationPosition, false, hasBubbleUnder ? 1 : .5f);
        }
    }

    #endregion

    #region BOOSTER GET NEIGHBORS

    public List<Bubble> ThunderBolt_GetListBubbleInsideScreen()
    {
        List<Bubble> list = new List<Bubble>();
        var maxY = _camera.orthographicSize - 0.6f;
        foreach (var b in bubbles)
        {
            if (b.Pos.y < maxY && b.isLive && b.canTarget && !b.isDrop && !b.isPowerIgnore &&
                b.IsThunderBoltCanTarget &&
                b.hasConnectedWithRoot)
            {
                list.Add(b);
            }
        }

        return list;
    }

    public List<Bubble> GetListNeighborsForBoosterBomb(Vector3 origin)
    {
        List<Bubble> list = new List<Bubble>();
        var size = Physics2D.CircleCastNonAlloc(origin, 2, Vector2.zero, _results, 1, ballOnBoardLayer);
        for (int i = 0; i < size; i++)
        {
            var bubble = _results[i].collider.GetComponent<Bubble>();
            list.Add(bubble);
        }

        return list;
    }

    public List<Bubble> GetListNeighborsForBoosterRainbow(Vector3 origin)
    {
        _clearList.Clear();
        ResetCheckCondition();
        var list = new List<Bubble>();
        var size = Physics2D.CircleCastNonAlloc(origin, 0.6f, Vector2.zero, _results, 1, ballOnBoardLayer);
        for (int i = 0; i < size; i++)
        {
            var bubble = _results[i].collider.GetComponent<Bubble>();
            if (bubble.CanMatch || bubble.IsBoosterCanDestroy)
            {
                list.Add(bubble);
                _clearList.Add(bubble);
            }
        }

        foreach (var bubble in list)
        {
            bubble.clearDelay = 0;
            if (bubble.CanMatch)
            {
                if (bubble.IsDou)
                {
                    GetBubblesMatching(bubble, bubble.neighbors.ToArray(), bubble.duoC1, 0);
                    GetBubblesMatching(bubble, bubble.neighbors.ToArray(), bubble.duoC2, 0);
                }
                else
                {
                    GetBubblesMatching(bubble, bubble.neighbors.ToArray(), bubble.ballInfo.C, 0);
                }
            }
        }

        return new List<Bubble>(_clearList);
    }

    public List<Bubble> GetMatchingWithRainbowBubble(Bubble bubbleRainbow)
    {
        _clearList.Clear();
        ResetCheckCondition();
        var list = new List<Bubble>(bubbleRainbow.neighbors);
        foreach (var bubble in list)
        {
            bubble.clearDelay = 0;
            GetBubblesMatching(bubble, bubble.neighbors.ToArray(), bubble.ballInfo.C, 0);
        }

        return new List<Bubble>(_clearList);
    }

    private List<Bubble> GetListNeighborsForBoosterLighting(Vector3 origin)
    {
        origin.x = -10;
        Debug.DrawRay(origin, Vector3.right * 30);
        var list = new List<Bubble>();
        var size = Physics2D.RaycastNonAlloc(origin, Vector2.right, _results, 30, ballOnBoardLayer);
        for (int i = 0; i < size; i++)
        {
            var bubble = _results[i].collider.GetComponent<Bubble>();
            if (bubble.IsBoosterCanDestroy)
            {
                list.Add(bubble);
            }
        }

        return list;
    }

    #endregion

    #endregion

    #region RESULT

    private bool CheckWin()
    {
        if (collectMode == CollectMode.Gem)
        {
            return false;
        }

        bool isWin = true;
        for (int i = 0; i < 6; i++)
        {
            if (countDict[i] > 0)
            {
                isWin = false;
                break;
            }
        }

        if (!showEndCard)
        {
            return isWin;
        }

        if (isWin && PuzzleState.IsPlaying)
        {
            if (ShootRemain > 0 && needCreateNewBall)
                CreateNewShootBubbleAfterShoot();
            PuzzleState.IsPlaying = false;
            StartCoroutine(CRWin());
        }

        return isWin;
    }

    private IEnumerator CRWin()
    {
        //Luna.Unity.Analytics.LogEvent("PlayerWin", 0);
        //Luna.Unity.LifeCycle.GameEnded();
        Time.timeScale = 1;
        timeScale = 1;
        PuzzleState.IsEndGame = true;
        while (PuzzleState.IsSwapping)
        {
            yield return null;
        }

        PuzzleState.IsPlaying = false;
        HideAim();
        foreach (var bubble in bubbles.ToArray())
        {
            bubble.Drop(comboCount, null);
        }

        OnPreWin?.Invoke();
        firework.SetActive(true);
        PuzzleSound?.PlayWinSound();
        WaitForSeconds w = new WaitForSeconds(.05f);
        WaitForSeconds winDelayTime = new WaitForSeconds(winDelay);
        transform.DOKill();
        // if (currentBubble != null)
        // {
        //     currentBubble.ShootOnWin();
        //     yield return w;
        // }
        //
        // if (nextBubble != null)
        // {
        //     nextBubble.ShootOnWin();
        //     yield return w;
        // }
        //
        // List<Bubble> spawnBubbles = new List<Bubble>();
        // while (ShootRemain > 0 && !_isBreakWinShoot)
        // {
        //     ShootRemain--;
        //     OnShootChanged?.Invoke();
        //     var bubble = SpawnBubble();
        //     bubble.SetData(new BallInfo { C = Random.Range(0, 6), T = 0 }, true);
        //     bubble.Pos = currentShootHolder.position;
        //     bubble.ShootOnWin();
        //     spawnBubbles.Add(bubble);
        //     // if (SoundController.IsVibration) GameRemoteConfigSingleton.I.UseVibration(2);
        //     yield return w;
        // }

        // if (!_isBreakWinShoot) yield return CRAutoShootOnWin(spawnBubbles);
        yield return winDelayTime;

        OnWin?.Invoke();
        //Luna.Unity.Analytics.LogEvent("Show Popup", 0);
        // LevelUnlockData.I.SetWinCounty(levelIndex, LevelUnlockData.I.GetWinCount(levelIndex) + 1);
        // GameData.I.DataChanged = true;
        GameController.I.completePopup.SetActive(true);
        startBalls.gameObject.SetActive(false);
    }

    private IEnumerator CRAutoShootOnWin(List<Bubble> spawnBubbles)
    {
        while (spawnBubbles.Count > 0)
        {
            spawnBubbles = spawnBubbles.FindAll(s => s.state == BallState.SequenceDrop).ToList();
            yield return new WaitForSeconds(0.1f);
        }

        yield return new WaitForSeconds(0.1f);
    }

    public void SetLevel(int level)
    {
        levelIndex = level;
    }

    public void SetOutOfMove()
    {
        if (PuzzleState.IsPlaying)
        {
            PuzzleState.BlockUserInput = false;
            timeScale = 1;
            Time.timeScale = 1;
            OnOutOfMove?.Invoke();
        }
    }

    public void Skip()
    {
        _isBreakWinShoot = true;
        PlusScore(ShootRemain * 500, false, Vector2.zero);
        OnSkip?.Invoke();
    }

    #endregion

    #region Suggestion

    private void RemoveBallSuggestion()
    {
        if (_coroutineShowBallSuggestion != null)
            StopCoroutine(_coroutineShowBallSuggestion);
        _isSuggestionShowing = false;
        _suggestionCountTime = 0;
        PoolWrapper.I.RemoveAllNotices();
    }

    private Coroutine _coroutineShowBallSuggestion;
    private IEnumerable<object> spawnList;

    #endregion

    #region OTHER

    private void ShowNotify(List<Bubble> bubblesList)
    {
        foreach (var bubble in bubblesList)
        {
            PoolWrapper.I.SpawnNotice(bubble.Trans.position, false);
        }
    }

    private RowCol GetRowCol(Vector3 position)
    {
        var v = new RowCol();
        v.Row = (int)(Mathf.Abs(position.y - boardContainer.position.y) / rowDistance);
        v.Col = (int)Mathf.Abs(position.x + _offset.x);
        return v;
    }

    private Vector3 GetPositionByRowCol(int row, int col)
    {
        Vector3 position = new Vector3(1 * col, -rowDistance * row - _offset.y);
        if (row % 2 != 0)
            position.x += 1f / 2;
        position.x -= _offset.x;
        return position;
    }


    public List<int> GetStarScore()
    {
        return _starScore;
    }

    #endregion

    #region PUZZLE TEST

    public void SetShootRemain(int amount)
    {
        ShootRemain = amount;
    }

    public void AddShootRemain(int value) => ShootRemain += value;


    public void TestWin()
    {
        StartCoroutine(CRWin());
    }

    public void LoadPreviousLevel()
    {
        // levelIndex--;
        // ClearLevel();
        // StartCoroutine(LoadLevel());
    }

    public void PlayLevel(int level)
    {
        Debug.Log("Load level 1-1");
        levelIndex = level - 1;
        ClearLevel();
        StartCoroutine(LoadLevel());
    }

    public void ReloadLevel()
    {
        isLoaded = false;
        _isLevelLoad = true;
        ClearLevel();
        StartCoroutine(LoadLevel());
    }

    public void LoadNextLevel()
    {
        // levelIndex++;
        // ClearLevel();
        // StartCoroutine(LoadLevel());
    }

    public bool IsRunning()
    {
        return Application.isPlaying;
    }

    #endregion

    #region CONVERT

    public void IncreaseNumStars()
    {
        numStars++;
    }

    public int GetScore()
    {
        return Score;
    }

    public void InCreaseLevel()
    {
        levelIndex++;
    }

    public int GetLevelIndex()
    {
        return levelIndex;
    }

    public int GetLevelView()
    {
        return levelView;
    }

    public bool IsHardLevel()
    {
        return levelConfig.hard;
    }

    public int GetNumStars()
    {
        if (Score > _targetStars[2])
            return 3;
        if (Score > _targetStars[1])
            return 2;
        return 1;
    }

    public int GetShootRemain()
    {
        return ShootRemain;
    }

    public List<Bubble> GetBubbles()
    {
        return bubbles;
    }

    public float GetRowDistance()
    {
        return rowDistance;
    }

    public void HideAim()
    {
        aimController.Hide();
    }

    public void Continue(bool isWatchAds, int amount = 5)
    {
        ShootRemain += amount;
        PuzzleState.IsPlaying = true;
        PuzzleState.IsSwapping = false;
        PuzzleState.BlockUserInput = false;
        CreateShootBubbles();

        OnContinue?.Invoke(isWatchAds, amount);
    }

    #endregion

    public bool IsInsideClearList(Bubble bubble)
    {
        return _clearList.Contains(bubble);
    }

    private void OnDrawGizmos()
    {
        if (Input.GetMouseButton(1))
        {
            var scale1 = new Vector3(0.4f, 0.4f, 1);
            var scale2 = new Vector3(0.1f, 0.1f, 1);
            foreach (var bubble in bubbles)
            {
                Gizmos.color = Color.white;
                Gizmos.DrawCube(bubble.Pos, scale1);
                Gizmos.color = Color.red;
                if (bubble.isLive)
                {
                    Gizmos.DrawCube(bubble.Pos, scale2);
                }
            }
        }

        if (Input.GetKey(KeyCode.LeftControl))
        {
            var scale1 = new Vector3(0.4f, 0.4f, 1);
            var scale2 = new Vector3(0.1f, 0.1f, 1);
            foreach (var bubble in bubbles)
            {
                if (bubble.CanMatch)
                {
                    Gizmos.color = Color.white;
                    Gizmos.DrawCube(bubble.Pos, scale1);
                    // Gizmos.color = Color.red;
                    // Gizmos.DrawCube(bubble.Pos, scale2);
                }
            }
        }
    }

    // private void OnDestroy()
    // {
    //     transform.DOKill();
    // }

    public int GetAmountOfBoosterInGame(BubbleType type)
    {
        return 0;
    }

    public int GetRemainBubbles()
    {
        if (collectMode == CollectMode.Color)
        {
            int remainBubbles = 0;
            foreach (var i in countDict) remainBubbles += i.Value;
            return remainBubbles;
        }
        else
        {
            return gemCount;
        }
    }

    public int GetNumCols()
    {
        if (levelConfig == null)
        {
            return 11;
        }

        return levelConfig.col;
    }


    public float aimAngle = 0;

    public void SetAimAngle()
    {
        BubblePuzzleLogic.I.HideAim();
        BubblePuzzleLogic.I._isAiming = true;
        var direct = new Vector3(Mathf.Cos(aimAngle * Mathf.Deg2Rad), Mathf.Sin(aimAngle * Mathf.Deg2Rad));
        Aim(currentShootHolder.position + direct * 6);
        Debug.Break();
        //MouseUp(currentShootHolder.position + direct * 6);
    }
}

public enum CollectMode
{
    Color,
    Gem,
}