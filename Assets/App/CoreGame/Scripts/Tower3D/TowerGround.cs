using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerGround : MonoBehaviour
{
    public LayerMask Mask;
    public float PushPower = 10;
    public void OnCollisionStay(Collision collision)
    {
        if(Mask == (Mask | (1 << collision.gameObject.layer)))
        {
            collision.gameObject.GetComponent<Rigidbody>().AddForce(Vector3.right * PushPower, ForceMode.Force);
        }
    }
}
