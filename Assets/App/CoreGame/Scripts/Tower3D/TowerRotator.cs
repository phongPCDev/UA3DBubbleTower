using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerRotator : MonoBehaviour
{
    public float RotateSpeed = 60;
    public float LerpSpeed = 2;
    public float DistanceRotateThreshold = 2;

    private Camera mainCam;

    private Vector3 startPos = Vector3.zero;
    private Vector3 prevPos = Vector3.zero;
    private Vector3 targetPos = Vector3.zero;
    private Vector3 posDelta = Vector3.zero;
    private bool rotationBegan = false;

    private Vector3 horizontalRotateAxis = Vector3.zero;

    private void Awake()
    {
        mainCam = Camera.main;
        horizontalRotateAxis = Vector3.Cross(this.transform.position - mainCam.transform.position, mainCam.transform.right).normalized;
    }

    private void Update()
    {
        Rotate();
    }

    private void Rotate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            rotationBegan = false;
            startPos = Input.mousePosition;
            prevPos = Input.mousePosition;
        }

        if (Input.GetMouseButton(0))
        {
            targetPos = Input.mousePosition;
        }

        if (!rotationBegan)
        {
            if (Vector3.Distance(startPos, targetPos) >= DistanceRotateThreshold)
            {
                rotationBegan = true;
                BubblePuzzleLogic.I.HideAim();
                PuzzleState.IsRotate = true;
                Debug.Log("Enter here");
            }
        }
        else if (prevPos != targetPos)
        {
            posDelta = (targetPos - prevPos);

            transform.Rotate(Vector3.up, -Vector3.Dot(posDelta, mainCam.transform.right) * RotateSpeed * Time.deltaTime, Space.World);
            //transform.Rotate(mainCam.transform.right, Vector3.Dot(posDelta, Vector3.up) * RotateSpeed * Time.deltaTime, Space.World);


            prevPos = Vector3.Lerp(prevPos, targetPos, LerpSpeed * Time.deltaTime);
        }
    }
}
