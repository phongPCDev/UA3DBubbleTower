using System.Collections.Generic;
using AFramework;
using UnityEngine;

public class SpriteManager : ManualSingletonMono<SpriteManager>
{
    [SerializeField] private List<Sprite> editorSprites;
    [SerializeField] private List<Sprite> sprites;

    private Dictionary<string, Sprite> _spritesDict;
    [SerializeField] private string[] bubbleName;
    [SerializeField] private string[] bubbleNameCVD;
    [SerializeField] private string[] bubbleDouName;
    [SerializeField] private string[] bubbleDouNameVCD;
    [SerializeField] private string[] bubbleWoodName;
    [SerializeField] private string[] bubbleGemName;

    private const string obs_minus = "obs_minus";
    private const string obs_plus = "obs_plus";
    public const string bn_coin = "bn_coin";
    public const string ba_beam_arrow = "ba_beam_arrow";
    public const string pw_rocket = "pw_rocket";
    public const string pw_bg = "pw_bg";
    public const string obs_ghost_on = "obs_ghost_on";
    public const string obs_ghost_off = "obs_ghost_off";
    
    public Color[] matchColors =
    {
        new Color(0.8941177f, 0f, 0.04705882f), new Color(0.3333333f, 0.8980392f, 0.04705882f),
        new Color(0.0859f, 0.0859f, 0.941f), new Color(0.9960784f, 0.5882353f, 0.1333333f),
        new Color(0.9803922f, 0f, 0.6745098f), new Color(0f, 0.7254902f, 0.8117647f),
    };

    protected override void Awake()
    {
        base.Awake();
        _spritesDict = new Dictionary<string, Sprite>();
        foreach (var sprite in sprites)
        {
            if (sprite != null)
                _spritesDict.Add(sprite.name, sprite);
        }

        foreach (var sprite in editorSprites)
        {
            if (sprite != null)
                _spritesDict.Add(sprite.name, sprite);
        }
    }

    public static Sprite GetSprite(string spriteName)
    {
        return I._spritesDict[spriteName];
    }

    public static Sprite GetNormalSprite(int c)
    {
        if (I == null) return null;

        if (PuzzleState.IsColorblind)
            return I._spritesDict[I.bubbleNameCVD[c]];
        return I._spritesDict[I.bubbleName[c]];
    }

    public static Sprite GetGemSprite(int c)
    {
        if (I == null) return null;
        if (PuzzleState.IsColorblind)
            return I._spritesDict[I.bubbleGemName[c]];
        return I._spritesDict[I.bubbleGemName[c]];
    }

    public static Sprite GetDuoSprite(int c)
    {
        if (PuzzleState.IsColorblind)
            return I._spritesDict[I.bubbleDouNameVCD[c]];
        return I._spritesDict[I.bubbleDouName[c]];
    }

    public static Sprite GetMinusSprite()
    {
        return I._spritesDict[obs_minus];
    }

    public static Sprite GetPlusSprite()
    {
        return I._spritesDict[obs_plus];
    }

    public static Sprite GetWoodSprite(int c)
    {
        return I._spritesDict[I.bubbleWoodName[c]];
    }

    public static Sprite GetSprite(BubbleType t, int c)
    {
        string spriteName = string.Empty;
        if (t == BubbleType.Normal)
        {
            spriteName = I.bubbleName[c];
        }
        else if (t != BubbleType.Paint)
        {
        }
        else
        {
            Debug.Log($"Not found sprite for type {t} id {c}");
            return null;
        }

        return I._spritesDict[spriteName];
    }

    public void LoadSprites()
    {
        sprites = GameHelper.GetAllSpriteAssetsAtPath("Assets/App/CoreGame/Textures/BB_Texture");
        sprites.AddRange(GameHelper.GetAllSpriteAssetsAtPath("Assets/App/CoreGame/Textures/BB_CVD_Texture"));
    }

    public void InitBubbleName()
    {
        bubbleName = new[] { "bb_red", "bb_green", "bb_blue", "bb_orange", "bb_pink", "bb_cyan" };
        bubbleNameCVD = new[] { "CVD_red", "CVD_green", "CVD_blue", "CVD_yellow", "CVD_pink", "CVD_cyan" };
        bubbleDouName = new[]
        {
            "duo_red", "duo_green", "duo_blue", "duo_orange", "duo_pink",
            "duo_cyan"
        };
        bubbleDouNameVCD = new[]
        {
            "CVD_duo_red", "CVD_duo_green", "CVD_duo_blue", "CVD_duo_yellow",
            "CVD_duo_pink",
            "CVD_duo_cyan"
        };

        bubbleWoodName = new[] { "obs_wood3", "obs_wood2", "obs_wood1" };
        bubbleGemName = new[]
            { "nt_gem_red", "nt_gem_green", "nt_gem_blue", "nt_gem_yellow", "nt_gem_pink", "nt_gem_cyan" };
    }
}