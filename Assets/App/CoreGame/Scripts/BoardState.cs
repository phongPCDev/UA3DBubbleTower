public enum BoardState
{
    None,
    BallMoving,
    BoardNeedMoving,
    BoardMoving,
    BallSwap,
    BoardClear,
    WaitToCheckMatch,
    AutoPlay,
}