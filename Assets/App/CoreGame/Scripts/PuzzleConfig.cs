public abstract class PuzzleConfig
{
    public const float DropGravity = 4.5f;
    public const float NextBubbleScale = 1f;
    public const float GhostAlpha = 0.5f;
}