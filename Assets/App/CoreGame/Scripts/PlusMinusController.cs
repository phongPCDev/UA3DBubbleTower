using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlusMinusController : MonoBehaviour
{
    [SerializeField] private SpriteRenderer render;
    [SerializeField] private Sprite minusSprite;
    [SerializeField] private Sprite plusSprite;

    public void SetMinus()
    {
        render.sprite = minusSprite;
    }

    public void SetPlus()
    {
        render.sprite = plusSprite;
    }
}