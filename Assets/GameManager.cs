using System;
using System.Collections;
using System.Collections.Generic;
using AFramework;
using UnityEngine;

public class GameManager : SingletonMono<GameManager>
{
    public int level = 76;
    private void Start()
    {
        // LoadLevel();
    }

    public void LoadLevel()
    {
        BubblePuzzleLogic.I.gameObject.SetActive(true);
        Debug.Log("Load level 1");
        BubblePuzzleLogic.I.PlayLevel(level);
    }
}