﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDeSpawnEffect : MonoBehaviour
{
    public float LiveTime = 1;

    private void OnEnable()
    {
        StartCoroutine(RemoveObj());
    }

    IEnumerator RemoveObj()
    {
        yield return new WaitForSeconds(LiveTime);
        Destroy(gameObject);
    }
}