using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerColorBrushController : MonoBehaviour
{
    [SerializeField] private Color[] colors;
    [SerializeField] private ParticleSystem particleColor;
    [SerializeField] private float delayActive = 0.5f;
    public Bubble owner;

    public void PlayFx(Bubble bubble, int color, RaycastHit2D[] hits = null, bool onHit = false)
    {
        owner = bubble;
        if (color < 0 || color > 6)
        {
            if (BubblePuzzleLogic.I.nextBubble != null)
            {
                color = BubblePuzzleLogic.I.nextBubble.ballInfo.C;
            }
            else
            {
                color = BubblePuzzleLogic.I.GetRandomColorOnConfigList();
            }
        }

        // BubblePuzzleLogic.I.specialBallActiveCount++;
        // var particleColorMain = particleColor.main;
        // particleColorMain.startColor = colors[color];
        // if (hits != null) StartCoroutine(Active(hits, onHit, color));
    }

    public void PlayFx(Bubble bubble, BubbleType targetType, int color, RaycastHit2D[] hits = null, bool onHit = false)
    {
        owner = bubble;
        BubblePuzzleLogic.I.specialBallActiveCount++;
        // var particleColorMain = particleColor.main;
        // particleColorMain.startColor = colors[color];
        if (hits != null) StartCoroutine(Active(hits, targetType));
    }

    IEnumerator Active(RaycastHit2D[] hits, bool onHit, int c)
    {
        yield return new WaitForSeconds(delayActive);
        // foreach (var hit2D in hits)
        // {
        //     var neighbor = hit2D.collider.GetComponent<Bubble>();
        //     if (neighbor != this && neighbor.isLive && neighbor.ballInfo.T is BubbleType.Normal or BubbleType.Frozen
        //             or BubbleType.Morph or BubbleType.Chain or BubbleType.Duo or BubbleType.Minus
        //             or BubbleType.Plus ||
        //         neighbor.ballInfo.T == BubbleType.GhostEnable && !onHit ||
        //         (neighbor.ballInfo.T == BubbleType.GhostDisable && onHit))
        //         neighbor.ChangeBallInfo(BubbleType.Normal, c);
        //     else if (neighbor.IsPower && neighbor.isLive && neighbor.canTarget)
        //     {
        //         neighbor.RemoveConnections(owner.ballInfo.T, true);
        //         neighbor.StartCoroutine(neighbor.DestroyBubble(owner,  c, owner.ballInfo.T, false));
        //     }
        // }

        BubblePuzzleLogic.I.specialBallActiveCount--;
    }

    IEnumerator Active(RaycastHit2D[] hits, BubbleType targetType)
    {
        yield return new WaitForSeconds(delayActive);
        // List<Bubble> bubbles = new List<Bubble>();
        // foreach (var hit2D in hits)
        // {
        //     var neighbor = hit2D.collider.GetComponent<Bubble>();
        //
        //     if (neighbor.IsPower && neighbor.isLive)
        //     {
        //         neighbor.RemoveConnections(owner.ballInfo.T, true);
        //         neighbor.StartCoroutine(neighbor.DestroyBubble(owner, -(int)owner.ballInfo.T, owner.ballInfo.T, false));
        //     }
        //     else if (neighbor != this && neighbor.IsColorBrushCanChange())
        //     {
        //         neighbor.RemoveOnChangeBubbleType();
        //         var newBallInfo = new BallInfo { T = targetType };
        //         if (targetType == BubbleType.Rocket)
        //         {
        //             newBallInfo.C = 30 + 60 * Random.Range(0, 6);
        //         }
        //
        //         neighbor.SetData(newBallInfo);
        //         bubbles.Add(neighbor);
        //         neighbor.ignoreCombo = true;
        //         neighbor.canTarget = false;
        //         neighbor.RemoveConnections(owner.ballInfo.T);
        //     }
        // }
        // BubblePuzzleLogic.I.UpdateDrop();
        //
        // foreach (var hit2D in bubbles)
        // {
        //     hit2D.StartCoroutine(hit2D.DestroyBubble(owner, 0, owner.ballInfo.T, false));
        // }

        BubblePuzzleLogic.I.specialBallActiveCount--;
    }
}